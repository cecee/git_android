#ifndef __JNI_TIMER_H__
#define __JNI_TIMER_H__
//--------------------
//For timer
#include <signal.h>
#include <stdio.h>
#include <sys/time.h>
//-----------------------
#include <jni.h>
#include "extern.h"


class Jni_timer
{
	public:
//	struct sigaction sa;
//    struct itimerval lxtimer;
//    struct itimerval set_time_val, get_time_val;
//    static int ListCnt;
//static void handler(int sig, siginfo_t *si, void *uc);
	struct sigevent sev;
    struct itimerspec its;
    sigset_t mask;
    struct sigaction sa;
    timer_t timerid;
	
	static void timer_handler(int sig, siginfo_t *si, void *uc);
	
	Jni_timer();
	~Jni_timer();

};
#endif