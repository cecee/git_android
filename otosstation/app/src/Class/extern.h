#ifndef __EXTERN_H__
#define __EXTERN_H__
#include <stdio.h>
#include <string.h>

#include <jni.h>
#include "Dboard_control.h"
#include "Jni_timer.h"
#include "JniMain.h"

#include <android/log.h>
#define  LOG_TAG "cecee"
#define  TRACE(...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

#define BUILD_UINT16(loByte, hiByte) \
          ((int)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))
#define HI_UINT16(a) (((a) >> 8) & 0xFF)
#define LO_UINT16(a) ((a) & 0xFF)


extern Dboard_control  *pDboard;
//extern Jni_timer* pTimer;
extern int serial_lock;
extern int nfc_timeout;
extern int dummy_counter;
extern int TTY_S0, TTY_S1, TTY_S3;
extern int  SendToJava(int kind);

#endif