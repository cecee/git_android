#include "com_otosone_otosstation_utils_jni.h"
#include <jni.h>
//#include "JniMain.h"
#include "extern.h"

JavaVM *j_vm;
JNIEnv *j_env;

static jobject j_obj;
static jclass jNativesCls;
static jclass j_cls;
jmethodID android_call;
//jmethodID mjmethod;
int nfc_timeout;
int dummy_counter=0;

Dboard_control  *pDboard;
//NFC_control  *pNFC;
int TTY_S0=-1;
int TTY_S1=-1;
int TTY_S3=-1;
int FC_Flag = -1;

int SendToJava(int kind);

int JNI_OnLoad(JavaVM* vm, void* reserved) {
	j_vm = vm;
	j_vm->GetEnv((void**) &j_env, JNI_VERSION_1_6);
	j_cls = j_env->FindClass( "com/otosone/otosstation/utils/jni");
	android_call = j_env->GetMethodID(j_cls, "fromJni", "(I)V");
    j_obj = j_env->NewGlobalRef(j_env->NewObject(j_cls, android_call));
	return JNI_VERSION_1_6;
}

int  SendToJava(int kind)
{
 	//j_vm->AttachCurrentThread(&j_env, NULL);
	//j_env->CallVoidMethod(j_obj, android_call, kind);//
	FC_Flag = kind;
	return 1;
}

JNIEXPORT void JNICALL Java_com_otosone_otosstation_utils_jni_DboardOpen  (JNIEnv *env, jobject obj)
{
     TRACE("#@#Java_com_otosone_otosstation_utils_jni_DboardOpen\n");
      pDboard = new Dboard_control();

}

JNIEXPORT jintArray JNICALL Java_com_otosone_otosstation_utils_jni_returnedUnsignedShortArr
  (JNIEnv *env, jobject obj, jint idx)
{
	int sz;
	Uint8 buf[512];
    int tmp;
	switch(idx){
		case 0:
			sz=sizeof(pDboard->db_info);
			memcpy(buf, &pDboard->db_info, sz);
			break;
		case 1:
			sz=sizeof(pDboard->cb_all);
			memcpy(buf, &pDboard->cb_all, sz);
			break;
		case 2:
			sz=sizeof(pDboard->mCB_info);
			memcpy(buf, &pDboard->mCB_info, sz);
			break;
		case 3:
      //  	sz=sizeof(pNFC->nfc_up);
      //  	memcpy(buf, &pNFC->nfc_up, sz);
        	break;
        case 4:
            sz=sizeof(pDboard->db_upload.dStatus);
            memcpy(buf, &pDboard->db_upload.dStatus, sz);
            break;
         case 5:
             sz=sizeof(pDboard->db_upload.CbData)+1;
             tmp = (int)(&pDboard->db_upload.cid)&0xff;
             TRACE("#@####C++ ==============tmp[%d] sz[%d]\n",tmp, sz);
             buf[0]= tmp & 0xff;

             memcpy(buf+1, &pDboard->db_upload.CbData, sz);
             break;
		default:
			sz=20;
			break;
	}
    int tmpArr[sz];
    jintArray retArr = env->NewIntArray(sz);

    //TRACE("#@####C++ ==============idx[%d] sz[%d]\n",idx, sz);
    for (int i = 0; i < sz; i++){
    	tmpArr[i]=buf[i];
    }
	env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
	return retArr;
}

JNIEXPORT void JNICALL Java_com_otosone_otosstation_utils_jni_DboardCommand
  (JNIEnv *env, jobject obj, jintArray msg)
 {
    Uint8 buffer[64];
    jint *intArray = env->GetIntArrayElements(msg, NULL);
    if (intArray == NULL)  return ;
    jsize len = env->GetArrayLength(msg);
    for (int i = 0; i < len; i++)   	buffer[i]=intArray[i];
    pDboard->Tmessage(buffer, len);
    env->ReleaseIntArrayElements(msg, intArray, 0);
 }

JNIEXPORT void JNICALL Java_com_otosone_otosstation_utils_jni_DboardGetInformation
   (JNIEnv *env, jobject obj, jint kind)
  {
  	int sz=sizeof(pDboard->db_info);
	Uint8 buf[32];
	Uint8 buffer[8];
	buffer[0]=0xDE;
	buffer[1]=0xAA;
	buffer[2]=0x08;
	buffer[3]=0x00;
	buffer[4]=kind;//0x10: Dboard information  00~0B  C board information  0x0F==all
	buffer[5]=0x55;
	buffer[6]=0x21;
	buffer[7]=0XFD;
 	pDboard->Tmessage(buffer, 8);
	return ;
  }

JNIEXPORT void JNICALL Java_com_otosone_otosstation_utils_jni_ShellCommand (JNIEnv *env, jobject obj, jstring jstr)
{
    char buf[128]={0};
    //string str_command;
    jboolean isCopy;
    const char *str = env->GetStringUTFChars(jstr, &isCopy);
   if (isCopy == JNI_TRUE) {
       memcpy(buf, str, 128);
        env->ReleaseStringUTFChars(jstr, str);
   }
    //TRACE("#@####C++ ==============jstr[%s] \n",buf);
	pDboard->send_debug(buf);
  // env->ReleaseStringUTFChars( jstr, str);

}


JNIEXPORT void JNICALL Java_com_otosone_otosstation_utils_jni_ContolCboard(JNIEnv *env, jobject obj, jint cid, jint what, jint value)
{
    char i;
    Uint8 bd=0x01;//1:cbd  -->0:dbd
    char buf[32];
    TRACE("#@####C++ ============== cid[%d] what[%d] value[%d]\n",cid, what, value);
    sprintf(buf,"!%02d%02d%02d%03d@", (Uint8)bd, (Uint8)cid, (Uint8)what, (Uint8)value);
    TRACE("#@#DDD buf[%s]\n",buf);

   	pDboard->Tmessage((Uint8*)buf, 12);
   	for(i=0; i<12; i++) TRACE("#@####C++ ==============[%d] buf[%x]\n",i, buf[i]);
}

JNIEXPORT void JNICALL Java_com_otosone_otosstation_utils_jni_ControlDboard  (JNIEnv *env, jobject obj, jint what, jint value)
{
    char i;
    Uint8 bd=0x00;//1:cbd  -->0:dbd
    Uint8 cid=0x00;
    char buf[32];
    TRACE("#@####C++ ============== cid[%d] what[%d] value[%d]\n",cid, what, value);
    sprintf(buf,"!%02d%02d%02d%03d@", (Uint8)bd, (Uint8)cid, (Uint8)what, (Uint8)value);
    TRACE("#@#DDD buf[%s]\n",buf);
   	pDboard->Tmessage((Uint8*)buf, 12);
   	for(i=0; i<12; i++) TRACE("#@####C++ ==============[%d] buf[%x]\n",i, buf[i]);

}

JNIEXPORT void JNICALL Java_com_otosone_otosstation_utils_jni_CanCommand(JNIEnv *env, jobject obj, jint id, jbyteArray data)
{
    int i;
    Uint8 buf[8]={0,};
    jbyte* byteArray = env->GetByteArrayElements( data, NULL);
    if (byteArray == NULL)    return;
    int bufCnt = env->GetArrayLength(data);

    long ext_add=0x0CECEF00;
    if(id==0xAAAAAAAA) ext_add=id;
    else ext_add =ext_add +(id&0xff);

    if(bufCnt>8)bufCnt=8;
    memcpy(buf,byteArray,bufCnt);
    pDboard->CAN_Tx(ext_add, buf, 8);
}

JNIEXPORT jintArray JNICALL Java_com_otosone_otosstation_utils_jni_CanGetCellvoltage (JNIEnv *env, jobject obj, jint id)
{
	int sz = 20;
    int tmpArr[20];
    jintArray retArr = env->NewIntArray(20);

    for (int i = 0; i < sz; i++){
    	tmpArr[i]= pDboard->cbd_data[id].bms_value.cell_votage[i];
    }
	env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
	return retArr;
}
JNIEXPORT jintArray JNICALL Java_com_otosone_otosstation_utils_jni_GetCboardData (JNIEnv *env, jobject obj, jint cid)
{
	int i;
	Uint8 buf[1024];
	int sz = sizeof(pDboard->cbd_data[0]);
    int tmpArr[sz];
    memcpy(buf, &pDboard->cbd_data[cid],sz);
    jintArray retArr = env->NewIntArray(sz);
    for (i = 0; i < sz; i++){
    	tmpArr[i]= buf[i];
    }
	env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
	return retArr;
}

JNIEXPORT jintArray JNICALL Java_com_otosone_otosstation_utils_jni_GetRequestData (JNIEnv *env, jobject obj, jint bd_id)
{
	int i;
	Uint8 buf[16];
	int sz = 8;
    int tmpArr[sz];
    memcpy(buf, &pDboard->can_request_data[bd_id],8);
    jintArray retArr = env->NewIntArray(sz);
    for (i = 0; i < sz; i++){
    	tmpArr[i]= buf[i];
    }
	env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
	return retArr;
}

JNIEXPORT jintArray JNICALL Java_com_otosone_otosstation_utils_jni_GetNFCData(JNIEnv *env, jobject obj)
{
	int i;
	Uint8 buf[128];
	int sz = pDboard->nfc_rcv_buf[4];
    int tmpArr[sz];

    memcpy(buf, &pDboard->nfc_rcv_buf,sz);
    jintArray retArr = env->NewIntArray(sz);
    for (i = 0; i < sz; i++) 	tmpArr[i]= buf[i];
	env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
	return retArr;
}
JNIEXPORT jint JNICALL Java_com_otosone_otosstation_utils_jni_GetFcFlag (JNIEnv *env, jobject obj)
{
    int fc_flag = FC_Flag;
    FC_Flag=0;
    return fc_flag;
}
