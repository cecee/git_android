#ifndef __DBOARD_CONTROL_H__
#define __DBOARD_CONTROL_H__

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <iostream>
#include <vector>


#define MAX_SIZE 1024 // max size of queue

typedef unsigned short      Uint16;
typedef unsigned char       Uint8;
typedef unsigned int         Uint32;


typedef struct tagstcbFault{
	Uint8 fault_comm_mc: 1;
	Uint8 fault_comm_cc: 1;
	Uint8 fault_comm_bms: 1;
	Uint8 fault_onoff_fan: 1;
	Uint8 fault_onoff_heater: 1;
	Uint8 fault_onoff_servoL1: 1;
	Uint8 fault_onoff_servoL2: 1;
	Uint8 fault_alarm_bms: 1;
	Uint8 fault_alarm_cc: 1;
	Uint8 fault_onoff_door: 1;
	Uint8 xx10: 1;
	Uint8 xx11: 1;
	Uint8 xx12: 1;
	Uint8 xx13: 1;
	Uint8 xx14: 1;
	Uint8 xx15: 1;
}CBIT_FAULT;

typedef union tagcbuncAlarm
{
	Uint16 value;
	CBIT_FAULT b;
}CBOARD_FAULT;


typedef struct tag_d1
{
	Uint8 soc;
	Uint8 Low_PackVoltage;
	Uint8 High_PackVoltage;
	Uint8 Low_PackCurrent;
	Uint8 High_PackCurrent;
	Uint8 Low_Temperature;
	Uint8 High_Temperature;
	Uint8 BattCapa;
}CAN1_BMS1;

typedef struct tag_d2
{
	Uint8 year;
	Uint8 month;
	Uint8 day;
	Uint8 factory;//nation(4bit) factory(4bit)
	Uint8 Low_errcode;
	Uint8 High_errcode;
	Uint8 Low_serial;
	Uint8 High_serial;
}CAN1_BMS2;

typedef struct tag_d3
{
	Uint8 Low_OutVoltage;
	Uint8 High_OutVoltage;
	Uint8 Low_OutCurrent;
	Uint8 High_OutCurrent;
	Uint8 Low_AwdVoltage;
	Uint8 High_AwdVoltage;
	Uint8 Low_AwdCurrent;
	Uint8 High_AwdCurrent;
}CAN1_CHG1;

typedef struct tagstcbdStatus{
	Uint8 batt_exist: 1;
	Uint8 bms_exist: 1;
	Uint8 chg_exist: 1;
	Uint8 df80_oc: 1;
	Uint8 sol0_oc: 1;
	Uint8 sol1_oc: 1;
	Uint8 sol2_oc: 1;
	Uint8 sol3_oc: 1;
}BIT_CBD_STATUS;

typedef struct tagstcbAlarm//reading data
{
	Uint8 alarm_bms: 1;
	Uint8 alarm_cc: 1;
	Uint8 alarm_fan: 1;
	Uint8 alarm_temp: 1;
	Uint8 o_fan: 1;
	Uint8 o_heater: 1;
	Uint8 response: 1;
	Uint8 b7: 1;
}CBIT_ALARM;

typedef struct tagstcbdContolBit{
	Uint8 charging_on: 1;
	Uint8 fan_on: 1;
	Uint8 heater_on: 1;
	Uint8 cmanual: 1;
	Uint8 lock_b0: 1;
	Uint8 lock_b1: 1;
	Uint8 color_b0: 1;
	Uint8 color_b1: 1;
}BIT_RCV_CONTROL;

typedef struct tagst_chgstatusBit{
	Uint8 charger_temp: 1;
	Uint8 working_b0: 1;
	Uint8 working_b1: 1;
	Uint8 comm: 1;
	Uint8 b4: 1;
	Uint8 b5: 1;
	Uint8 b6: 1;
	Uint8 b7: 1;
}BIT_CHG_STATUS;

typedef union tag_chgstatus
{
	Uint8 value;
	BIT_CHG_STATUS b;
}CBD_CHG;

typedef union tagcbunAlarm
{
    Uint8 value;
	CBIT_ALARM b;
}CBD_ALARM;

typedef union taguncbdsta
{
	Uint8 value;
	BIT_CBD_STATUS b;
}CBD_STATUS;

typedef union taguncbdcont
{
	Uint8 value;
	BIT_RCV_CONTROL b;
}RCV_CONTROL;

typedef struct tag_d4
{
	CBD_STATUS  status; //1byte
	CBD_ALARM cbAlarm;//1byte
	CBD_CHG ChargerStatus;
	RCV_CONTROL rcv;
	Uint8 dip;
	signed char tmp36_temp;
	Uint8 bms_add;
	Uint8 fw_version;
}CAN1_MBD1;


typedef struct tag_upcan
{
	CAN1_BMS1 B1;
	CAN1_BMS2 B2;
	CAN1_CHG1 C1;
	CAN1_MBD1 M1;
}CAN1_UP;
//-----------------------------------------------------
typedef struct tag_cbinf0
{
	Uint16 header;
	Uint16 size;
	Uint8 node;
	CAN1_UP cbinfo;
	Uint8 crc;
	Uint8 dummy;
	Uint8 end;
}CB_INFOR;

typedef struct tag_cbinf0_all
{
	Uint16 header;
	Uint16 size;
	CAN1_UP cbinfo[12];
	Uint8 crc;
	Uint8 end;
}CB_INFOR_ALL;

///////////////////////////////
typedef struct tagstCurData
{
	Uint8 alight_oc: 1;
	Uint8 monitor_oc: 1;
	Uint8 ac_fail_oc: 1;
	Uint8 dc_fail_oc: 1;
	Uint8 bat_fail_oc: 1;
	Uint8 ovc_fail_oc: 1;
	Uint8 door_oc: 1;
	Uint8 aheater1: 1;
	Uint8 aheater2: 1;
	Uint8 candn: 1;//candn통신
	Uint8 rs232: 1; //rs232통신
	Uint8 alarm_temp: 1;
	Uint8 th_status1: 1;
	Uint8 th_status2: 1;
	Uint8 th_status3: 1;
	Uint8 x15: 1;
}BIT_CUR_STATUS;


typedef union tagunionStatusport
{
	Uint16 value;
	BIT_CUR_STATUS b;
}STATUS;

typedef struct tagbdStatusport
{
	STATUS status; //2
	signed char temper[3];//3
	signed char humidi[3];//3
	signed char tmp36_0;//1
	signed char tmp36_1;//1
	Uint16 cb_exist;//2
	Uint16 main_V;//2
	Uint16 main_A;//2
	Uint16 main_W;//2
}D_STATUS;


typedef struct tagstrdinfo
{
	Uint16  header;//2
	Uint16  size;//2
	D_STATUS Status;//10
	Uint8  crc;//1
	Uint8  end;//1
}DB_INFO;
///--------------------------------------------

typedef struct tag_bdcontrols
{
	Uint8 allowed_low_voltage;
	Uint8 allowed_high_voltage;
	Uint8 allowed_low_current;
	Uint8 allowed_high_current;
	RCV_CONTROL rcv;
	Uint8 heater_on_temp;
	Uint8 fan_on_temp;
	uint8_t x7;
}CBD_CONTROL;

typedef struct tagstrcinfo //18
{
	Uint16  header;//2
	Uint16  size; //2
	Uint8  cbd_id;//1
	Uint8  d_heater_temp;//1
	Uint8  d_control;//1
	Uint8  dummy;//1
	CBD_CONTROL	control; //8
	Uint8  crc;//1
	Uint8  end;//1
}CB_COMMAD;
//=================================================

typedef struct _DHT11
{
	//signed integer
	Uint16 temper;
	Uint16 humidi;
}DHT11;

typedef struct _RELAY_BIT
{
	Uint8 ac_light: 1;
	Uint8 ac_monitor: 1;
	Uint8 ac_heaterA: 1;
	Uint8 ac_heaterB: 1;
}RELAY_BIT;

typedef struct dbd_fan_tag_bit
{
	Uint8 fan_f1: 1;
	Uint8 fan_f2: 1;
	Uint8 fan_f3: 1;
	Uint8 fan_f4: 1;
    Uint8 fan_r1: 1;
	Uint8 fan_r2: 1;
	Uint8 fan_r3: 1;
	Uint8 fan_r4: 1;
}FAN_BIT;


typedef struct dbd_ssensor_tag_bit
{
	Uint16 door: 1;
	Uint16 slope1: 1;
	Uint16 slope2: 1;
	Uint16 shock1: 1;
	Uint16 shock2: 1;
	Uint16 nfc_led: 1;
	Uint16 mood: 1;
 	Uint16 dht1: 1;
	Uint16 dht2: 1;
	Uint16 dht3: 1;
}SENSOR_BIT;

typedef struct dbd_pahalarm_tag_bit
{
	Uint8 ac_fail: 1;
	Uint8 dc_fail: 1;
	Uint8 buckup_bat_fail: 1;
	Uint8 ovc: 1;
}PSU_ALARM_BIT;

typedef struct _DBD_STATUS
{
	RELAY_BIT bRelay;
	FAN_BIT bFanCtrl;
    FAN_BIT bFanAlarm;
	SENSOR_BIT bSensor;
	PSU_ALARM_BIT bPsuAlarm;
	DHT11 dht[3];
	Uint8 buckRGB[6];
	Uint16 tmp36_0;
	Uint16 tmp36_1;
	Uint16 cb_exist;
	Uint16 cds;
	Uint16 dimmer;
	Uint16 main_V;
	Uint16 main_A;
	Uint16 main_W;
}DBD_STATUS;

//-------------------------------------
typedef struct _BIT_MOTOR_STATUS{
	uint8_t D1_ctrl: 1;
	uint8_t D2_ctrl: 1;
	uint8_t B1_ctrl: 1;
	uint8_t B2_ctrl: 1;
 	uint8_t D1_err: 1;
	uint8_t D2_err: 1;
	uint8_t B1_err: 1;
	uint8_t B2_err: 1;
 	uint8_t D1_pos: 2;
	uint8_t D2_pos: 2;
	uint8_t B1_pos: 2;
	uint8_t B2_pos: 2;
}BIT_MOTOR_STATUS;

typedef struct tag_sol_status{
	BIT_MOTOR_STATUS b;
}SOLENOID_VALUE;

//BMS------------------------------------

typedef struct tag_bms_err_bit
{
	Uint32 uv_warning: 1;
	Uint32 uv_error: 1;
	Uint32 ov_warning: 1;
	Uint32 ov_error: 1;
	Uint32 ot_warning: 1;
	Uint32 ot_error: 1;
	Uint32 ut_warning: 1;
	Uint32 ut_error: 1;

	Uint32 oc_warning_chg: 1;
	Uint32 oc_error_chg: 1;
	Uint32 oc_warning_dis: 1;
	Uint32 oc_error_dis: 1;
	Uint32 comm_error: 1;
	Uint32 by1bit5: 1;
	Uint32 by1bit6: 1;
	Uint32 by1bit7: 1;

	Uint32 pack_uv_warning: 1;
	Uint32 pack_uv_protect: 1;
	Uint32 pack_ov_warning: 1;
	Uint32 pack_ov_protect: 1;
	Uint32 by2bit4: 1;
	Uint32 by2bit5: 1;
	Uint32 by2bit6: 1;
	Uint32 by2bit7: 1;

	Uint32 by3bit0: 1;
	Uint32 by3bit1: 1;
	Uint32 by3bit2: 1;
	Uint32 by3bit3: 1;
	Uint32 by3bit4: 1;
	Uint32 by3bit5: 1;
	Uint32 warning: 1;
	Uint32 error: 1;
}BMS_ERR_BIT;


typedef struct tag_regacy_bms_err_bit
{
	Uint16 fault_dis_htemp: 1;
	Uint16 fault_dis_ltemp: 1;
	Uint16 fault_cha_htemp: 1;
	Uint16 fault_cha_ltemp: 1;
	Uint16 fault_cha_ocurr: 1;
	Uint16 fault_dis_ocurr: 1;
	Uint16 fault_singlecell_lvolt: 1;
	Uint16 fault_singlecell_hvolt: 1;
//--------------------------------------------
	Uint16 fault_singlecell_dvolt: 1;
	Uint16 x9: 1;
	Uint16 x10: 1;
	Uint16 x11: 1;
	Uint16 x12: 1;
	Uint16 x13: 1;
	Uint16 x14: 1;
	Uint16 x15: 1;
}REGACY_BMS_ERR_BIT;

typedef union tag_uni_regacy_err
{
	Uint16 value;
	REGACY_BMS_ERR_BIT b;
}BMS_REGACY_ERR;

typedef union tag_bms_error_union
{
	Uint8 value[4];
	BMS_ERR_BIT b;
}BMS_ERR;

typedef union _CELL_BAL
{
	Uint8 value[4];
}CELL_BAL;

typedef union _USER_DATA
{
	Uint8 value[3];
}USER_DATA;

typedef union _PACK_COUNTER
{
	Uint8 value[6];
}PACK_COUNTER;

typedef struct tag_bms_value
{
    Uint16 cell_votage[20];//cell_votage[16];
	Uint16 cell_temper[6];//"-"온도 있을수 있음
	Uint16 pack_voltage;
	Uint16 pack_current; //"-"값 있음 전류방향에 따라서
	Uint16 pack_soc;//new: 2byte  old:1byte
	Uint16 pack_temper;//(58)통신[60]MSB

	Uint8 pack_serial;//추가[62]
	Uint8 pack_parallel;//추가
	Uint8 pack_capacity;
	Uint8 pack_year;
	Uint8 pack_month;
	Uint8 pack_day;
	Uint16 pack_nation;
	Uint16 pack_serialNumber;//(68)통신[70-lsb][71-msb]
	Uint16 pack_cycle;//[72-lsb][73-msb]
	USER_DATA pack_userData;//[74-lsb][76-msb]
	PACK_COUNTER pack_Counter;//[77]-[82]
    CELL_BAL pack_cellBancing;//83(MSB) .. 86(LSB)//추가
	BMS_ERR pack_err; //[90-[93]
	BMS_REGACY_ERR regacy_pack_err;
}BMS_VALUE;
//----------------------------------

typedef struct tag_status_bit
{
    Uint16 charging: 1;
    Uint16 bms_err: 1;
    Uint16 chargingONOFF: 1;
    Uint16 batt_exist: 1;   //x3
	Uint16 comm_fail: 1;
	Uint16 ac_fail: 1;
	Uint16 tmp_fail: 1;
	Uint16 fan_fail: 1;
	Uint16 dc_fail: 1;
	Uint16 load_balance: 1;
	Uint16 relay: 1;
	Uint16 dc_sd: 1;
	Uint16 ctrl_fan: 1;     //x12 sps194 내부의 FAN
    Uint16 pBattIn: 1;      //x13
    Uint16 pBattOut: 1;     //x14
    Uint16 bms_enable: 1;   //x15
}SPS_STATUS_BIT;

typedef struct tag_sps_value
{
    uint16_t voltage_setting;
    uint16_t current_setting;
    uint16_t voltage_outer;
    uint16_t voltage_sps;
    uint16_t current_monitor;
    int16_t temperature_inner;//signed short
    int16_t rsev;//signed short
    uint8_t cid;
    uint8_t charging_seg;
    uint16_t  charging_time;
    uint16_t  cversion;
	SPS_STATUS_BIT b;
}SPS_VALUE;


typedef struct _CBD_DATA
{
	BMS_VALUE bms_value;
	SPS_VALUE chg_value;
	SOLENOID_VALUE sol_value;
}CBD_DATA;


typedef struct _DB_UPLOAD
{
	Uint8 sof;
	Uint8 cid;
    Uint16 size;
	DBD_STATUS dStatus;
	CBD_DATA CbData;
	Uint16 crc;
	Uint16 eof;
}DB_UPLOAD;


class Dboard_control
{
    public:

	int recv_cnt;

	DB_INFO db_info;
	DB_INFO db_comm;

	CB_INFOR mCB_info;
	CB_INFOR_ALL cb_all;

	CAN1_UP cb_info[12];
	CBD_DATA  cbd_data[20];

	Uint8 Dcome_sbuf[1024];
	Uint8 Dcome_buffer[1024];


    Uint8 cbd_rcv_buf[16][16][8]={0,};//[cid][seq][data]can 15번 올라온다
    Uint8 dbd_rcv_buf[4][8]={0,}; //[seq][data]can 4번 올라온다
    Uint8 nfc_rcv_buf[128]={0,}; //nfc data
    Uint8 can_request_data[16][8]={0,}; //can one frame
//
    DB_UPLOAD db_upload;

	int front;
	int rear;
	Uint8 queue[MAX_SIZE+1];
	Uint8 master_packet;
	Uint16 packet_sz;
	Uint8 ex_data;

	public:
        void MASTER_OpenUart(int uart);
	    int CAN_Tx(int can_id, Uint8 *data, int length);
		int Tmessage(Uint8 *data, int size);
		void update_info(Uint8 *data);
		void send_debug(char *msg) ;
		void PutSerialBuf(void);
		void Serial_process(Uint8 data);
		Dboard_control();
		~Dboard_control();

};
#endif