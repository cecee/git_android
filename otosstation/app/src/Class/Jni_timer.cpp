#define CLOCKID CLOCK_REALTIME
#define SIG SIGRTMIN

#define PERIOD 10000 //10ms
#include "Jni_timer.h"
#include "extern.h"

Jni_timer::Jni_timer()
{
	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = timer_handler;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIG, &sa, NULL) == -1) return;
/* Block timer signal temporarily */
     sigemptyset(&mask);
     sigaddset(&mask, SIG);
     if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1) return;
	/* Create the timer */
	sev.sigev_notify = SIGEV_SIGNAL;
	sev.sigev_signo = SIG;
	sev.sigev_value.sival_ptr = &timerid;
	if (timer_create(CLOCKID, &sev, &timerid) == -1) return;
	/* Start the timer */
	its.it_value.tv_sec = 0;///freq_nanosecs / 1000000000;
	its.it_value.tv_nsec = PERIOD*1000;//freq_nanosecs % 1000000000;
	its.it_interval.tv_sec = its.it_value.tv_sec;
	its.it_interval.tv_nsec = its.it_value.tv_nsec;
	if (timer_settime(timerid, 0, &its, NULL) == -1) return;
}

 void Jni_timer::timer_handler(int sig, siginfo_t *si, void *uc)
{
	if(pDboard != NULL)
	{
        pDboard->PutSerialBuf();
	}
	return ;
}

