#include "Dboard_control.h"
#include "extern.h"
#include <jni.h>
#include <termios.h>
//#include "jniMain.h"
#include <stdio.h>


Jni_timer* pTimer;
int serial_lock=0;

//extern int SendToJava(int kind);

Dboard_control::Dboard_control()
{
	TRACE("#@#init =======Dboard_control===\n");

	pTimer=new Jni_timer();
	master_packet=0;
	ex_data=0;
 	recv_cnt=0;
	packet_sz=0;
	memset(Dcome_sbuf,0,sizeof(Dcome_sbuf));
	memset(Dcome_buffer,0,sizeof(Dcome_sbuf));
	MASTER_OpenUart(0);
	MASTER_OpenUart(1);
	MASTER_OpenUart(3);
}

Dboard_control::~Dboard_control(){
	TRACE("#@#finished=======Dboard_control===\n");
}

//==============================
void  Dboard_control::MASTER_OpenUart(int uart)
{
   struct termios newtio;
   int TTY;
   char tty[32]={0,};

   switch(uart){
        case 0:
            sprintf(tty,"/dev/ttyS0");
            TTY=TTY_S0;
            break;
        case 1:
            sprintf(tty,"/dev/ttyS1");
            TTY=TTY_S1;
            break;
        case 3:
            sprintf(tty,"/dev/ttyS3");
            TTY=TTY_S3;
            break;
   }
  if(TTY<0)	{
        TTY = open(tty, O_RDWR | O_NOCTTY | O_NONBLOCK);
  }
  else{
        close(TTY);
        TTY = -1;
        TTY = open(tty, O_RDWR | O_NOCTTY | O_NONBLOCK);
   }

    TRACE("#@#MASTER_OpenUart ------uart[%d]  fd[%d]\n",uart, TTY);

    bzero(&newtio, sizeof(newtio)); /* clear struct for new port settings */
    tcgetattr(TTY,&newtio); /* save current serial port settings */
    newtio.c_cflag &=  ~PARENB;        // Make 8n1
    newtio.c_cflag &=  ~CSTOPB;
    newtio.c_cflag &=  ~CSIZE;
    newtio.c_iflag &= ~(INLCR | ICRNL);//0x0D-->0x0A로 변환되어 올라오는문제
    //newtio.c_cflag &= ~ (IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);

    newtio.c_cflag |=  CS8;
    newtio.c_cflag &=  ~CRTSCTS;       // no flow control
    newtio.c_lflag =   0;              // no signaling chars, no echo, no canonical processing
    newtio.c_oflag =   0;              // no remapping, no delays
    newtio.c_cc[VMIN] = 128;           // read doesn't block
    newtio.c_cc[VTIME] = 0;            // 0.5 seconds read timeout

    newtio.c_cflag |=  CREAD | CLOCAL;     // turn on READ & ignore ctrl lines
    newtio.c_iflag &=  ~(IXON | IXOFF | IXANY);// turn off s/w flow ctrl
    newtio.c_lflag &=  ~(ICANON | ECHO | ECHOE | ISIG); // make raw
    newtio.c_oflag &=  ~OPOST;              // make raw

    tcflush(TTY, TCIFLUSH); //TCIOFLUSH
    tcsetattr(TTY,TCSANOW,&newtio);

    switch(uart){
        case 0:
            cfsetospeed(&newtio,B115200);
            TTY_S0=TTY;
            break;
        case 1:
            cfsetospeed(&newtio,B115200);
            TTY_S1=TTY;
            break;
        case 3:
            cfsetospeed(&newtio,B115200);
            TTY_S3=TTY;
            break;
    }
}

int  Dboard_control::Tmessage(Uint8 *data, int size)
{
	int i,j;
	int sum=0;
	Uint8 mbuf[512];
	Uint16 msgID;
	int ok=0;
	msgID=BUILD_UINT16(data[0],data[1]);
    for(i=0;i<16;i++)sum=sum+data[i];
    sum=sum&0xff;
    data[16]=sum;
 	return 0;
}


int  Dboard_control::CAN_Tx(int can_id, Uint8 *data, int length)
{
	int i,j;
	int sum=0;
	Uint16 data_sz =19;//total length
	Uint8 mbuf[512]={0,};
	Uint8 can_data[8]={0,};
	if(length>8)length=8;
	for(i=0;i<length;i++) can_data[i]=data[i];

	mbuf[0]=0xFA;//STX
	mbuf[1]=0x00;//Direction 0: Host to Sub 1:Sub to Host
	mbuf[2]=0x00;//Protocol  0: CAN 1:NFC 2:Other
	mbuf[3]=HI_UINT16(data_sz);//HI Data sz
	mbuf[4]=LO_UINT16(data_sz);//LO Data sz

	mbuf[5]=can_id&0xFF;
	mbuf[6]=(can_id>>8)&0xFF;
	mbuf[7]=(can_id>>16)&0xFF;
	mbuf[8]=(can_id>>24)&0xFF;

    for(i=0;i<8;i++) mbuf[9+i]=can_data[i];
    for(i=0;i<17;i++)sum=sum+data[i];
    sum=sum&0xff;
    mbuf[17]=sum;//crc
    mbuf[18]=0xFB;//ETX
/*
	TRACE("#@#CAN_Tx AA(1)-----mbuf[%02X][%02X][%02X][%02X]-[%02X][%02X][%02X][%02X]-[%02X][%02X][%02X][%02X]-[%02X][%02X][%02X][%02X]-[%02X][%02X][%02X]---\n",
			mbuf[0], mbuf[1],  mbuf[2],  mbuf[3],  mbuf[4],  mbuf[5],  mbuf[6],  mbuf[7],
			mbuf[8], mbuf[9],  mbuf[10], mbuf[11], mbuf[12], mbuf[13], mbuf[14], mbuf[15],
			mbuf[16],mbuf[17], mbuf[18]);
*/
	write(TTY_S3, mbuf, data_sz);
	return 0;
}


void Dboard_control::update_info(Uint8 *data)
{
	Uint16 cb_exist;
	int sz=BUILD_UINT16(data[2],data[3]);
	switch(data[0]){
		case 0xDA:
			memcpy(&db_info, data,sizeof(db_info));
			cb_exist=db_info.Status.cb_exist;
			break;
		case 0xCA:
			memcpy(&mCB_info, data, sizeof(mCB_info));
			break;
		case 0xFA:
			memcpy(&cb_all,data,sizeof(cb_all));
			break;
	}

}

void Dboard_control::send_debug(char *msg)
{
	char data[128]={0,};
	 int len;
	 sprintf(data,"%s\r\n",msg);
	 len = strlen(data);
     write(TTY_S0, data, len);
}

void  Dboard_control::PutSerialBuf(void)
{
	int i;
	int res=0;
	Uint8 mbuf[512]={0,};
	res=read(TTY_S3, mbuf, 512);
	for(i=0;i<res;i++) {
	    Serial_process(mbuf[i]);
	}
}

void Dboard_control::Serial_process(Uint8 data)
{
	int sum=0;
	int i,seq,protocol,Sz,cid;
	int cnt=0;
	static int length=0xff;
	int arg=0xff00;
	int req_arg=0xfa00;
	//TRACE("#@#D[%02x]\n", data);
    if(master_packet==0 && data==0xFA)
    {
        master_packet=1;
        recv_cnt=0;
        memset(Dcome_sbuf,0, sizeof(Dcome_sbuf));
    }
    if(master_packet==1 )
    {
       Dcome_sbuf[recv_cnt++]=data;
       if(recv_cnt==5) length=Dcome_sbuf[4];
       if((data==0xFB) && (recv_cnt==length))
       {
            //TRACE("#@#D[ETX][%d][%d]\n", recv_cnt, length);
            protocol = Dcome_sbuf[1];
            //TRACE("#@#jdSerial FIND [ETX] [%d] rcnt[%d] seq[%02X] cid[%02X] \n",protocol, recv_cnt, Dcome_sbuf[5], Dcome_sbuf[6]);
            switch(protocol){
                case 0: //CAN
                    seq= Dcome_sbuf[5];
                    cid= Dcome_sbuf[6];
                    if(seq==0xAA){//request as like event
                        memcpy(&can_request_data[cid],&Dcome_sbuf[9],8);//request
                        SendToJava( req_arg + cid );
                         break;
                    }
                    memcpy(&cbd_rcv_buf[cid][seq],&Dcome_sbuf[9],8);
                    if(cid==0x10 && seq==0x04){//HVAC block 0-4 5개 x 8 40
                        memcpy(&cbd_data[cid],&cbd_rcv_buf[cid],40);
                        SendToJava( arg + cid );
                    }
                    else if(seq==0x0e){//CBD
                        memcpy(&cbd_data[cid],&cbd_rcv_buf[cid],sizeof(CBD_DATA));
                        SendToJava( arg + cid );
                    }

                    break;
                case 1: //NFC
                    memcpy(nfc_rcv_buf,&Dcome_sbuf[0],length);
                    SendToJava(0xFF80);//NFC
                     break;
            }
            memset(Dcome_sbuf,0, sizeof(Dcome_sbuf));
            master_packet=0;
        }
        if(recv_cnt>32){
             memset(Dcome_sbuf,0, sizeof(Dcome_sbuf));
             master_packet=0;
        }
    }

}



