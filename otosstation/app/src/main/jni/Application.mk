APP_ABI := armeabi armeabi-v7a
APP_OPTIM := release
APP_PLATFORM := android-9
APP_STL := gnustl_static
APP_CPPFLAGS += -fexceptions
APP_CPPFLAGS += -std=c++11
LOCAL_CFLAGS += -O3