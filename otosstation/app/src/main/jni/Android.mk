LOCAL_PATH := $(call my-dir)
$(warning $(LOCAL_PATH))

#==============================================
include $(CLEAR_VARS)

@echo=========================
$(warning $(LOCAL_PATH))
@echo=========================

LOCAL_MODULE := libJnilib
DEFALUT_PATH := ../..
LOCAL_SRC_FILES := ../../Class/jniMain.cpp \
                   ../../Class/Jni_timer.cpp \
                   ../../Class/Dboard_control.cpp \

LCOAL_C_INCLUDES := $(LOCAL_PATH)
LOCAL_C_INCLUDES += ../../Class

#LOCAL_LDLIBS := -ldl -lGLESv1_CM -lGLESv2 -llog -landroid
LOCAL_LDLIBS := -ldl -llog -landroid

include $(BUILD_SHARED_LIBRARY)
