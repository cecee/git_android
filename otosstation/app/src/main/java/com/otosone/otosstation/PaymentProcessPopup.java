package com.otosone.otosstation;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.otosone.otosstation.utils.IBUtil;


/**
 * Created by shootdol on 2017-09-15.
 */
public class PaymentProcessPopup extends Dialog {
    protected final String TAG = getClass().getSimpleName();

    private Context m_context;
    private LinearLayout pop_payment_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        //lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        //lpWindow.dimAmount = 0.0f;
        //getWindow().setAttributes(lpWindow);

        setContentView(R.layout.pop_payment);

        IBUtil.Log(TAG, "onCreate()");

        setLayout();
        setClickListener();

    }

    public PaymentProcessPopup(Context context) {
        super(context , android.R.style.Theme_Translucent_NoTitleBar);
        m_context = context;
    }

    private void setClickListener(){
    }

    public void dialogClose(){
        IBUtil.Log(TAG, "dialogCloses()");
        pop_payment_layout.setVisibility(View.GONE);
        pop_payment_layout.invalidate();
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                dismiss();
            }
        }, 500);
    }

    private void setLayout(){
        pop_payment_layout = (LinearLayout) findViewById(R.id.pop_payment_layout);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_1:
               // Constant.pUpdate.TAG_try=0;
               // Constant.pUpdate.TAG_SEQ = 6;
               // dialogClose();

                break;
        }
        IBUtil.Log(TAG, "KEYCODE onKeyDown: " + keyCode);
        return super.onKeyDown(keyCode, event);
    }

//    private final Runnable mPaymentEndRunnable = new Runnable() {
//        @Override
//        public void run() {
//            while(true) {
//                if(Constant.TAG_flag == 7) {
//                    IBUtil.Log(TAG, "PAY OK!");
//                    break;
//                } else if(Constant.TAG_flag == 0) {
//                    IBUtil.Log(TAG, "PAY FAIL!");
//                    break;
//                } else {
//                    IBUtil.Log(TAG, "PAY WAIT!");
//                    IBUtil.Log(TAG, "#@# Wait tag ["+Constant.TAG_flag+"] Constant.TAG_wait["+ Constant.TAG_wait +"]");
//                }
//                try {
//                    Thread.sleep(500);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//            dialogClose();
//        }
//    };

    public static void paymentPopupClose() {
        //dialogClose();
    }
}
