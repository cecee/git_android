package com.otosone.otosstation.utils;

/**
 * Created by Administrator on 2017-09-20.
 */

public class jni {
    static {
        System.loadLibrary("Jnilib");
    }

    public  void fromJni(int arg) {
        //int cid=0;
        //int[] cdata;
        //System.out.printf("#@#=======QueryFromC ==>[%x]\n", arg);
        if(arg>=0xfa00){
            Constant.pUpdate.FC_flag=arg;
        }

    }


    public native void DboardOpen();
    public native int[] returnedUnsignedShortArr(int idx);
    public native void DboardCommand(int[] arry);
    public native void DboardGetInformation(int kind);
    public native void ShellCommand(String str);
    public native void ContolCboard(int cid, int what, int value);
    public native void ControlDboard(int what, int value);//what 0-5 led_color 6:Front FAN 7:Rear FAN

    public native void CanCommand(int id, byte[] data);
    public native int[] CanGetCellvoltage(int id);
    public native int[] GetCboardData(int id);
    public native int[] GetRequestData(int id);//event
    public native int[] GetNFCData();
    public native int GetFcFlag();

}
