package com.otosone.otosstation.frag;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.otosone.otosstation.R;
import com.otosone.otosstation.StationActivity;
import com.otosone.otosstation.utils.Constant;
import com.otosone.otosstation.utils.IBCircleProgress;
import com.otosone.otosstation.utils.IBUtil;

/**
 * Created by shootdol-tw on 2019-10-01.
 */

public class Frag_D_Info extends Fragment {
    protected final String TAG = getClass().getSimpleName();
    private View view;
    static Context context;

    private IBCircleProgress circle_progress_bar;
    private TextView card_name_txt, info_txt, level_txt;

    private String customer_name = "";
    private int nextstep_flag = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_d_info, viewGroup, false);
        context = view.getContext();
        init();
        return view;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible && isResumed()) {
            IBUtil.Log(TAG, "visible!!!!!!!!!!!!!! ");
            onResume();
        } else {
            IBUtil.Log(TAG, "invisible!!!!!!!!!!!!!!");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IBUtil.Log(TAG, "onResume()");
        Constant.m_frag_tag = IBFrag.FRAG_D_INFO;
    }

    @Override
    public void onPause() {
        super.onPause();
        IBUtil.Log(TAG, "onPause()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IBUtil.Log(TAG, "onDestroy()");
    }

    void init() {
        IBUtil.Log(TAG, "init()");

        setLayout();
        batteryInfo();

        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                ((StationActivity) context).ChangeFragment(IBFrag.FRAG_E_PAY);
            }
        }, 3000);
    }

    public boolean frOnKeyDown(int key_code) {
        boolean ret = false;
        switch (key_code) {
        }
        return ret;
    }

    private void setLayout() {
        card_name_txt = (TextView) view.findViewById(R.id.card_name_txt);
        info_txt = (TextView) view.findViewById(R.id.info_txt);
        level_txt = (TextView) view.findViewById(R.id.level_txt);
        circle_progress_bar = (IBCircleProgress) view.findViewById(R.id.circle_progress_bar);
        card_name_txt.setText(Constant.NFC_CARD_NAME);
    }

    private void batteryInfo() {
        //final SpannableStringBuilder sp = new SpannableStringBuilder(customer_name + getString(R.string.info_battery_check_level));
        //sp.setSpan(new ForegroundColorSpan(0xFFFDED01), customer_name.length()+5, customer_name.length()+12, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //info_txt.append(sp);
        info_txt.setText(customer_name + getString(R.string.info_battery_check_level));

        if(Constant.current_battery_level > 99) {
            level_txt.setTextSize(300);
        }
        level_txt.setText("" + Constant.current_battery_level);
    }
}
