package com.otosone.otosstation.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.media.MediaExtractor;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

public class OtosVideoView extends SurfaceView implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {
//public class KaraokeVideoView extends SurfaceView implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener {
    protected final String TAG = getClass().getSimpleName();

    private String mUrl;
    private MediaPlayer mediaPlayer;
    private boolean surfaceCreated;
    private boolean preparedListener = true;

    private int mSeekTo = 0;

    private PlaybackThread playbackThread;
    private final Object playbackLock = new Object();

    public OtosVideoView(Context context) {
        super(context);
        init();
    }

    public OtosVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        //setZOrderOnTop(true); //always on Top
        getHolder().addCallback(this);
        getHolder().setFormat(PixelFormat.TRANSPARENT);
        mediaPlayer = new MediaPlayer();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        surfaceCreated = false;
        Log.d(TAG, "surfaceDestroyed!!!!");
        stop();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceCreated = true;

        if (playbackThread != null) {
            synchronized (playbackLock) {
                playbackLock.notifyAll();
            }
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "surfaceChanged!!!!");
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.d(TAG, "getDuration!!!!!!!!!!!!!  - " + mediaPlayer.getDuration());
        mediaPlayer.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d(TAG, "onCompletion !!!!!!!!!");
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.d(TAG, "onDraw!!!!!!!!!");
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
    }

//    private void clearSurface(Surface surface) {
//        EglCore eglCore = new EglCore();
//        WindowSurface win = new WindowSurface(eglCore, surface, false);
//        win.makeCurrent();
//        GLES20.glClearColor(0, 0, 0, 0);
//        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
//        win.swapBuffers();
//        win.release();
//        eglCore.release();
//    }

    public void clearSurface() {
        SurfaceHolder holder = getHolder();
        holder.setFormat(PixelFormat.TRANSPARENT);
        holder.setFormat(PixelFormat.OPAQUE);
        mediaPlayer.setDisplay(holder);
    }

    public void setPreparedListener(boolean pre) {
        preparedListener = pre;
    }

    public void load(String url, int msec) {
        mUrl = url;
        mSeekTo = msec;
        playbackThread = new PlaybackThread();
        playbackThread.start();
    }

    private void play() {
        mediaPlayer.setDisplay(getHolder());

        if (mUrl == null || mUrl.equals("")) {
            return;
        }

        try {
            Log.d(TAG, "mUrl - " + mUrl);
            mediaPlayer.setDataSource(mUrl);
            //if(preparedListener) mediaPlayer.setOnPreparedListener(this);
            //mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.prepareAsync();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            Log.d(TAG, "play()1 - " + e.getMessage());
        } catch (IllegalStateException e) {
            e.printStackTrace();
            Log.d(TAG, "play()2 - " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "play()3 - " + e.getMessage());
        }
    }

    private class PlaybackThread extends Thread {
        @Override
        public void run() {
            super.run();

            if (!surfaceCreated) {
                synchronized (playbackLock) {
                    try {
                        playbackLock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            play();
        }
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public void pause() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                try {
                    mediaPlayer.pause();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            } else {
                resume();
                Log.d(TAG, "Playback has already stopped. Pause ignored");
            }
        }
    }

    public void resume() {
        if (mediaPlayer != null) {
            if (!mediaPlayer.isPlaying()) {
                try {
                    mediaPlayer.start();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            } else {
                Log.d(TAG, "Playback already in progress. Resume ignored");
            }
        }
    }

    public void stop() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.stop();
                mediaPlayer.reset();
//                SurfaceHolder holder = getHolder();
//                Surface surface = holder.getSurface();
//                clearSurface(surface);

                //초기화 안해주면 Prepare Async failed.: status=0x80000000
                //그리고 영상 중간중간 렉 생기는 경우 있음
                mediaPlayer.release();
                mediaPlayer = null;
                mediaPlayer = new MediaPlayer();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
    }

    public void  nextFile(String url, int msec){
        try {
            stop();
            load(url, msec);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void setVolume (float leftVolume, float rightVolume) {
        mediaPlayer.setVolume(leftVolume, rightVolume);
    }

    public int  getAudioTrackCount(){
        int numTracks = -1;
        try {
            MediaExtractor extractor = new MediaExtractor();
            extractor.setDataSource(mUrl);
            numTracks = extractor.getTrackCount();
            Log.d(TAG, "Audio Tracks : " + numTracks);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return numTracks;
    }

    public void prevSec(int sec) {
        int currentTime, totalTime;
        try {
            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    totalTime = mediaPlayer.getDuration() / 1000;
                    currentTime = mediaPlayer.getCurrentPosition() / 1000;
                    currentTime = currentTime - sec;
                    if (currentTime < 0) currentTime = 0;
                    mediaPlayer.seekTo(currentTime * 1000);
                }
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void nextSec(int sec) {
        int currentTime, totalTime;
        try {
            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    totalTime   = mediaPlayer.getDuration() / 1000;
                    currentTime = mediaPlayer.getCurrentPosition() / 1000;
                    currentTime = currentTime + sec;
                    if(currentTime > totalTime) currentTime = totalTime;
                    mediaPlayer.seekTo(currentTime * 1000);
                }
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public int getCurrentPosition() {
        int msec = 0;
        try {
            if (mediaPlayer != null) {
                msec = mediaPlayer.getCurrentPosition();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        return msec;
    }

    public int getDuration() {
        int msec = 0;
        try {
            if (mediaPlayer != null) {
                msec = mediaPlayer.getDuration();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        return msec;
    }

    public void seekTo(int msec) {
        int totalTime;
        try {
            if (mediaPlayer != null) {
                totalTime   = mediaPlayer.getDuration();
                if(msec > totalTime) msec = totalTime;
                mediaPlayer.seekTo(msec);
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }
}
