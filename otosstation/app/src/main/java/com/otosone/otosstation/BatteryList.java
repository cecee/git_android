package com.otosone.otosstation;

/**
 * Created by shootdol on 2017-09-11.
 */
public class BatteryList {
    private int battery_id;
    private int percent;
    private int remainTime;
    private boolean isOpen;
    private boolean isEmpty;

    public int getBattery_id() {
        return battery_id;
    }
    public void setBattery_id(int battery_id) {
        this.battery_id = battery_id;
    }

    public int getPercent() { return percent; }
    public void setPercent(int percent) {
        this.percent = percent;
    }

    public int getRemainTime() { return remainTime; }
    public void setRemainTime(int remainTime) {
        this.remainTime = remainTime;
    }

    public boolean getGateOpen() { return isOpen; }
    public void setGateOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public boolean getGateEmpty() { return isEmpty; }
    public void setGateEmpty(boolean isEmpty) {
        this.isEmpty = isEmpty;
    }
}