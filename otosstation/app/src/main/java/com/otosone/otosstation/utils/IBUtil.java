package com.otosone.otosstation.utils;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import com.otosone.otosstation.BatteryList;
import com.otosone.otosstation.frag.Frag_A_HomeMenu;
import com.otosone.otosstation.frag.Frag_B_Insert;
import com.otosone.otosstation.frag.Frag_C_Check;
import com.otosone.otosstation.frag.Frag_D_Info;
import com.otosone.otosstation.frag.Frag_E_Pay;
import com.otosone.otosstation.frag.Frag_F_Take;
import com.otosone.otosstation.frag.Frag_G_End;
import com.otosone.otosstation.frag.IBFrag;

import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by shootdol on 2017-01-16.
 */
public class IBUtil {
    private static final String TAG = "IBUtil";

    public static void screenshot (View view) {
        view.setDrawingCacheEnabled(true);
        Bitmap scrreenshot = view.getDrawingCache();
        String filename = "screenshot.png";

        try{
            File f = new File(Environment.getExternalStorageDirectory(),filename);
            f.createNewFile();
            OutputStream outStream = new FileOutputStream(f);
            scrreenshot.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.close();
        }catch( IOException e){
            e.printStackTrace();
        }
        view.setDrawingCacheEnabled(false);
    }

    public static int getRandom(int min, int max) {
        int mResult = 0;
        try {
            Random mRand = new Random();
            mResult = mRand.nextInt(max) + min;
            Log.e(TAG, "getRandom : " + mResult);
        } catch (Exception e) {
            Log.e(TAG, "getRandom : " + e.getMessage());
        }
        return mResult;
    }

    @NonNull
    public static String runProcess(String execStr, boolean display) throws RuntimeException {
        StringBuffer output = new StringBuffer();
        try {
            if(execStr.length() != 0) {
                Process process = Runtime.getRuntime().exec(execStr);
                Log.e(TAG, "execStr : " + execStr);
                if (display) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    int read;
                    char[] buffer = new char[4096];
                    while ((read = reader.read(buffer)) > 0) {
                        output.append(buffer, 0, read);
                    }
                    reader.close();
                    Log.e(TAG, "runProcess OK: " + output.toString());
                }
                process.waitFor();
                return output.toString();
            }
        } catch (IOException e) {
            Log.e(TAG, "runProcess ERR1: " + e.getMessage());
        } catch (InterruptedException e) {
            Log.e(TAG, "runProcess ERR2: " + e.getMessage());
        }
        return output.toString();
    }

    public static void Log(String tag, String msg) {
        try {
            Log.e(tag, "#@#" + msg);
        } catch (Exception e) {
            Log.e(TAG, "Log : " + e.getMessage());
        }
    }

    public static boolean FileExtensionFilter(File dir, String name) {
        return (name.endsWith(".m") || name.endsWith(".M"));
    }

    public static boolean isFileExists(String path) {
        try {
            File files = new File(path);
            if(files.exists()==true) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            Log.e(TAG, "isFileExists : " + e.getMessage());
        }
        return false;
    }

    public static String getRandomString(int length) {
        StringBuffer buffer = new StringBuffer();
        Random random = new Random();

        String chars[] = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z".split(",");

        for (int i = 0; i < length; i++) {
            buffer.append(chars[random.nextInt(chars.length)]);
        }
        return buffer.toString();
    }

    public static int spToPx(Context context, float sp) {
        final float scale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (sp * scale + 0.5f);
    }

    public static int dp2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    public static void recv_submqtt(String topic) {
        Log.d(TAG, "#@#mqtt subscribe_TOPIC : " + topic);
        try {
            Constant.pahoMqttClient.subscribe(Constant.mqttclient, topic, 1);
        } catch (MqttException e) {
            Log.d(TAG, "subscribe_" + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void send_pubmqtt(String msg, String topic) {
        if (!msg.isEmpty()) {
            try {
                Constant.pahoMqttClient.publishMessage(Constant.mqttclient, msg, 1, topic);
            }
            catch (MqttException e)
            {
                Log.d(TAG, "MqttException ");
                e.printStackTrace();
            }
            catch (UnsupportedEncodingException e) {
                Log.d(TAG, "UnsupportedEncodingException ");
                e.printStackTrace();
            }
        }
    }

    public static double getVersion(Context context) {
        double version = 0;
        try {
            PackageInfo i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String tmp = i.versionName;// + i.versionCode;
            try {
                version = Double.parseDouble(tmp);
            } catch(NumberFormatException nfe) {
                IBUtil.Log(TAG, "Could not parse " + nfe);
            }
        } catch(PackageManager.NameNotFoundException e) { }

        return version;
    }

    public static void tagSound(Context context) {
        IBUtil.Log(TAG, "========tagSound !!!!");
        Uri _sound = RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_NOTIFICATION);

        MediaPlayer mp = new MediaPlayer();
        try {
            mp.setVolume(1.0f, 1.0f);
            //mp.setDataSource(context, Settings.System.DEFAULT_ALARM_ALERT_URI);
            mp.setDataSource(context, _sound);
            mp.setLooping( false );
            mp.prepare();
        } catch( IOException e ) {}
        mp.start();
    }

    public static Map<String, String> getCpuInfoMap() {
        Map<String, String> map = new HashMap<String, String>();
        try {
            Scanner s = new Scanner(new File("/proc/cpuinfo"));
            while (s.hasNextLine()) {
                String[] vals = s.nextLine().split(": ");
                if (vals.length > 1) map.put(vals[0].trim(), vals[1].trim());
            }
            s.close();
        } catch (Exception e) {
            IBUtil.Log("getCpuInfoMap", Log.getStackTraceString(e));
        }
        return map;
    }

    public static String getIPAddress(int lan) {
        String chk, addr = "";
        if (lan == 0)
            chk = "ip addr show wlan0 | grep 'inet ' | busybox cut -d' ' -f6|busybox cut -d/ -f1";
        else chk = "ip addr show eth0 | grep 'inet ' | busybox cut -d' ' -f6|busybox cut -d/ -f1";
        //else chk = "ls -al";

        try {
            //String line;
            //boolean r = false;
            String[] cmd = {"sh", "-c", chk};
            Process p = Runtime.getRuntime().exec(cmd);

            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            addr = input.readLine();
            input.close();
        } catch (IOException e) {
            IBUtil.Log(TAG, "Runtime Error: " + e.getMessage());
            e.printStackTrace();
        }

        //Log.d("#@# Exception11222", "IP ADDRESS__" + addr + "__");
        return addr;
    }

    public static void noti_msg(Activity act, String msg) {
        try {
            StationInfoBar.removePlayInfoToast(true);
            StationInfoBar.showNormalInfo(act, 0, msg, IBToast.PRIORITY_NORMAL, false);
            IBUtil.Log(TAG, "noti_msg:" + msg);

        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    public static boolean frOnKeyDown(int key_code, Fragment frag) {
        boolean ret = false;
        if (Constant.m_frag_tag == -1) return ret;
        IBUtil.Log(TAG, "KEYCODE frOnKeyDown: " + key_code + "-fragno:" + Constant.m_frag_tag);
        try {
            switch (Constant.m_frag_tag) {
                case IBFrag.FRAG_A_HOMEMENU:
                    ret = ((Frag_A_HomeMenu) frag).frOnKeyDown(key_code);
                    break;
                case IBFrag.FRAG_B_INSERT:
                    ret = ((Frag_B_Insert) frag).frOnKeyDown(key_code);
                    break;
                case IBFrag.FRAG_C_CHECK:
                    ret = ((Frag_C_Check) frag).frOnKeyDown(key_code);
                    break;
                case IBFrag.FRAG_D_INFO:
                    ret = ((Frag_D_Info) frag).frOnKeyDown(key_code);
                    break;
                case IBFrag.FRAG_E_PAY:
                    ret = ((Frag_E_Pay) frag).frOnKeyDown(key_code);
                    break;
                case IBFrag.FRAG_F_TAKE:
                    ret = ((Frag_F_Take) frag).frOnKeyDown(key_code);
                    break;
                case IBFrag.FRAG_G_END:
                    ret = ((Frag_G_End) frag).frOnKeyDown(key_code);
                    break;
            }
        } catch (Exception e) {
        }

        return ret;
    }

    public static Fragment ChangeFragment(Context context, int menu, int fragid, FragmentTransaction fragTran) {
        Fragment frag;
        switch (menu) {
            default:
            case IBFrag.FRAG_A_HOMEMENU:
                frag = new Frag_A_HomeMenu();
                break;
            case IBFrag.FRAG_B_INSERT:
                frag = new Frag_B_Insert();
                break;
            case IBFrag.FRAG_C_CHECK:
                frag = new Frag_C_Check();
                break;
            case IBFrag.FRAG_D_INFO:
                frag = new Frag_D_Info();
                break;
            case IBFrag.FRAG_E_PAY:
                frag = new Frag_E_Pay();
                break;
            case IBFrag.FRAG_F_TAKE:
                frag = new Frag_F_Take();
                break;
            case IBFrag.FRAG_G_END:
                frag = new Frag_G_End();
                break;
        }

        //FragmentTransaction fragTran = getFragmentManager().beginTransaction();
        //fragTran.replace( R.id.menu_frag, frag );
        fragTran.replace(fragid, frag);
        //fragTran.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        //fragTran.addToBackStack(null);
        fragTran.commit();

        Constant.m_frag_tag = menu;

        IBUtil.Log(TAG, "ChangeFragment fragNo_" + menu);
        return frag;
    }

    public static void setBatteryStatusList() {
        if (Constant.m_frag_tag == IBFrag.FRAG_A_HOMEMENU) {
            int ii;
            Constant.m_battery_status_list.clear();

            for (ii = 0; ii < Constant.pUpdate.BUCK_CNT; ii++) {
                BatteryList s1 = new BatteryList();
                s1.setBattery_id(ii);
                // s1.setPercent(Const.cinfo[ii][0][0]);
//            IBUtil.Log(TAG, "Battery_Empty_"  + Constant.C_Update.get_bucket_empty(ii));
                if (Constant.pUpdate.get_bucket_empty(ii) == 0) {
                    s1.setGateEmpty(true);
                } else {
                    s1.setPercent(Constant.pUpdate.get_bucket_soc(ii));
                    s1.setGateOpen(false);
                    s1.setRemainTime(40);
                }
                Constant.m_battery_status_list.add(s1);

                //3x3 배열중 첫라인 두번째는 버킷아님. 모니터
//                if (ii == 0) {
//                    Constant.m_battery_status_list.add(null);
//                }
            }

            Constant.m_battery_status_adapter.notifyDataSetChanged();
        }
    }

    public static String getWhatKindOfNetwork(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return "WIFI";
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_ETHERNET) {
                return "ETH";
            }
        }
        return "NONE";
    }
}
