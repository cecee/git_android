package com.otosone.otosstation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.otosone.otosstation.utils.IBUtil;

import java.util.ArrayList;

/**
 * Created by shootdol-tw on 2017-06-22.
 */

public class BatteryListAdapter extends ArrayAdapter<BatteryList> {
    protected final String TAG = getClass().getSimpleName();
    private Context mContext = null;
    private ArrayList<BatteryList> items;
    private int mtype = 0;

    public BatteryListAdapter(Context context, int textViewResourceId, ArrayList<BatteryList> items) {
        super(context, textViewResourceId, items);
        this.items = items;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public BatteryList getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setType(int type) {
        mtype = type; // 0: battery info 1: replace battery
    }

    private class ViewHolder {
        public LinearLayout battery_percent_layout;
        public TextView battery_percent_txt;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_batterylist, null);

            holder.battery_percent_layout = (LinearLayout) convertView.findViewById(R.id.battery_percent_layout);
            holder.battery_percent_txt = (TextView) convertView.findViewById(R.id.battery_percent_txt);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //System.out.printf("##@#-----------getView----------[%d] position[%d]\n",items.size(), position);
        if(items.size() <= position) {
            //FIXME:가끔 리스트 에러 확인용
            //java.lang.IndexOutOfBoundsException: Invalid index 0, size is 0
            //at java.util.ArrayList.throwIndexOutOfBoundsException(ArrayList.java:255)
            IBUtil.Log(TAG, "!!!!!!!!!!!!!! BatteryListAdapter index err_" + items.size() + " - " + position);
            return convertView;
        }

        final BatteryList p = items.get(position);
        final int pos = position;

        try {
            if (p != null) {
                if(mtype == 0) {
                   // System.out.printf("##@#-----------getView----------[%b] position[%d]\n",p.getGateEmpty(), position);
                    if (p.getGateEmpty() == true) {
                        holder.battery_percent_layout.setBackgroundResource(R.drawable.bg_gray_rounded);
                        holder.battery_percent_txt.setText(mContext.getString(R.string.empty_battery));
                    }
                    else
                        {
                        if (p.getPercent() < 100) {
                            holder.battery_percent_layout.setBackgroundResource(R.drawable.bg_green_rounded);
                            holder.battery_percent_txt.setText(p.getPercent() + "%");
                            holder.battery_percent_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.charge_img, 0, 0);
                        } else {
                            holder.battery_percent_layout.setBackgroundResource(R.drawable.bg_green_rounded);
                            holder.battery_percent_txt.setText(p.getPercent() + "%");
                            holder.battery_percent_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.full_img, 0, 0);
                        }
                    }
                }
                else {
                    if (p.getGateEmpty() == true) {
                        holder.battery_percent_layout.setBackgroundResource(R.drawable.bg_blue_rounded);
                        //if(pos==0) holder.battery_percent_txt.setText("" + (pos + 1));
                        //else holder.battery_percent_txt.setText("" + pos);
                        holder.battery_percent_txt.setText("" + (pos+1));
                    } else {
                        holder.battery_percent_layout.setBackgroundResource(R.drawable.bg_gray_rounded);
                        //if (pos == 0) holder.battery_percent_txt.setText("" + (pos + 1));
                        //else holder.battery_percent_txt.setText("" + pos);
                        holder.battery_percent_txt.setText("" + (pos+1));
                    }
                }
            }
        } catch (Exception e) {
            IBUtil.Log(TAG, "SingerListAdapter err_" + e.getMessage());
        }

        return convertView;
    }
}
