package com.otosone.otosstation.frag;

import android.app.ActivityManager;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.otosone.otosstation.BatteryListAdapter;
import com.otosone.otosstation.R;
import com.otosone.otosstation.StationActivity;
import com.otosone.otosstation.utils.Constant;
import com.otosone.otosstation.utils.IBUtil;
import com.otosone.otosstation.utils.OtosVideoView;

import java.util.List;

/**
 * Created by shootdol-tw on 2019-10-01.
 */

public class Frag_A_HomeMenu extends Fragment {
    protected final String TAG = getClass().getSimpleName();
    private View view;
    static Context context;

    private OtosVideoView otos_welcome_video;
    private RelativeLayout welcome_layout;
    private final Handler m_video_handler = new Handler();
    //private ArrayList<BatteryList> battery_status_list = new ArrayList<BatteryList>();
    private GridView battery_status_grid = null;
    //public BatteryListAdapter battery_status_adapter = null;
    //ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, (int)(ToneGenerator.MAX_VOLUME * 0.85));
    //ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_ALARM, (int)(ToneGenerator.MAX_VOLUME * 0.85));
    private TextView stationInfor_textview;

    String r_topic = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_a_homemenu, viewGroup, false);
        context = view.getContext();
        init();
        return view;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible && isResumed()) {
            IBUtil.Log(TAG, "visible!!!!!!!!!!!!!! ");
            onResume();
        } else {
            IBUtil.Log(TAG, "invisible!!!!!!!!!!!!!!");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IBUtil.Log(TAG, "onResume()");
        Constant.m_frag_tag = IBFrag.FRAG_A_HOMEMENU;
        welcome_page();
    }

    @Override
    public void onPause() {
        super.onPause();
        IBUtil.Log(TAG, "onPause()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IBUtil.Log(TAG, "onDestroy()");
    }

    void init() {
        IBUtil.Log(TAG, "init()");

        setLayout();
        welcome_page();
    }

    public boolean frOnKeyDown(int key_code) {
        boolean ret = false;
       // welcome_page();
        switch (key_code) {
            case KeyEvent.KEYCODE_ENTER: //jypark for test
                IBUtil.Log(TAG, "KeyEvent.KEYCODE_ENTER");
                Constant.pUpdate.TAG_SEQ = 2;
                Constant.pUpdate.TAG_WAIT = 0;
                return true;
            case KeyEvent.KEYCODE_1: //jypark for test
                //((StationActivity) context).ChangeFragment(IBFrag.FRAG_G_END);//jypark
                return true;
        }
        return ret;
    }

    private void setLayout() {
         otos_welcome_video = (OtosVideoView) view.findViewById(R.id.otos_welcome_video);
        welcome_layout = (RelativeLayout) view.findViewById(R.id.welcome_layout);
        battery_status_grid = (GridView) view.findViewById(R.id.battery_status_grid);
        stationInfor_textview = (TextView) view.findViewById(R.id.message_txt);

    }

    private final Runnable mVideoRunnable = new Runnable() {
        @Override
        public void run() {
            playVideo();
        }
    };

    private void playVideo() {
        if (Constant.pUpdate.TAG_SEQ == 0) {
            String adfile = "file:///sdcard/maiv_ad.mp4";
            welcome_layout.setVisibility(View.INVISIBLE);
            otos_welcome_video.setVisibility(View.VISIBLE);
            otos_welcome_video.load(adfile, 0);
            otos_welcome_video.getMediaPlayer().setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    IBUtil.Log(TAG, "playVideo() onCompletion !!!");
                    welcome_page();
                }
            });
        }
    }

    public void welcome_page() {
        IBUtil.Log(TAG, "##@# welcome_page... welcome_page!!!!!!!");
        System.out.printf("##@#-----------Frag_A_HomeMenu----------\n");
        welcome_layout.setVisibility(View.VISIBLE);
        otos_welcome_video.stop();
        otos_welcome_video.setVisibility(View.INVISIBLE);
        m_video_handler.postDelayed(mVideoRunnable, 90000); //90초후 광고영상 재상

        Constant.m_battery_status_adapter = new BatteryListAdapter(context, R.layout.item_batterylist, Constant.m_battery_status_list);
        Constant.m_battery_status_adapter.setType(0);
        battery_status_grid.setAdapter(Constant.m_battery_status_adapter);
        IBUtil.setBatteryStatusList();
    }
}
