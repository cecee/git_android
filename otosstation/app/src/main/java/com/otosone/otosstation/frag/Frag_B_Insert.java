package com.otosone.otosstation.frag;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.otosone.otosstation.R;
import com.otosone.otosstation.StationActivity;
import com.otosone.otosstation.utils.Constant;
import com.otosone.otosstation.utils.IBCircleProgress;
import com.otosone.otosstation.utils.IBUtil;

/**
 * Created by shootdol-tw on 2019-10-01.
 */

public class Frag_B_Insert extends Fragment {
    protected final String TAG = getClass().getSimpleName();
    private View view;
    static Context context;

    private IBCircleProgress circle_progress_bar;
    private TextView card_name_txt, info_txt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_b_insert, viewGroup, false);
        context = view.getContext();
        init();
        return view;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible && isResumed()) {
            IBUtil.Log(TAG, "visible!!!!!!!!!!!!!! ");
            onResume();
        } else {
            IBUtil.Log(TAG, "invisible!!!!!!!!!!!!!!");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IBUtil.Log(TAG, "onResume()");
        Constant.m_frag_tag = IBFrag.FRAG_B_INSERT;
    }

    @Override
    public void onPause() {
        super.onPause();
        IBUtil.Log(TAG, "onPause()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IBUtil.Log(TAG, "onDestroy()");
    }

    void init() {
        IBUtil.Log(TAG, "init()");

        setLayout();
        batteryInput();
    }


    private void setLayout() {
        card_name_txt = (TextView) view.findViewById(R.id.card_name_txt);
        info_txt = (TextView) view.findViewById(R.id.info_txt);
        circle_progress_bar = (IBCircleProgress) view.findViewById(R.id.circle_progress_bar);

        card_name_txt.setText(Constant.NFC_CARD_NAME);
    }

    public boolean frOnKeyDown(int key_code) {
        boolean ret = false;
        switch (key_code) {
            case KeyEvent.KEYCODE_1:
                IBUtil.Log(TAG, "KeyEvent.KEYCODE_1");
                Constant.testchk = 1;
                return true;
        }
        return ret;
    }

    private void batteryInput() {
        int pos=Constant.pUpdate.insert_battery_pos+1;
        String insert_pos="";
        insert_pos= (Constant.pUpdate.insert_battery_pos<0)? "-??":String.format("-%02d\n",pos);
        info_txt.setText(getString(R.string.info_battery_input) + insert_pos);
        IBUtil.noti_msg((Activity) context, getString(R.string.info_battery_input) + insert_pos);
    }
}
