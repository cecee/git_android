package com.otosone.otosstation.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;

import com.cecee.InbarAlert.InbarAlertDialog;
import com.otosone.otosstation.R;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by shootdol on 2017-11-22.
 */

public class IBNewSoftwareInstall extends AsyncTask<Integer, Integer, Integer> {
    protected final String TAG = getClass().getSimpleName();

    Context context;
    private static final int BUFFER_SIZE = 4096;
    String fileURL, saveFilePath;
    boolean isStart = false;
    InbarAlertDialog pDialog = null;

    public IBNewSoftwareInstall(Context c, String from, String to) {
        context = c;
        fileURL = from;
        saveFilePath = to;
        IBUtil.Log(TAG, "fileURL_" + fileURL);
        IBUtil.Log(TAG, "saveFilePath_" + saveFilePath);
    }

    void start() {
        isStart = true;
        pDialog = new InbarAlertDialog(context, InbarAlertDialog.PROGRESS_TYPE)
                .setTitleText(context.getString(R.string.found_newsw))
                .setContentText(context.getString(R.string.download_newsw));

        pDialog.show();
        pDialog.setCancelable(false);
        IBUtil.Log(TAG, "start()");
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        IBUtil.Log(TAG, "onPreExecute()");
        start();
    }

    @Override
    protected Integer doInBackground(Integer... integers) {
        IBUtil.Log(TAG, "doInBackground()");

        URL url = null;
        HttpURLConnection httpConn = null;
        int responseCode = 0;

        try {
            url = new URL(fileURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            IBUtil.Log(TAG, "err0_" + e.getMessage());
        }

        try {
            httpConn = (HttpURLConnection) url.openConnection();
            responseCode = httpConn.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
            IBUtil.Log(TAG, "err00_" + e.getMessage());
        }

        // always check HTTP response code first
        if (responseCode == HttpURLConnection.HTTP_OK) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();

            if (disposition != null) {
                // extracts file name from header field
                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10,
                            disposition.length() - 1);
                }
            } else {
                // extracts file name from URL
                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
                        fileURL.length());
            }

            IBUtil.Log(TAG, "Content-Type = " + contentType);
            IBUtil.Log(TAG, "Content-Disposition = " + disposition);
            IBUtil.Log(TAG, "Content-Length = " + contentLength);
            IBUtil.Log(TAG, "fileName = " + fileName);

            // opens input stream from the HTTP connection
            InputStream inputStream = null;
            try {
                inputStream = httpConn.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
                IBUtil.Log(TAG, "err1_" + e.getMessage());
            }
            //String saveFilePath = saveDir + File.separator + fileName;

            // opens an output stream to save into file
            FileOutputStream outputStream = null;
            try {
                outputStream = new FileOutputStream(saveFilePath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                IBUtil.Log(TAG, "err2_" + e.getMessage());
            }

            int bytesRead = -1;
            byte[] buffer = new byte[BUFFER_SIZE];
            try {
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                outputStream.close();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            IBUtil.Log(TAG, "File downloaded");
        } else {
            IBUtil.Log(TAG, "No file to download. Server replied HTTP code: " + responseCode);
        }
        httpConn.disconnect();

        return 0;
    }

    @Override
    protected void onProgressUpdate(Integer... params) {
        IBUtil.Log(TAG, "onProgressUpdate()");
    }

    @Override
    protected void onPostExecute(Integer result) {
        super.onPostExecute(result);
        IBUtil.Log(TAG, "onPostExecute()");

        if(isStart && pDialog != null) {
            pDialog.dismiss();
            pDialog = new InbarAlertDialog(context, InbarAlertDialog.PROGRESS_TYPE).setTitleText(context.getString(R.string.download_newsw_installing));
            pDialog.show();
            pDialog.setCancelable(false);
            installAPK();
        }
    }

    public void showInstallDialog() {
//        new InbarAlertDialog(context, InbarAlertDialog.WARNING_TYPE)
//                .setTitleText(context.getString(R.string.download_newsw_success))
//                .setContentText(context.getString(R.string.confirm_newsw))
//                //.setCancelText(context.getString(R.string.confirm_no))
//                //.setConfirmText(context.getString(R.string.install_newsw))
//                .showCancelButton(false)
////                .setCancelClickListener(new InbarAlertDialog.OnSweetClickListener() {
////                    @Override
////                    public void onClick(InbarAlertDialog sDialog) {
////                        sDialog.dismiss();
////                    }
////                })
////                .setConfirmClickListener(new InbarAlertDialog.OnSweetClickListener() {
////                    @Override
////                    public void onClick(InbarAlertDialog sDialog) {
////                        sDialog.dismiss();
////                        startInstallAPK();
////                    }
////                })
//                .show();
//
//        new Handler().postDelayed(new Runnable()
//        {
//            @Override
//            public void run()
//            {
//                startInstallAPK();
//            }
//        }, 3000);
    }

//    public void startInstallAPK() {
//        //UpdateInfo.Shell_Commandl("pm install -r " + saveFilePath + "\r\n am start -n com.otosone.otosstation/com.otosone.otosstation.StationActivity");
//        UpdateInfo.Shell_Commandl("pm install -r -d " + saveFilePath);
//    }

    private void installAPK() {
        String exec_install = "pm install -r -d " + saveFilePath;
        //IBUtil.Log("#@#", "SingSkyAPKDownAsyncTask __" + exec_install);

        try {
            if (exec_install.length() != 0)
                Runtime.getRuntime().exec(exec_install);
        } catch (IOException e) {
            //IBUtil.Log("#@#", "installAPK__Err_" + e.getMessage());
            pDialog.dismiss();
            //progress1_info_txt.setText("Installation failed.");
            new InbarAlertDialog(context, InbarAlertDialog.ERROR_TYPE)
                    .setTitleText(context.getString(R.string.download_newsw_failed))
                    .showCancelButton(false)
                    .setConfirmText(context.getString(R.string.confirm_ok))
                    .show();
        }
        //IBUtil.Log("#@#", "installAPK END");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pDialog.dismiss();
            }
        }, 60000);
        return;
    }

}
