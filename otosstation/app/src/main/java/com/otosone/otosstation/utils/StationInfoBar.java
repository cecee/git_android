package com.otosone.otosstation.utils;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;

/**
 * Created by shootdol-tw on 2017-03-07.
 */

public class StationInfoBar {
    static int IBTOAST_NSERARCH = 1;
    static int IBTOAST_SONGINFO = 2;
    static int IBTOAST_PLAYINFO = 3;

    static boolean always_on = false;
    static boolean refresh = true;
    static int current_toast = 0;
    static Activity act_current;

    static void removePlayInfoToast(boolean chk) {
        try {
            if (Constant.m_playinfo_toast != null) {
                Constant.m_playinfo_toast.cancel();
                Constant.m_playinfo_toast = null;
                IBUtil.Log("SongInfoBar", "#@###### removePlayInfoToast!!!!!!!!!!!!!!");
            }
        } catch (Exception e) {}
    }

    static Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == IBTOAST_PLAYINFO) {
                if (!always_on) removePlayInfoToast(true);
            }
            return false;
        }
    });

    static void sendMessage(int msgid) {
        Message msg = handler.obtainMessage(msgid);
        handler.sendMessage(msg);
    }

    static void sendDelayMessage(int msec, int msgid) {
        Message msg = handler.obtainMessage(msgid);
        handler.sendMessageDelayed(msg, msec);
    }

    static void removeMessage() {
        handler.removeMessages(IBTOAST_NSERARCH);
        handler.removeMessages(IBTOAST_SONGINFO);
        handler.removeMessages(IBTOAST_PLAYINFO);
    }

    static void removeMessage(int msgid) {
        handler.removeMessages(msgid);
    }

    public static void showNormalInfo(Activity act, int msgtype, String str, int priority, boolean ani) {
        act_current = act;
        if (refresh || current_toast != 9) {
            removePlayInfoToast(false);
        }

        try {
            Constant.m_playinfo_toast = IBToast.makeText_InfoMsg(act, msgtype, str, IBToast.STYLE_INFO);
            Constant.m_playinfo_toast.setPriority(priority);
            if (ani)
                Constant.m_playinfo_toast.setAnimation(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

            removeMessage();

            always_on = false;
            refresh = true;
            current_toast = 9;
            Constant.m_playinfo_toast.show();
            sendDelayMessage(6000, IBTOAST_PLAYINFO);
        } catch (Exception e) {}
    }
}
