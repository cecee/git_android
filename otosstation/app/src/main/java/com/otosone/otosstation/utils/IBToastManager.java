package com.otosone.otosstation.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.WeakHashMap;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH;
import static com.otosone.otosstation.utils.IBToast.LENGTH_STICKY;

/**
 * Created by shootdol-tw on 2017-03-06.
 */

class IBToastManager extends Handler implements Comparator<IBToast> {

    private static final int MESSAGE_DISPLAY = 0xc2007;
    private static final int MESSAGE_ADD_VIEW = 0xc20074dd;
    private static final int MESSAGE_REMOVE = 0xc2007de1;

    private static WeakHashMap<Activity, IBToastManager> sManagers;
    private static ReleaseCallbacks sReleaseCallbacks;

    private final Queue<IBToast> msgQueue;
    private final Queue<IBToast> stickyQueue;

    private IBToastManager() {
        msgQueue = new PriorityQueue<IBToast>(1, this);
        stickyQueue = new LinkedList<IBToast>();
    }

    static synchronized IBToastManager obtain(Activity activity) {
        if (sManagers == null) {
            sManagers = new WeakHashMap<Activity, IBToastManager>(1);
        }
        IBToastManager manager = sManagers.get(activity);
        if (manager == null) {
            manager = new IBToastManager();
            ensureReleaseOnDestroy(activity);
            sManagers.put(activity, manager);
        }
        return manager;
    }

    static void ensureReleaseOnDestroy(Activity activity) {
        if (SDK_INT < ICE_CREAM_SANDWICH) {
            return;
        }
        if (sReleaseCallbacks == null) {
            sReleaseCallbacks = new ReleaseCallbacksIcs();
        }
        sReleaseCallbacks.register(activity.getApplication());
    }


    static synchronized void release(Activity activity) {
        try {
            if (sManagers != null) {
                final IBToastManager manager = sManagers.remove(activity);
                if (manager != null) {
                    manager.clearAllMsg();
                }
            }
        } catch (Exception e) {
            IBUtil.Log("IBToastManager", "release: " + e.getMessage());
        }
    }

    static synchronized void clearAll() {
        if (sManagers != null) {
            final Iterator<IBToastManager> iterator = sManagers.values().iterator();
            while (iterator.hasNext()) {
                final IBToastManager manager = iterator.next();
                if (manager != null) {
                    manager.clearAllMsg();
                }
                iterator.remove();
            }
            sManagers.clear();
        }
    }

    void add(IBToast IBToast) {
        msgQueue.add(IBToast);
        if (IBToast.mInAnimation == null) {
            IBToast.mInAnimation = AnimationUtils.loadAnimation(IBToast.getActivity(),
                    android.R.anim.fade_in);
        }
        if (IBToast.mOutAnimation == null) {
            IBToast.mOutAnimation = AnimationUtils.loadAnimation(IBToast.getActivity(),
                    android.R.anim.fade_out);
        }
        displayMsg();
    }

    void clearMsg(IBToast IBToast) {
        if (msgQueue.contains(IBToast) || stickyQueue.contains(IBToast)) {
            // Avoid the message from being removed twice.
            removeMessages(MESSAGE_DISPLAY, IBToast);
            removeMessages(MESSAGE_ADD_VIEW, IBToast);
            removeMessages(MESSAGE_REMOVE, IBToast);
            msgQueue.remove(IBToast);
            stickyQueue.remove(IBToast);
            removeMsg(IBToast);
        }
    }

    void clearAllMsg() {
        removeMessages(MESSAGE_DISPLAY);
        removeMessages(MESSAGE_ADD_VIEW);
        removeMessages(MESSAGE_REMOVE);
        clearShowing();
        msgQueue.clear();
        stickyQueue.clear();
    }

    void clearShowing() {
        final Collection<IBToast> showing = new HashSet<IBToast>();
        obtainShowing(msgQueue, showing);
        obtainShowing(stickyQueue, showing);
        for (IBToast msg : showing) {
            clearMsg(msg);
        }
    }

    static void obtainShowing(Collection<IBToast> from, Collection<IBToast> appendTo) {
        for (IBToast msg : from) {
            if (msg.isShowing()) {
                appendTo.add(msg);
            }
        }
    }

    private void displayMsg() {
        if (msgQueue.isEmpty()) {
            return;
        }
        // First peek whether the IBToast is being displayed.
        final IBToast IBToast = msgQueue.peek();

        final Message msg;
        if (!IBToast.isShowing()) {
            // Display the IBToast
            msg = obtainMessage(MESSAGE_ADD_VIEW);
            msg.obj = IBToast;
            sendMessage(msg);
        } else if (IBToast.getDuration() != LENGTH_STICKY) {
            msg = obtainMessage(MESSAGE_DISPLAY);
            sendMessageDelayed(msg, IBToast.getDuration()
                    + IBToast.mInAnimation.getDuration() + IBToast.mOutAnimation.getDuration());
        }
    }

    private void removeMsg(final IBToast IBToast) {
        clearMsg(IBToast);
        final View view = IBToast.getView();
        ViewGroup parent = ((ViewGroup) view.getParent());
        if (parent != null) {
            IBToast.mOutAnimation.setAnimationListener(new OutAnimationListener(IBToast));
            view.clearAnimation();
            view.startAnimation(IBToast.mOutAnimation);
        }

        Message msg = obtainMessage(MESSAGE_DISPLAY);
        sendMessage(msg);
    }

    private void addMsgToView(final IBToast IBToast) {
        View view = IBToast.getView();
        if (view.getParent() == null) { // Not added yet
            final ViewGroup targetParent = IBToast.getParent();
            final ViewGroup.LayoutParams params = IBToast.getLayoutParams();
            if (targetParent != null) {
                targetParent.addView(view, params);
            } else {
                IBToast.getActivity().addContentView(view, params);
            }
        }
        view.clearAnimation();
        view.startAnimation(IBToast.mInAnimation);
        if (view.getVisibility() != View.VISIBLE) {
            view.setVisibility(View.VISIBLE);
        }

        final int duration = IBToast.getDuration();
        if (duration != LENGTH_STICKY) {
            final Message msg = obtainMessage(MESSAGE_REMOVE);
            msg.obj = IBToast;
            sendMessageDelayed(msg, duration);
        } else { // We are sticky, we don't get removed just yet
            stickyQueue.add(msgQueue.poll());
        }
    }

//    void clearShowingIB(IBToast IBToast) {
//        final Collection<IBToast> showing = new HashSet<IBToast>();
//        obtainShowing(msgQueue, showing);
//        obtainShowing(stickyQueue, showing);
//
//        final int add_duration = IBToast.getDuration();
//
//        for (IBToast msg : showing) {
//            removeMessageIB(msg, add_duration);
//        }
//    }
//
//    private void removeMessageIB(IBToast IBToast, int add_duration) {
//        if(msgQueue.contains(IBToast) || stickyQueue.contains(IBToast)){
//            removeMessages(MESSAGE_REMOVE, IBToast);
//            final int duration = IBToast.getDuration();
//            if (duration != LENGTH_STICKY) {
//                final Message msg = obtainMessage(MESSAGE_REMOVE);
//                msg.obj = IBToast;
//                sendMessageDelayed(msg, duration+add_duration);
//
//                IBUtil.Log("TAG", "removeMessageIB: " + (duration+add_duration));
//            }
//            //msgQueue.remove(IBToast);
//            //stickyQueue.remove(IBToast);
//            //removeMsg(IBToast);
//        }
//    }

    @Override
    public void handleMessage(Message msg) {
        final IBToast IBToast;
        switch (msg.what) {
            case MESSAGE_DISPLAY:
                //IBUtil.Log("TAG", "MESSAGE_DISPLAY !!!!!!!!!! ");
                displayMsg();
                break;
            case MESSAGE_ADD_VIEW:
                //IBUtil.Log("TAG", "MESSAGE_ADD_VIEW !!!!!!!!!! ");
                IBToast = (IBToast) msg.obj;
                //clearShowingIB(IBToast);
                addMsgToView(IBToast);
                break;
            case MESSAGE_REMOVE:
                IBToast = (IBToast) msg.obj;
                removeMsg(IBToast);
                break;
            default:
                super.handleMessage(msg);
                break;
        }
    }

    @Override
    public int compare(IBToast lhs, IBToast rhs) {
        return inverseCompareInt(lhs.mPriority, rhs.mPriority);
    }

    static int inverseCompareInt(int lhs, int rhs) {
        return lhs < rhs ? 1 : (lhs == rhs ? 0 : -1);
    }

    private static class OutAnimationListener implements Animation.AnimationListener {

        private final IBToast IBToast;

        private OutAnimationListener(IBToast IBToast) {
            this.IBToast = IBToast;
        }

        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            final View view = IBToast.getView();
            if (IBToast.isFloating()) {
                final ViewGroup parent = ((ViewGroup) view.getParent());
                if (parent != null) {
                    parent.post(new Runnable() { // One does not simply removeView
                        @Override
                        public void run() {
                            parent.removeView(view);
                        }
                    });
                }
            } else {
                view.setVisibility(View.GONE);
            }
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    }

    interface ReleaseCallbacks {
        void register(Application application);
    }

    @TargetApi(ICE_CREAM_SANDWICH)
    static class ReleaseCallbacksIcs implements Application.ActivityLifecycleCallbacks, ReleaseCallbacks {
        private WeakReference<Application> mLastApp;

        public void register(Application app) {
            if (mLastApp != null && mLastApp.get() == app) {
                return; // Already registered with this app
            } else {
                mLastApp = new WeakReference<Application>(app);
            }
            app.registerActivityLifecycleCallbacks(this);
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            release(activity);
        }

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        }

        @Override
        public void onActivityStarted(Activity activity) {
        }

        @Override
        public void onActivityResumed(Activity activity) {
        }

        @Override
        public void onActivityPaused(Activity activity) {
        }

        @Override
        public void onActivityStopped(Activity activity) {
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        }
    }
}