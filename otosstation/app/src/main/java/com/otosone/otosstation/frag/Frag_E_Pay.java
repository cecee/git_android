package com.otosone.otosstation.frag;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.otosone.otosstation.PaymentProcessPopup;
import com.otosone.otosstation.R;
import com.otosone.otosstation.utils.Constant;
import com.otosone.otosstation.utils.IBCircleProgress;
import com.otosone.otosstation.utils.IBUtil;

import java.text.NumberFormat;

/**
 * Created by shootdol-tw on 2019-10-01.
 */

public class Frag_E_Pay extends Fragment {
    protected final String TAG = getClass().getSimpleName();
    private View view;
    static Context context;
    private IBCircleProgress circle_progress_bar;
    private TextView card_name_txt, info_txt1, info_txt2, pay_txt;

    private String customer_name = "";
    private PaymentProcessPopup payment_popup = null;
    private boolean isReplaceBattery = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_e_pay, viewGroup, false);
        context = view.getContext();
        init();
        return view;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible && isResumed()) {
            IBUtil.Log(TAG, "visible!!!!!!!!!!!!!! ");
            onResume();
        } else {
            IBUtil.Log(TAG, "invisible!!!!!!!!!!!!!!");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IBUtil.Log(TAG, "onResume()");
        Constant.m_frag_tag = IBFrag.FRAG_E_PAY;
    }

    @Override
    public void onPause() {
        super.onPause();
        IBUtil.Log(TAG, "onPause()");
        if(payment_popup != null) {
            payment_popup.dialogClose();
            payment_popup = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IBUtil.Log(TAG, "onDestroy()");
    }

    void init() {
        IBUtil.Log(TAG, "init()");

        setLayout();
        paymentInfo();
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                showPaymentPopup();
            }
        }, 3000);
    }

    public boolean frOnKeyDown(int key_code) {
        boolean ret = false;
        switch (key_code) {
        }
        return ret;
    }

    private void setLayout() {
        card_name_txt = (TextView) view.findViewById(R.id.card_name_txt);
        info_txt1 = (TextView) view.findViewById(R.id.info_txt1);
        info_txt2 = (TextView) view.findViewById(R.id.info_txt2);
        pay_txt = (TextView) view.findViewById(R.id.pay_txt);
        circle_progress_bar = (IBCircleProgress) view.findViewById(R.id.circle_progress_bar);
        card_name_txt.setText(Constant.NFC_CARD_NAME);
    }

    private void paymentInfo() {
        //final SpannableStringBuilder sp1 = new SpannableStringBuilder(customer_name + getString(R.string.info_paid_string));
        //sp1.setSpan(new ForegroundColorSpan(0xFFFF9700), customer_name.length()+5, customer_name.length()+12, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //info_txt1.append(sp1);
        //final SpannableStringBuilder sp2 = new SpannableStringBuilder((100 - Constant.current_battery_level) +  getString(R.string.info_paid_string2));
        //sp2.setSpan(new ForegroundColorSpan(0xFFFFF100), 0, 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //info_txt2.append(sp2);
        info_txt1.setText(customer_name + getString(R.string.info_paid_string));
        info_txt2.setText((100 - Constant.current_battery_level) +  getString(R.string.info_paid_string2));

        NumberFormat nf = NumberFormat.getInstance();
        pay_txt.setText("" +  nf.format(Constant.current_pay));
    }

    private void showPaymentPopup() {
        IBUtil.Log(TAG, "#@#payment_popup()-----------");
        try {
            payment_popup = new PaymentProcessPopup(context);
            payment_popup.setCanceledOnTouchOutside(true);
            payment_popup.setOnDismissListener(new DialogInterface.OnDismissListener()
            {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    IBUtil.Log(TAG, "#@#payment_popup dismiss()------------");
                    payment_popup = null;
                }
            });

            payment_popup.show();
            Constant.pUpdate.TAG_try=0;
            Constant.pUpdate.TAG_WAIT=0;
            Constant.pUpdate.TAG_SEQ = 6;
        } catch (Exception e) {
            IBUtil.Log(TAG, "#@#payment_popup() Error__" + e.getMessage());
        }
    }
}
