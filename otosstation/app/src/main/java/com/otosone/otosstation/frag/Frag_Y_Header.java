package com.otosone.otosstation.frag;

import android.app.Fragment;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.otosone.otosstation.R;
import com.otosone.otosstation.utils.Constant;
import com.otosone.otosstation.utils.IBMonitor;
import com.otosone.otosstation.utils.IBUtil;

/**
 * Created by shootdol on 2017-11-30.
 */

public class Frag_Y_Header extends Fragment {
    protected final String TAG = getClass().getSimpleName();

    private View view;
    private Context context;

    private ImageView no_network_img;
    private TextView test_txt, cs_txt, cs_tel_txt;

    private IBMonitor m_ibMonitor = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_y_header, viewGroup, false);
        context = view.getContext();
        init();
        return view;
    }

    void init() {
        IBUtil.Log(TAG, "init()");

        no_network_img = (ImageView) view.findViewById(R.id.no_network_img);
        test_txt = (TextView) view.findViewById(R.id.test_txt);

        cs_txt = (TextView) view.findViewById(R.id.cs_txt);
        cs_tel_txt = (TextView) view.findViewById(R.id.cs_tel_txt);

        cs_tel_txt.setText("057-0000-0000");

        m_ibMonitor = new IBMonitor(context);
        m_ibMonitor.setOnChangeIBStatusListener(IBChangedListener);
        IntentFilter fillter = new IntentFilter();
        fillter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        fillter.addAction("otos.station.FORCE_CONTROL");
        fillter.addAction("otos.station.MQTT_CONNECT");

        context.registerReceiver(m_ibMonitor, fillter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IBUtil.Log(TAG, "onDestroy()");
    }


    IBMonitor.OnChangeIBStatusListener IBChangedListener = new IBMonitor.OnChangeIBStatusListener() {
        @Override
        public void OnChanged(int status) {
            IBUtil.Log(TAG, "[IBMonitor] status_" + status);
            switch (status) {
//                case IBMonitor.CONNECTIVITY_CHANGE:
//                    IBUtil.Log(TAG, "[IBMonitor] CONNECTIVITY_CHANGE");
//                    checkInternetConnect();
//                    break;
                case IBMonitor.FORCE_CONTROL:
                    IBUtil.Log(TAG, "[IBMonitor] FORCE_CONTROL");
                    testModeTxt();
                    break;
                case IBMonitor.MQTT_CONNECT:
                    IBUtil.Log(TAG, "[IBMonitor] MQTT_CONNECT");
                    checkMqttConnect();
                    break;
                default:
                    break;
            }
        }
    };

    private void checkInternetConnect() {
        //String nettype = "";
        String nettype = IBUtil.getWhatKindOfNetwork(context);
        IBUtil.Log(TAG, "네트워크 상태!!!___" + nettype);

        if (nettype.equals("ETH")) {
            no_network_img.setVisibility(View.GONE);
        } else {
            no_network_img.setVisibility(View.VISIBLE);
        }
    }

    private void checkMqttConnect() {
        if (Constant.mqtt_connect_ok) {
            test_txt.setVisibility(View.GONE);
            no_network_img.setVisibility(View.GONE);
        } else {
            test_txt.setVisibility(View.VISIBLE);
            test_txt.setText("Network Fail");
            no_network_img.setVisibility(View.VISIBLE);
        }
    }

    private void testModeTxt() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(Constant.pUpdate.FORCE_CONTROL) {
                    test_txt.setVisibility(View.VISIBLE);
                    test_txt.setText("TEST MODE");
                } else {
                    test_txt.setVisibility(View.GONE);
                }
            }
        }, 500);
    }
}
