package com.otosone.otosstation.utils;

import android.app.Fragment;

import com.otosone.otosstation.BatteryList;
import com.otosone.otosstation.BatteryListAdapter;
import com.otosone.otosstation.mqtt.PahoMqttClient;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.json.JSONObject;

import java.util.ArrayList;

public class Constant {

    public static final String m_disableUI = "service call activity 42 s16 com.android.systemui";
    public static UpdateInfo pUpdate;// = new UpdateInfo();

    public static int current_battery_level=0;
    public static int current_pay=0; //결제금액
    public static int current_payed=0; //결제한금액

    public static int testchk = -1;
    public static boolean test_take = false;
    public static boolean main_thread = false;
    public static boolean m_boot_completed = false;
    public static boolean mqtt_connect_ok = false;
    public static int mqtt_TryConnection = 0;

 //   public static String MQTT_BROKER_ADDR = "221.165.27.76";
    public static String MQTT_BROKER_ADDR = "13.209.169.85";
    public static String MQTT_BROKER_PORT = "1883";
    public static String MQTT_BROKER_URL  = "tcp://" + MQTT_BROKER_ADDR + ":" + MQTT_BROKER_PORT;
    public static String PUBLISH_TOPIC_SERVER = "otos_server";
    public static String TOPIC_STATION = "STN/";
    public static String CLIENT_ID = "otos_station_";
    public static String NFC_CARD_NAME = "THPARK";

    public static MqttAndroidClient mqttclient;
    public static PahoMqttClient pahoMqttClient;

    public static IBToast m_playinfo_toast;

    public static HVAC hvacBoard = new HVAC();
    public static ArrayList <JSONObject> list_mqtt_obj = new ArrayList <JSONObject>();
     //for wave-progress
    public static final boolean ANTI_ALIAS = true;
    public static final int DEFAULT_SIZE = 150;
    public static final int DEFAULT_ANIM_TIME = 1000;
    public static final int DEFAULT_MAX_VALUE = 100;
    public static final int DEFAULT_VALUE = 50;
    public static final int DEFAULT_HINT_SIZE = 15;
    public static final int DEFAULT_VALUE_SIZE = 15;
    public static final int DEFAULT_ARC_WIDTH = 15;
    public static final int DEFAULT_WAVE_HEIGHT = 40;
//---------------------------------------------------------------

    public static String formatDate;
    public int timer_cnt = 0;
    public int updatePeriodCnt = 0;
    public static ArrayList<BatteryList> m_battery_status_list = new ArrayList<BatteryList>();
    public static BatteryListAdapter m_battery_status_adapter = null;
    public static boolean first_init=false;
    public static boolean tag_reset=false;

    public static int m_frag_tag = 0;
    public static Fragment m_current_frag;
}

class MOTOR {
    int mPosition=0xFF;
    int mControl=0;
    int mError=0;

}

class BUCKET {
    boolean ignore=false;
    boolean cbd_exist1=false;
    boolean cbd_exist2=false;
    boolean wait=false;
    boolean door_lock1=false;
    boolean door_lock2=false;
    boolean batt_lock1=false;
    boolean batt_lock2=false;
    int soc=0;
    int bms_cnt=0;
    int rgba=0;
}


class AC_POWER_DETECT{
    long volt;
    long ampere;
    long watt;
}


class HVAC_RELAY{
    boolean AC_light;
    boolean AC_heater1;
    boolean AC_heater2;
    boolean AC_heater3;
}


class HVAC_FAN{
    boolean Fan1;
    boolean Fan2;
    boolean Fan3;
    boolean Fan4;
    boolean Fan5;
    boolean Fan6;
}

class HVAC_INPUT_PORT{
    boolean ac_fail;
    boolean dc_fail;
    boolean bat_fail;
    boolean ovc;
    boolean door;
    boolean slope1;
    boolean slope2;
    boolean shock;
}

class HVAC_TEMPERATURE{
    int FAS=70;//tf_alm_set;
    int FAR=60;//tf_alm_rel;
    int RAS=70;//tr_alm_set;
    int RAR=60;//tr_alm_rel;
    int VFO=35;//vf_on;
    int VFF=30;//vf_off;
    int VRO=35;//vr_on;
    int VRF=30;//vr_off;
    int CHO=40;//cn_hi_on;
    int CHF=30;//cn_hi_off;
    int CLO=25;//cn_lo_on;
    int CLF=15;//cn_lo_off;
    int HNO=15;//hn_on;
    int HNF=25;//hn_off;
    int HAS=80;//hn_alm_set;
    int HAR=70;//hn_alm_rel;
    int HI_Temp;
    int LO_Temp;
    int HeaterAlarm;
}

class HVAC {
    short[][] dth_th =new short[3][2];
    int[][] tmp36 =new int[3][4];
    AC_POWER_DETECT ac_detect = new AC_POWER_DETECT();
    HVAC_RELAY relay_ctrl = new HVAC_RELAY();
    HVAC_FAN   fan_ctrl  =  new HVAC_FAN();
    HVAC_FAN   fan_alarm =  new HVAC_FAN();
    HVAC_INPUT_PORT input = new HVAC_INPUT_PORT();
    int relayControl;
    int fanControl;
    int fanAlarm;
    int inPort;
}

class CHARGER{
    int setVoltage;
    int setCurrent;
    int spsInVoltage;
    int spsVoltage;
    int monCurrent;
    int inTemper;
    int rsev;
    int cid;
    int seq;
    int seq_sec;
    long version=0;
    long status;
    boolean open_door;
    boolean batt_exist;
    boolean comm_err;
}

class BMS{
    int voltage;
    int current;
    int soc=0;
    int temper;
    int serial;
    int parallel;
    int capacity;
    int year;
    int month;
    int day;
    int nation;
    long serialNumber;
    long userData;
    long Counter1;
    long Counter2;
    long Counter3;
    long cellBancing;
    long cell_alarm_status;
    int[] cell_votage = new int[20];
    int[] cell_temper = new int[6];
}

class C_BOARD {
    boolean exist=false;
    BMS bms = new BMS();
    CHARGER charger = new CHARGER();
    MOTOR Motor = new MOTOR();
    int rgb;
}

class UPDATE_SET_VALUE{
    HVAC_RELAY d_relay= new HVAC_RELAY();
    HVAC_FAN d_fan =    new HVAC_FAN();
    String lastNfcId;
}