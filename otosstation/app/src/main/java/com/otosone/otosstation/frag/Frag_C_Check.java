package com.otosone.otosstation.frag;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.otosone.otosstation.R;
import com.otosone.otosstation.StationActivity;
import com.otosone.otosstation.utils.Constant;
import com.otosone.otosstation.utils.IBCircleProgress;
import com.otosone.otosstation.utils.IBUtil;

/**
 * Created by shootdol-tw on 2019-10-01.
 */

public class Frag_C_Check extends Fragment {
    protected final String TAG = getClass().getSimpleName();
    private View view;
    static Context context;

    private IBCircleProgress circle_progress_bar;
    private TextView card_name_txt, info_txt;

    private int nextstep_flag = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_c_check, viewGroup, false);
        context = view.getContext();
        init();
        return view;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible && isResumed()) {
            IBUtil.Log(TAG, "visible!!!!!!!!!!!!!! ");
            onResume();
        } else {
            IBUtil.Log(TAG, "invisible!!!!!!!!!!!!!!");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IBUtil.Log(TAG, "onResume()");
        Constant.m_frag_tag = IBFrag.FRAG_C_CHECK;
    }

    @Override
    public void onPause() {
        super.onPause();
        IBUtil.Log(TAG, "onPause()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IBUtil.Log(TAG, "onDestroy()");
    }

    void init() {
        IBUtil.Log(TAG, "init()");

        setLayout();
        batteryCheck();

        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                ((StationActivity) context).ChangeFragment(IBFrag.FRAG_D_INFO);
            }
        }, 4500);
    }


    private void setLayout() {
        card_name_txt = (TextView) view.findViewById(R.id.card_name_txt);
        info_txt = (TextView) view.findViewById(R.id.info_txt);

        circle_progress_bar = (IBCircleProgress) view.findViewById(R.id.circle_progress_bar);
        card_name_txt.setText(Constant.NFC_CARD_NAME);
    }

    public boolean frOnKeyDown(int key_code) {
        boolean ret = false;
        switch (key_code) {
        }
        return ret;
    }

    private void batteryCheck() {
//        final SpannableStringBuilder sp = new SpannableStringBuilder(getString(R.string.info_battery_check));
//        sp.setSpan(new ForegroundColorSpan(0xFFFF9500), 5, 11, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        info_txt.append(sp);
        info_txt.setText(getString(R.string.info_battery_check));
    }
}
