package com.otosone.otosstation.frag;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.otosone.otosstation.R;
import com.otosone.otosstation.utils.IBUtil;

/**
 * Created by shootdol on 2017-11-30.
 */

public class Frag_Z_Footer extends Fragment {
    protected final String TAG = getClass().getSimpleName();

    private View view;
    private Context context;

    private TextView ver_txt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_z_footer, viewGroup, false);
        context = view.getContext();
        init();
        return view;
    }

    void init(){
        IBUtil.Log(TAG, "init()");

        ver_txt = (TextView) view.findViewById(R.id.ver_txt);

        ver_txt.setText("Ver " + String.format("%.2f", IBUtil.getVersion(context)));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IBUtil.Log(TAG, "onDestroy()");
    }
}
