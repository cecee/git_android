package com.otosone.otosstation.frag;

/**
 * Created by shootdol-tw on 2019-10-01.
 */

public class IBFrag {
    public static final int FRAG_A_HOMEMENU = 0;
    public static final int FRAG_B_INSERT = 1;
    public static final int FRAG_C_CHECK = 2;
    public static final int FRAG_D_INFO = 3;
    public static final int FRAG_E_PAY = 4;
    public static final int FRAG_F_TAKE = 5;
    public static final int FRAG_G_END = 6;
}
