package com.otosone.otosstation.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.os.SystemClock;

import com.otosone.otosstation.StationActivity;
import com.otosone.otosstation.frag.IBFrag;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static android.content.ContentValues.TAG;
import static com.otosone.otosstation.utils.Constant.hvacBoard;
import static java.lang.Integer.valueOf;

/**
 * Created by Administrator on 2017-09-22.
 */


public class UpdateInfo {

    private static jni JNI;
    private static Context mcontext;
    private static Constant constant;
    public String STATION_JOB="INIT";
    public String STATION_CPUID;
    public String STATION_APK_VERSION;
    public String STATION_IP="";
    public String STATION_NATION ="";
    public String STATION_CITY = "";
    public String STATION_TIMEZONE ="";
    public String STATION_LAT ="";
    public String STATION_LON ="";



    public String IGNORE_BUCKET="#00";
    public int STATION_SERVICE = 1;
    public int STATION_TYPE = 0;

    public boolean FORCE_CONTROL=false;
    public boolean IGNORE_NFC = false;
    public int UPDATE_PERIOD_TIME = 60;
    public static int FC_flag=0;
    public static int TV_flag=0;

    private int COLOR_FULL    =0x0000FF00;
    private int COLOR_ZERO    =0x80800000;
    private int COLOR_CHARGING=0x80000000;//RGBA
    private int COLOR_EMPTY   =0x02020000;//약한노란색
    private int COLOR_TAKEOUT =0x80808001;
    private int COLOR_INSERT  =0x80808001;
    private int COLOR_WAIT    =0x40404001;

    public int take_battery_pos=-1;
    public int insert_battery_pos=-1;
    public int insert_battery_cnt=-1;
    public int exInsert_battery_cnt =-1;
    public final int BUCK_CNT=6;
    public final int CBD_CNT= BUCK_CNT*2;
    public boolean STATION_READY=false;
    public int subBdScanCnt=0;

    public int TAG_SEQ  = 0;
    public int TAG_WAIT = 0;
    public int TAG_try=0;
    private int TAG_TIMEOUT = 50;//100ms*50 = 5sec
    private int INSERT_TIMEOUT = 200;//100ms*200 = 20sec
    private long HVAC_ALARM=0;

    public UPDATE_SET_VALUE UpdateSetValue = new UPDATE_SET_VALUE();
    public HVAC_TEMPERATURE StationSetTemp = new HVAC_TEMPERATURE();
    public BUCKET[] buckets = new BUCKET[BUCK_CNT];
    public C_BOARD[] Cboards = new C_BOARD[CBD_CNT];

    private boolean bucket_update_flag = false;
    //private int bucket_ignore=0x00;//bucket 0-15
    private int request_id=0;
    private boolean subBd_ready=true;
    private int TrySubBdRequest=0;
    private int[] subBd_try = new int[17];//16(CHG)+1(HVAC)
    private boolean[] subBd_exist = new boolean[17];//16(CHG)+1(HVAC)

    public UpdateInfo(Context context) {
        JNI = new jni();
        constant = new Constant();
        mcontext = context;
        BUCKET_init();

        hvacBoard.fan_ctrl.Fan1=false;
        hvacBoard.fan_ctrl.Fan2=false;
        hvacBoard.fan_ctrl.Fan3=false;
        hvacBoard.fan_ctrl.Fan4=false;
        hvacBoard.fan_ctrl.Fan5=false;
        hvacBoard.fan_ctrl.Fan6=false;
    }

    public int GetBit(long n, int bit) { return (int)(n >> bit) & 1;}
    public boolean CheckBit(long n, int bit) { return (((int)(n >> bit) & 1)==1)? true:false;}
    public long SetBit(long n, int bit) { return n |= 1 << bit; }
    public long ClearBit(long n, int bit) {return n &= ~(1 << bit);}

    public void BUCKET_init() {
        int i;

        for(i=0;i<CBD_CNT;i++){
            Cboards[i]=new C_BOARD();
         }

        for( i=0;i<BUCK_CNT;i++){
            buckets[i] = new BUCKET();
            //buckets[i].cbd_exist=false;
            buckets[i].cbd_exist1=false;
            buckets[i].cbd_exist2=false;
            buckets[i].soc=0;
            buckets[i].bms_cnt=0;

        }
        for(i=0;i<17;i++){
            subBd_try[i] = 50;
            subBd_exist[i]=false;
        }
        BUCKET_SetIgnore(IGNORE_BUCKET);
    }

    public void BUCKET_SetIgnore(String str_ign)
    {
        int ingnore=0;
        str_ign.replace("#", "");
        try {
            ingnore = Integer.parseInt(str_ign);
        } catch(NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }
        for(int i=0;i<BUCK_CNT;i++){
            buckets[i].ignore = CheckBit(ingnore, i);
        }
    }

    public int BUCKET_GetIgnore()
    {
        int ignore=0;
        for(int i=0;i<BUCK_CNT;i++){
            if(buckets[i].ignore) SetBit(ignore, i);
            else ClearBit(ignore, i);
        }
        return ignore;
    }

    public void pubMqttPeriod()
    {
        pubMqttCboard();
        pubMqttStation();
    }

    public void subMqttObjProcess(JSONObject jobj) {
        int type;
        int resp;
        String sCPU;
       // HVAC_TEMPERATURE set_temp = new HVAC_TEMPERATURE();
        Constant.mqtt_TryConnection = 0;
        //IBUtil.Log(TAG, "#@#mqtt=====> rsv_process[" + jobj);
        try {
            sCPU = jobj.getString("sCPU");
            type = jobj.getInt("type");
            if(!sCPU.equals(STATION_CPUID)) return;
            IBUtil.Log(TAG, "#@#mqtt=====> rsv type[" + type+"]");
            switch(type){
                case 0://cbd control
                    break;
                case 1://Station set
                    STATION_SERVICE =  jobj.getInt("bSVS");
                    IGNORE_BUCKET   =  jobj.getString("sIGN");
                    STATION_TYPE    =  jobj.getInt("iTYP");
                    UPDATE_PERIOD_TIME = jobj.getInt("iPER");
                    StationSetTemp.FAS =  jobj.getInt("iFAS");
                    StationSetTemp.FAR =  jobj.getInt("iFAR");
                    StationSetTemp.RAS =  jobj.getInt("iRAS");
                    StationSetTemp.RAR =  jobj.getInt("iRAR");
                    StationSetTemp.VFO =  jobj.getInt("iVFO");
                    StationSetTemp.VFF =  jobj.getInt("iVFF");
                    StationSetTemp.VRO =  jobj.getInt("iVRO");
                    StationSetTemp.VRF =  jobj.getInt("iVRF");
                    StationSetTemp.CHO =  jobj.getInt("iCHO");
                    StationSetTemp.CHF =  jobj.getInt("iCHF");
                    StationSetTemp.CLO =  jobj.getInt("iCLO");
                    StationSetTemp.CLF =  jobj.getInt("iCLF");
                    StationSetTemp.HNO =  jobj.getInt("iHNO");
                    StationSetTemp.HNF =  jobj.getInt("iHNF");
                    StationSetTemp.HAS =  jobj.getInt("iHAS");
                    StationSetTemp.HAR =  jobj.getInt("iHAR");
                    IBUtil.Log(TAG, "#@#mqtt=====> Station set UPDATE_PERIOD_TIME[" + UPDATE_PERIOD_TIME+"]");
                    break;
                case 2://nfc control
                    resp=jobj.getInt("resp");
                    switch(resp){
                        case 0:
                            int allowed=jobj.getInt("allowed");
                            int remain=jobj.getInt("remain");
                            constant.NFC_CARD_NAME = jobj.getString("name");
                            TAG_SEQ=(remain >= allowed) ? 2 : 0;
                            TAG_WAIT=0;
                            System.out.printf("#@#mqtt message allowed[%d] remain[%d] name[%s]전송 받음\n", allowed, remain, jobj.getString("name"));
                            break;
                        case 1:
                            constant.current_pay = jobj.getInt("pay");
                            System.out.printf("#@#mqtt (1)결재할금액 pay[%d]전송 받음\n", constant.current_pay);
                            if(TAG_SEQ !=0)TAG_SEQ=5;//TAG_SEQ=0 에서 받으면 무시
                            TAG_WAIT=0;
                            break;
                        case 2:
                            int amount=jobj.getInt("amount");
                           //constant.current_payed
                            constant.current_payed = amount;
                            if(amount==constant.current_pay){
                            System.out.printf("#@#mqtt (2)결재한금액 amount[%d]전송 받음 TAG_SEQ[%d]\n", amount, TAG_SEQ);
                            if(TAG_SEQ !=0) TAG_SEQ=7;//TAG_SEQ=0 에서 받으면 무시
                            TAG_WAIT=0;
                            }
                            break;
                    }
                    break;
                case 3://Station Control
                    int bCTRL = jobj.getInt("bCTRL");
                    int bRES = jobj.getInt("bRES");
                    String sRLY_CTRL="";
                    String sLOK_CTRL="";
                    String sLED_CTRL="";
                    String sFAN_CTRL="";
                    String sSPS_CTRL="";
                    String sCHG_CTRL="";

                    System.out.printf("#@#Control case 3 bCTRL==>[%d] \n",bCTRL);
                    FORCE_CONTROL = (bCTRL>0) ? true:false;////
                    if(FORCE_CONTROL) {
                        try {
                            sRLY_CTRL = jobj.getString("sRLY_CTRL");//#00-00
                            sLOK_CTRL = jobj.getString("sLOK_CTRL");//#00-00-00
                            sLED_CTRL = jobj.getString("sLED_CTRL");//@00|255|255|255|0
                            sFAN_CTRL = jobj.getString("sFAN_CTRL");//#00-00
                            sSPS_CTRL = jobj.getString("sSPS_CTRL");//#00-00-00
                            sCHG_CTRL = jobj.getString("sCHG_CTRL");//@00|840|200|
                        }
                        catch(Exception ex) {}

                        System.out.printf("#@#Control sLOK_CTRL[%s] \n",sLOK_CTRL);
                        System.out.printf("#@#Control sRLY_CTRL[%s] \n",sRLY_CTRL);
                        System.out.printf("#@#Control sLED_CTRL[%s] \n",sLED_CTRL);
                        System.out.printf("#@#Control sFAN_CTRL[%s] \n",sFAN_CTRL);
                        System.out.printf("#@#Control sSPS_CTRL[%s] \n",sSPS_CTRL);
                        System.out.printf("#@#Control sCHG_CTRL[%s] \n",sCHG_CTRL);

                        CBOARD_Locks(sLOK_CTRL);
                        CBOARD_SetRGBs(sLED_CTRL);
                        CBOARD_Chargings(sCHG_CTRL);
                        CBOARD_SPSs(sSPS_CTRL);
                        HVAC_Fans(sFAN_CTRL);
                        HVAC_ACRelays(sRLY_CTRL);
                        System.out.printf("#@#Station control sLOK_CTRL[%s]\n", sLOK_CTRL);
                    }
                    if(bRES==1){
                        System.out.printf("#@#mqtt 스테이션 Reboot message 전송 받음\n");
                        MBOARD_StationReboot();
                    }
                    break;
            }
         }
        catch (JSONException e) {
        }
    }

    public void STN_ChargingProcess(int cid) {
        int setVoltage = 840;
        int setCurrent = 100;
        int detCrrent =  300;//3.00A
        int detVoltage = 8100;//81.00V
        int soc = Cboards[cid].bms.soc;
        boolean cmd_charged = false;
        boolean rcharged =  CheckBit(Cboards[cid].charger.status,0);
        boolean relay =  CheckBit(Cboards[cid].charger.status,10);
        String str;
        if(rcharged && !relay){//charging on일때 relay off이면 relay -->on
            str = String.format("#%02x0101", cid);
            CBOARD_SPSs(str);//SPS_Relay On
        }

        if(soc==1000){
            if(rcharged && Cboards[cid].bms.current < detCrrent && Cboards[cid].charger.seq_sec>10 ){
                cmd_charged = false;
            }
            else if(Cboards[cid].bms.voltage < detVoltage) cmd_charged = true;
            else cmd_charged=rcharged;//상태유지
        }
        else if(soc>100 && soc<1000){
            cmd_charged = true;
        }

//        if(rcharged && Cboards[cid].charger.spsInVoltage>840){
//            CBOARD_SPSs("#0101");//SPS_Relay On
//        }

        if(cmd_charged==rcharged) return;
        if(cmd_charged){
            str = String.format("@%02d|%03d|%03d", cid, setVoltage, setCurrent);
            CBOARD_Chargings(str);// CBOARD_Chargings("@00|840|200");
        }
        else{
            str = String.format("@%02d|%03d|%03d", cid, 0, 0);//stop charging
            CBOARD_Chargings(str);
        }
    }

    public void STN_LockProcess(int cid) {
        int buck_id= cid/2;
        int lock_pos= cid%2;
        int rdLock = Cboards[cid].Motor.mControl&0x0f;
        int erLock = Cboards[cid].Motor.mError&0x0f;
        int wrLock=0;
        if(lock_pos==0){
            if(buckets[buck_id].batt_lock1) wrLock=wrLock|0x03; else wrLock=wrLock & 0x0C;
            if(buckets[buck_id].door_lock1) wrLock=wrLock|0x0C; else wrLock=wrLock & 0x03;
        }
        else {
            if(buckets[buck_id].batt_lock2) wrLock=wrLock|0x03; else wrLock=wrLock & 0x0C;
            if(buckets[buck_id].door_lock2) wrLock=wrLock|0x0C; else wrLock=wrLock & 0x03;

        }
        if((wrLock != rdLock) || erLock>0){
            String str= String.format("#%02X0F%02X",cid,wrLock);
            System.out.printf("#@#STN_LockProcess[%d][%s] wrLock[%x] rdLock[%x] erLock[%x]\n", cid, str, wrLock, rdLock, erLock);
            CBOARD_Locks(str);
        }
    }

    public void STN_RgbProcess(int cid) {
        int buck_id=cid/2;
        int rgba = buckets[buck_id].rgba;
        if(Cboards[cid].rgb != rgba ){
            CBOARD_SetRGB(cid, rgba);
        }
    }


    public void BUCKET_Lock(int bid, int lock) {
        String str;
        int llock1=0;
        int llock2=0;
        // System.out.printf("#@#BUCKET_Lock bid[%d] lock[%d]\n", bid, lock);
        if(lock>0){
            llock1 =0x0F;  llock2 =0x0F;
            buckets[bid].batt_lock1=true;
            buckets[bid].door_lock1=true;
            buckets[bid].batt_lock2=true;
            buckets[bid].door_lock2=true;
        }
        else {
            llock1=0; llock2=0;
            buckets[bid].batt_lock1=false;
            buckets[bid].door_lock1=false;
            buckets[bid].batt_lock2=false;
            buckets[bid].door_lock2=false;
        }
       // System.out.printf("#@#BUCKET_Lock bid[%d] llock[%02x]\n", bid, llock1);
        str= String.format("#%02X0F%02X",bid*2,llock1);
        CBOARD_Locks(str);

        str= String.format("#%02X0F%02X",(bid*2)+1,llock2);
        CBOARD_Locks(str);
    }

    public void CBOARD_LEDAll(int rgba) {
        for(int i=0;i<16;i++){
            CBOARD_SetRGB(i, rgba);
        }
    }
    public void BUCKET_LockAll(int lock) {
        lock = (lock==0)? 0:1;
        for(int i=0;i<BUCK_CNT;i++){
            BUCKET_Lock(i,lock);
        }
    }

    public void BUCKET_InsertReady(int buck_id) {
        buckets[buck_id].wait=true;
        BUCKET_Lock(buck_id,0);
    }

    public boolean BUCKET_CommCheck(int buck_id) {
        int cid1 = buck_id*2;
        int cid2 = cid1+1;
        boolean com_ok=( Cboards[cid1].charger.comm_err || Cboards[cid2].charger.comm_err)? false:true;
        return com_ok;
    }

    public void BUCKET_InsertDone(int buck_id) {
        IBUtil.Log(TAG, "#@#mqtt BUCKET_InsertDone....");
        //pppp 20.09.07
//        StringBuffer  sb = new StringBuffer ();
        int SOCavr = buckets[buck_id].soc;
        int cid0=buck_id*2;
        int cid1=cid0+1;
        int soc0,soc1;
        String sn0, sn1;

        IBUtil.Log(TAG, "#@#Service Event BUCKET_InsertDone !!!");
        try {
            Constant.current_battery_level = SOCavr;
//            str= String.format("%s|Buck:%d|SOC:%d|%2.1f|%2.1f|",
//                    UpdateSetValue.lastNfcId, buck_id,SOCavr,(float)Cboards[cid0].bms.soc/10, (float)Cboards[cid1].bms.soc/10);  sb.append(str);
//            str= String.format("#%08X|#%08X", Cboards[cid0].bms.serialNumber, Cboards[cid1].bms.serialNumber);  sb.append(str);
            soc0=Cboards[cid0].bms.soc/10;
            soc1=Cboards[cid1].bms.soc/10;
            sn0= String.format("#%08X", Cboards[cid0].bms.serialNumber);
            sn1= String.format("#%08X", Cboards[cid1].bms.serialNumber);

            pubMqtt_IsCalcurateMoney(UpdateSetValue.lastNfcId, SOCavr);
            //pubMqttStationEvent("eINCOMEING", sb.toString());
            pubMqttStationServiceEvent("eINCOMEING", UpdateSetValue.lastNfcId, buck_id, -1, SOCavr, soc0, soc1, sn0, sn1);


            buckets[buck_id].wait=false;
            BUCKET_Lock(buck_id,1);
        } catch (Exception e) {
            IBUtil.Log(TAG, "Err_" + e.getMessage());
        }
    }

    public void BUCKET_TakeReady(int buck_id, int payed) {
        //pppp 20.09.07
        StringBuffer  sb = new StringBuffer ();
        String str;

        int SOCavr = buckets[buck_id].soc;
        int cid0=buck_id*2;
        int cid1=cid0+1;
        int soc0,soc1;
        String sn0,sn1;
        try {
            buckets[buck_id].wait=true;
            soc0=Cboards[cid0].bms.soc/10;
            soc1=Cboards[cid1].bms.soc/10;
            sn0= String.format("#%08X",Cboards[cid0].bms.serialNumber);
            sn1= String.format("#%08X",Cboards[cid1].bms.serialNumber);
//            str= String.format("%s|Buck:%d|Payed:%d|SOC:%d|%2.1f|%2.1f|",
//                    UpdateSetValue.lastNfcId, buck_id, payed, SOCavr,(float)Cboards[cid0].bms.soc/10, (float)Cboards[cid1].bms.soc/10);  sb.append(str);
//            str= String.format("#%08X|#%08X", Cboards[cid0].bms.serialNumber, Cboards[cid1].bms.serialNumber);  sb.append(str);

            pubMqttStationServiceEvent("eREADY", UpdateSetValue.lastNfcId,buck_id,payed, SOCavr, soc0, soc1, sn0, sn1);
            BUCKET_Lock(buck_id,0);//open Door

        } catch (Exception e) {
            IBUtil.Log(TAG, "Err_" + e.getMessage());
        }
    }

    public void BUCKET_TakeDone(int buck_id) {
        IBUtil.Log(TAG, "#@#Service Event BUCKET_TakeDone !!!");
        try {
            pubMqttStationServiceEvent("eDONE",UpdateSetValue.lastNfcId,buck_id,-1,-1,-1,-1,"","");
            buckets[buck_id].wait=false;
            BUCKET_Lock(buck_id,1);
        } catch (Exception e) {
            IBUtil.Log(TAG, "Err_" + e.getMessage());
        }
    }

    public void Tag_process() {
       // System.out.printf("#@#TAG_SEQ[%d] TAG_WAIT[%d]\n", TAG_SEQ, TAG_WAIT);
        switch (TAG_SEQ) {
            case 0:
                TAG_WAIT = 0;
                break;
            case 1:
                //waiting tag register response
                if (TAG_WAIT > TAG_TIMEOUT) {
                    TAG_WAIT=0;
                    TAG_SEQ = 0;
                }
                break;
            case 2:
                insert_battery_pos=BUCK_FindEmpty();
                System.out.printf("##@##TAG_SEQ[%d] TAG_WAIT[%d] insert_battery_pos[%d]\n", TAG_SEQ, TAG_WAIT, insert_battery_pos);
                if(insert_battery_pos<0){
                    Constant.tag_reset=true;
                    //System.out.printf("#@#TAG_SEQ[%d] TAG_WAIT[%d] insert_battery_pos[%d]\n", TAG_SEQ, TAG_WAIT, insert_battery_pos);
                }
                else {
                    BUCKET_InsertReady(insert_battery_pos);//lock open
                    ///////////////////

                    ((StationActivity) mcontext).ChangeFragment(IBFrag.FRAG_B_INSERT);//insert the battery into slot xx 그림

                    insert_battery_cnt=0;
                    exInsert_battery_cnt=-1;
                    TAG_WAIT = 0;
                    TAG_SEQ = 3;
                    //System.out.printf("#@#TAG_SEQ[%d] TAG_WAIT[%d] insert_battery_pos[%d]\n", TAG_SEQ, TAG_WAIT, insert_battery_pos);
                }
                break;
            case 3:
                //waiting insert battery response
                insert_battery_cnt=0;
                if(insert_battery_pos>=0) {
                    int left = (Cboards[insert_battery_pos*2].charger.batt_exist) ? 1:0;
                    int right = (Cboards[(insert_battery_pos*2)+1].charger.batt_exist) ? 2:0;
                    insert_battery_cnt = left+right;
                }
               // insert_battery_cnt = buckets[insert_battery_pos].bms_cnt;
                if(exInsert_battery_cnt != insert_battery_cnt){
                    exInsert_battery_cnt=insert_battery_cnt;
                    if (insert_battery_cnt == 3) TAG_WAIT=0;
                    if (insert_battery_cnt >0 ) {
                        ((StationActivity) mcontext).display_batt_cnt(insert_battery_cnt);
                    }
                }
                if (insert_battery_cnt == 3) {
                    if(BUCKET_CommCheck(insert_battery_pos))
                    {
                        BUCKET_InsertDone(insert_battery_pos);//lock close
                        insert_battery_pos=-1;
                        TAG_WAIT=0;
                        TAG_SEQ = 4;
                    }
                    else{
                       System.out.printf("#@#TAG_SEQ[%d] TAG_WAIT[%d] 밧데리통신에러 pos[%d]\n", TAG_SEQ, TAG_WAIT, insert_battery_pos);
                    }
                }
                //for test
                if (Constant.testchk > 0) {
                    System.out.printf("#@#TAG_SEQ[%d] TAG_WAIT[%d] 밧데리통신에러 pos[%d]\n", TAG_SEQ, TAG_WAIT, insert_battery_pos);
                    BUCKET_InsertDone(insert_battery_pos);//lock close
                    Constant.testchk= -1;
                    insert_battery_pos=-1;
                    TAG_WAIT=0;
                    TAG_SEQ = 4;
                }
                ////
                if (TAG_WAIT > INSERT_TIMEOUT) {
                    //System.out.printf("#@#TAG_SEQ[%d] TAG_WAIT[%d] 밧데리 기다림 pos[%d]\n", TAG_SEQ, TAG_WAIT, insert_battery_pos);
                }
                break;
            case 4:
                //wait pay reponse
                //if (TAG_WAIT > INSERT_TIMEOUT) TAG_SEQ = 0;
                if(IGNORE_NFC) {
                    constant.current_pay=Constant.current_battery_level*1000;
                    TAG_SEQ=5;
                }
                break;
            case 5:
                IBUtil.Log(TAG, "#@#Tag startTagging TAG_SEQ [" + TAG_SEQ + "] TAG_WAIT[" + TAG_WAIT + "]  Constant.TAG_try[" + TAG_try + "]");
                ((StationActivity) mcontext).ChangeFragment(IBFrag.FRAG_C_CHECK);

                TAG_SEQ = 99; //dumy 헛발질
                IBUtil.Log(TAG, "#@#tag TAG_SEQ[" + TAG_SEQ + "]  TAG_WAIT[" + TAG_WAIT + "]");
                //if (TAG_WAIT > INSERT_TIMEOUT) TAG_SEQ = 0;
                break;
            case 6:
                if (TAG_WAIT>30) //3000ms
                {
                    TAG_WAIT=0;
                    if(TAG_try >3 || IGNORE_NFC) {
                        TAG_SEQ=7;
                    }
                    pubMqtt_GetApproval(UpdateSetValue.lastNfcId, constant.current_pay);
                    String tmp="GetApproval|"+String.valueOf(constant.current_pay);

                    if(IGNORE_NFC){
                        TAG_SEQ=7;
                    }
                    TAG_try++;
                    IBUtil.Log(TAG, "#@#Tag startTagging TAG_SEQ[" + TAG_SEQ + "] TAG_WAIT[" + TAG_WAIT + "]  Constant.TAG_try[" + TAG_try + "]");
                }
                break;
            case 7:
                //승인OK
                IBUtil.Log(TAG, "#@#Tag startTagging TAG_SEQ[" + TAG_SEQ + "] TAG_WAIT[" + TAG_WAIT + "]  Constant.TAG_try[" + TAG_try + "]");
                ((StationActivity) mcontext).ChangeFragment(IBFrag.FRAG_F_TAKE);

                BUCKET_TakeReady(take_battery_pos, constant.current_payed );//lock open take led
                TAG_WAIT = 0;
                TAG_SEQ = 8;
                IBUtil.Log(TAG, "#@#tag TAG_SEQ[" + TAG_SEQ + "]  TAG_WAIT[" + TAG_WAIT + "] take_battery_pos:"+take_battery_pos);
                break;
            case 8:
                //가져갈때 까지 기달...
                IBUtil.Log(TAG, "#@#tag TAG_SEQ[" + TAG_SEQ + "]  TAG_WAIT[" + TAG_WAIT + "] take_battery_pos:"+take_battery_pos);
                if(BUCKET_IsEmpty( take_battery_pos))
               // if(TAG_SEQ==0)
                {
                    //BUCKET_TakeDone(take_battery_pos);//lock close
                    TAG_WAIT = 0;
                    TAG_SEQ = 9;
                    IBUtil.Log(TAG, "#@#tag TAG_SEQ[" + TAG_SEQ + "]  TAG_WAIT[" + TAG_WAIT + "] take_battery_pos:"+take_battery_pos);
                }
                break;
            case 9:
                if(TAG_WAIT>20)
                {
                    BUCKET_TakeDone(take_battery_pos);//lock close
                    IBUtil.Log(TAG, "#@#tag TAG_SEQ[" + TAG_SEQ + "]  TAG_WAIT[" + TAG_WAIT + "]");
                    ((StationActivity) mcontext).ChangeFragment(IBFrag.FRAG_G_END);
                    take_battery_pos = -1;
                    TAG_WAIT=0;
                    TAG_SEQ = 0; //dumy
                }
                break;
            case 99:
               // IBUtil.Log(TAG, "#@#tag TAG_SEQ[" + TAG_SEQ + "]  TAG_WAIT[" + TAG_WAIT + "]");
                break;
        }
    }

    public int BUCK_FindEmpty() {
        int empty_bucket=-1;
        for(int i=0; i<BUCK_CNT; i++){
            if(buckets[i].bms_cnt==0 && !buckets[i].ignore) { empty_bucket = i; break;}
        }
        System.out.printf("#@#TAG BUCK_FindEmpty TAG_SEQ[%d] TAG_WAIT[%d] empty_bucket[%d]\n", TAG_SEQ, TAG_WAIT, empty_bucket);
        return empty_bucket;
    }

public int BUCK_FindFull() {
    int i;
    int full_bucket=-1;
    int vol,vol1,vol2,cid1,cid2;
    List<Integer> voltage = new ArrayList<>();
//test
   // buckets[0].soc=100; Cboards[0].bms.voltage=854; Cboards[1].bms.voltage=886;
   // buckets[1].soc=100;Cboards[2].bms.voltage=834; Cboards[3].bms.voltage=886;
   // buckets[2].soc=100;Cboards[4].bms.voltage=824; Cboards[5].bms.voltage=886;
   // buckets[3].soc=100;Cboards[6].bms.voltage=854; Cboards[7].bms.voltage=886;

    for(i=0;i<BUCK_CNT;i++){
        cid1=i*2;cid2=cid1+1;
        vol1=Cboards[cid1].bms.voltage;
        vol2=Cboards[cid2].bms.voltage;
        if(buckets[i].soc==100 && !buckets[i].ignore) {
            vol = (vol1 > vol2) ? vol2 : vol1;
            voltage.add(vol);
        }
        else voltage.add(50000);
    }
    int min = Collections.min(voltage);
    if(min<50000){
        full_bucket = voltage.indexOf(min);
    }
    System.out.printf("#@#BUCK_FindFull full_bucket[%d] vol[%d]\n", full_bucket,min);//7594-->75.94V
    if(FORCE_CONTROL) return 7;
    return full_bucket;
}

public boolean BUCKET_IsEmpty(int buck_id) {
    if(buckets[buck_id].ignore) return false;
//    if (!Cboards[buck_id * 2].charger.open_door && !Cboards[(buck_id * 2) + 1].charger.open_door) {
//        Constant.testchk = -1;
//        return true;
//    }
    //for MaiV
    if (!Cboards[buck_id * 2].charger.batt_exist && !Cboards[(buck_id * 2) + 1].charger.batt_exist) {
        Constant.testchk = -1;
        return true;
    }
    return false;
}

//--------------------------------------------------------------------------------------------------
// BUCKET
//--------------------------------------------------------------------------------------------------

//public void BUCKET_SetLock(int buck_id) {
//    int cid0=buck_id*2;
//    int cid1=cid0+1;
//    boolean lock;
//    int whichLock=0;
//    IBUtil.Log(TAG, "#@#BUCKET_SetLock buckets["+buck_id+"]"+buckets[buck_id].door_lock1+buckets[buck_id].batt_lock1);
//    if (buckets[buck_id].cbd_exist1) {
//        whichLock=0;
//        if(buckets[buck_id].door_lock1) SetBit(whichLock,0); else ClearBit(whichLock,0);
//        if(buckets[buck_id].door_lock2) SetBit(whichLock,1); else ClearBit(whichLock,1);
//        if(buckets[buck_id].batt_lock1) SetBit(whichLock,2); else ClearBit(whichLock,2);
//        if(buckets[buck_id].batt_lock2) SetBit(whichLock,3); else ClearBit(whichLock,3);
//        CBOARD_Lock(cid0, whichLock,0xff);
//    }
//    if (buckets[buck_id].cbd_exist2) {
//        whichLock=0;
//        if(buckets[buck_id].door_lock1) SetBit(whichLock,0); else ClearBit(whichLock,0);
//        if(buckets[buck_id].door_lock2) SetBit(whichLock,1); else ClearBit(whichLock,1);
//        if(buckets[buck_id].batt_lock1) SetBit(whichLock,2); else ClearBit(whichLock,2);
//        if(buckets[buck_id].batt_lock2) SetBit(whichLock,3); else ClearBit(whichLock,3);
//        CBOARD_Lock(cid1, whichLock,0xff);
//    }
//}

public void BUCKET_SetRGB(int buck_id) {
        if(buckets[buck_id].wait) {
            //buckets[buck_id].charging=false;
            buckets[buck_id].rgba = COLOR_WAIT;
        }
        else if(buckets[buck_id].bms_cnt==0) {
            //buckets[buck_id].charging=false;
            buckets[buck_id].rgba = COLOR_EMPTY;
        }
        else if(buckets[buck_id].soc>0 && buckets[buck_id].soc<100) {
            //buckets[buck_id].charging=true;
            buckets[buck_id].rgba = COLOR_CHARGING;
        }
        else if(buckets[buck_id].soc==100) {
            //buckets[buck_id].charging=true;
            buckets[buck_id].rgba = COLOR_FULL;
        }
        else if(buckets[buck_id].soc==0) {
            //buckets[buck_id].charging=false;
            buckets[buck_id].rgba = COLOR_ZERO;
        }
}


    public void BUCKET_Update() {
        int i;
        int bms_cnt;
        int soc1,soc2;
        int cid1,cid2;
        if(!bucket_update_flag)return;

        for(i=0;i<(CBD_CNT+1);i++){
            if(subBd_try[i]<3) subBd_exist[i]=true;
            else subBd_exist[i]=false;
            //IBUtil.Log(TAG, "#@#buck subBdExist[" +i+"] ["+ subBd_exist[i] + "] Constant.bdTry["+subBd_try[i]+"]");
        }
//BUCK_exist+
        for(i=0;i<BUCK_CNT;i++)  {
            if(buckets[i].ignore) {
                buckets[i].cbd_exist1 =false;
                buckets[i].cbd_exist2 =false;
            }
            buckets[i].cbd_exist1=subBd_exist[i*2];
            buckets[i].cbd_exist2=subBd_exist[(i*2)+1];
        }
//BUCK_exist-
            for (i = 0; i < BUCK_CNT; i++)
            {
                cid1 = i * 2; cid2 = cid1+1;
                bms_cnt=0;

                if(!buckets[i].cbd_exist1 && !buckets[i].cbd_exist2){
                    buckets[i].soc = 0;
                    buckets[i].bms_cnt = 0;
                     continue;
                }

                //System.out.printf("#@#buck buckets[%d] cbd_exist1[%b] status[%04x][%04x]\n",i, buckets[i].cbd_exist1, Cboards[cid1].charger.status, Cboards[cid2].charger.status);
                //System.out.printf("#@#buck buckets[%d] status[%b] comm_err[%b]\n",i, CheckBit(Cboards[cid1].charger.status, 4), CheckBit(Cboards[cid2].charger.status, 4));
                if (!buckets[i].cbd_exist1) soc1=0;
                else {
                    soc1= Cboards[cid1].bms.soc;//comm_fail bit가 일때 통신 정상이면 bms있는걸로 한다...
                    if(Cboards[cid1].charger.batt_exist)bms_cnt++;
                    //if(!Cboards[cid1].charger.comm_err)bms_cnt++;
                    //if (!CheckBit(Cboards[cid1].charger.status, 4)) bms_cnt++;
                }

                if (!buckets[i].cbd_exist2) soc2=0;
                else {
                   soc2= Cboards[cid2].bms.soc;
                   if(Cboards[cid2].charger.batt_exist)bms_cnt++;
                   //if(!Cboards[cid2].charger.comm_err)bms_cnt++;
                   // if (!CheckBit(Cboards[cid2].charger.status, 4)) bms_cnt++;
                }

                buckets[i].soc = (soc1 + soc2) / 20;
                buckets[i].bms_cnt = bms_cnt;
               if(FORCE_CONTROL==false) {
                   BUCKET_SetRGB(i);
                }
            }

        bucket_update_flag=false;
    }

    public  int get_ing_bucket() {
       // return Constant.Station_infor.give_bucket;
        return -1;
    }

    public int get_bucket_soc(int bucket_id) {
        return buckets[bucket_id].soc;
    }

    public int get_bucket_empty(int bucket_id) {
        //0 bms_cnt 1:한개있음 2:두개있음
        //IBUtil.Log(TAG, "#@#buck bucket_id[" +bucket_id+"]  bms_cnt["+buckets[bucket_id].bms_cnt+"]");
        return buckets[bucket_id].bms_cnt;
    }

    public int get_bucket_next_empty() {
        //return Constant.Station_infor.next_empty_position;
        return -1;
    }

    public int get_bucket_take_pos() {
        //return Constant.Station_infor.take_bucket;
        return -1;
    }



//    public static int Shell_Commandl(String msg) {
//        JNI.ShellCommand(msg);
//        IBUtil.Log("TAG", msg);
//        return 1;
//    }

/// HVAC Control
/// 0000-0000-0000-0000
/// |__________________ VF Alarm
///  |_________________ VR Alarm
///   |________________
///      |____________| Heater Alarm
    public void HVAC_HeaterAlarmSet() {
        int i,j;
        for(i=0;i<3;i++){
            for(j=0;j<4;j++){
                if( hvacBoard.tmp36[i][j] >= StationSetTemp.HAS ){
                    SetBit(HVAC_ALARM,(i*4)+j);
                }
                else if( hvacBoard.tmp36[i][j] >= StationSetTemp.HAR){
                    if(CheckBit(HVAC_ALARM,(i*4)+j)) ClearBit(HVAC_ALARM,(i*4)+j);
                }
                else ClearBit(HVAC_ALARM,(i*4)+j);
            }
        }
        StationSetTemp.HeaterAlarm =(int)HVAC_ALARM;
    }
    public void HVAC_SetHiTemp() {
        if(hvacBoard.dth_th[0][0]>=hvacBoard.dth_th[1][0]){
            StationSetTemp.HI_Temp=hvacBoard.dth_th[0][0];
            StationSetTemp.LO_Temp=hvacBoard.dth_th[1][0];
        }
        else{
            StationSetTemp.HI_Temp=hvacBoard.dth_th[1][0];
            StationSetTemp.LO_Temp=hvacBoard.dth_th[0][0];
        }
    }

    public void HVAC_VFAlarmSet() {
        if(StationSetTemp.HI_Temp>=StationSetTemp.FAS){
            SetBit(HVAC_ALARM,15);
        }
        else if (StationSetTemp.HI_Temp>=StationSetTemp.FAR){
            if(CheckBit(HVAC_ALARM,15)) ClearBit(HVAC_ALARM,15);
        }
        else {
            ClearBit(HVAC_ALARM,15);
        }
        //StationSetTemp.HeaterAlarm =(int)HEATER_ALARM;
    }

    public void HVAC_VRAlarmSet() {
        if(StationSetTemp.HI_Temp>=StationSetTemp.RAS){
            SetBit(HVAC_ALARM,14);
        }
        else if (StationSetTemp.HI_Temp>=StationSetTemp.RAR){
            if(CheckBit(HVAC_ALARM,14)) ClearBit(HVAC_ALARM,14);
        }
        else {
            ClearBit(HVAC_ALARM,14);
        }
    }

    public void HVAC_SetFrontVenturationControl() {
        int hiTemp=StationSetTemp.HI_Temp/10;
        if(hiTemp>=StationSetTemp.VFO) hvacBoard.fan_ctrl.Fan1=true;
        else if(hiTemp<StationSetTemp.VFF) hvacBoard.fan_ctrl.Fan1=false;
        //System.out.printf("#@#HVAC_FANControl  HI_Temp[%d] VFO[%d] VFF[%d]\n",StationSetTemp.HI_Temp,StationSetTemp.VFO, StationSetTemp.VFF);
    }
    public void HVAC_SetRearVenturationControl() {
        int temper=StationSetTemp.HI_Temp/10;
        if(temper>=StationSetTemp.VRO) hvacBoard.fan_ctrl.Fan2=true;
        else if(temper<StationSetTemp.VRF) hvacBoard.fan_ctrl.Fan2=false;
    }

    public void HVAC_SetRelayControl() {
        hvacBoard.relay_ctrl.AC_heater1=false;
        hvacBoard.relay_ctrl.AC_heater2=false;
        hvacBoard.relay_ctrl.AC_heater3=false;
    }

    public void HVAC_SetCicurationFanControl() {
        int hiTemp=StationSetTemp.HI_Temp/10;
        if(hiTemp>=StationSetTemp.VFO) {
            hvacBoard.fan_ctrl.Fan4=true;
            hvacBoard.fan_ctrl.Fan5=true;
            hvacBoard.fan_ctrl.Fan6=true;
        }
        else if(hiTemp<StationSetTemp.VFF) {
            hvacBoard.fan_ctrl.Fan4=false;
            hvacBoard.fan_ctrl.Fan5=false;
            hvacBoard.fan_ctrl.Fan6=false;
        }
        //System.out.printf("#@#HVAC_FANControl  HI_Temp[%d] VFO[%d] VFF[%d]\n",StationSetTemp.HI_Temp,StationSetTemp.VFO, StationSetTemp.VFF);
    }

    public void STN_FANControl() {
        long wFanValue=0;
        long rFanValue=hvacBoard.fanControl;
        String str="";
        wFanValue= (hvacBoard.fan_ctrl.Fan1) ? SetBit(wFanValue, 0) : ClearBit(wFanValue, 0);
        wFanValue= (hvacBoard.fan_ctrl.Fan2) ? SetBit(wFanValue, 1) : ClearBit(wFanValue, 1);
        wFanValue= (hvacBoard.fan_ctrl.Fan3) ? SetBit(wFanValue, 2) : ClearBit(wFanValue, 2);
        wFanValue= (hvacBoard.fan_ctrl.Fan4) ? SetBit(wFanValue, 3) : ClearBit(wFanValue, 3);
        wFanValue= (hvacBoard.fan_ctrl.Fan5) ? SetBit(wFanValue, 4) : ClearBit(wFanValue, 4);
        wFanValue= (hvacBoard.fan_ctrl.Fan6) ? SetBit(wFanValue, 5) : ClearBit(wFanValue, 5);
       // IBUtil.Log(TAG, "#@#HVAC_["+hvacBoard.fan_ctrl.Fan1+"] " + hvacBoard.fan_ctrl.Fan2+"] "+ hvacBoard.fan_ctrl.Fan3+"] "+ hvacBoard.fan_ctrl.Fan4+"] "+ hvacBoard.fan_ctrl.Fan5+"] "+ hvacBoard.fan_ctrl.Fan6+"]");
        if(wFanValue != rFanValue){
            str= String.format("#FF%02X",wFanValue);
            HVAC_Fans(str);//
        }
        //System.out.printf("#@#STN_FANControl  wFanValue[%x] rFanValue[%x] str[%s]\n",wFanValue,rFanValue,str);
    }

    public void STN_ACRELAYControl() {
        long wValue=0;
        long rValue=hvacBoard.relayControl;
        String str="";
        wValue= (hvacBoard.relay_ctrl.AC_light) ? SetBit(wValue, 0) : ClearBit(wValue, 0);
        wValue= (hvacBoard.relay_ctrl.AC_heater1) ? SetBit(wValue, 1) : ClearBit(wValue, 1);
        wValue= (hvacBoard.relay_ctrl.AC_heater2) ? SetBit(wValue, 2) : ClearBit(wValue, 2);
        wValue= (hvacBoard.relay_ctrl.AC_heater3) ? SetBit(wValue, 3) : ClearBit(wValue, 3);
        if(wValue != rValue){
            str= String.format("#FF%02X",wValue);
            HVAC_ACRelays(str);//
        }
        //System.out.printf("#@#STN_ACRELAYControl  wValue[%x] rValue[%x] str[%s]\n",wValue,rValue,str);
    }

    public void HVAC_Control() {
        HVAC_HeaterAlarmSet();
        HVAC_SetHiTemp();
        HVAC_VFAlarmSet();
        HVAC_VRAlarmSet();
        HVAC_SetFrontVenturationControl();
        HVAC_SetRearVenturationControl();
        HVAC_SetCicurationFanControl();
        HVAC_SetRelayControl();
        STN_FANControl();
        STN_ACRELAYControl();
        StationSetTemp.HeaterAlarm =(int)HVAC_ALARM;
    }

 //------------------------------------------------------------
    public void EventCheck() {
        FC_flag = JNI.GetFcFlag();
        if(FC_flag==0) return;
        //System.out.printf("#@#DDD======> FC_flag [%x] TAG_SEQ[%d]\n",FC_flag, TAG_SEQ);
        if (FC_flag>=0xff00 && FC_flag<=0xff10 ) //from can data signal
        {
            CAN_Update(FC_flag,0 );
            FC_flag=0;
        }

        else if (FC_flag>=0xfa00 && FC_flag<0xfa80 ) //from can request signal
        {
            //IBUtil.Log(TAG, "#@#DDDfrom can request signal ======> FC_flag ["+FC_flag+"]");
            System.out.printf("#@#DDD======> FC_flag [%x] \n",FC_flag);
            CAN_Update(FC_flag,1 );
            FC_flag=0;
        }
        else if (FC_flag==0xff80 ) //from NFC
        {
            IBUtil.Log(TAG, "#@#DDD======> from NFC");
            NFC_Event();
            FC_flag=0;
        }
    }

    public void EventMQTTSubscribe() {
        if (Constant.list_mqtt_obj.size() > 0) {
            IBUtil.Log(TAG, "mqtt===> list_mqtt size[" + Constant.list_mqtt_obj.size()+"]");
            subMqttObjProcess(Constant.list_mqtt_obj.get(0));
            Constant.list_mqtt_obj.remove(0);
        }
    }

    public void Scan_Request() {
        //정상 응답시간 150ms
        //subBoard가 없는경우 약 500ms지연 (TrySubBdRequest 50)
        boolean ignore = false;
        int bucket_id;
        byte[] data;

        bucket_id=request_id/2;
        //HVAC제외
        if(request_id < CBD_CNT) ignore = buckets[bucket_id].ignore;
        if(ignore){
            //request_id++;
            if(++request_id >CBD_CNT) request_id=0;
            subBd_ready=true;
            return;
        }
        if(TrySubBdRequest++>50) subBd_ready=true;//50 약 500ms 동안 응답이 없으면 담으로
        if(subBd_ready)
        {
            subBd_ready=false; TrySubBdRequest=0;
            bucket_update_flag=true;

            // System.out.printf("#@#scan FORCE_CONTROL[%b] request SubBd(%d)========>subBd_try[%d] SCAN_CNT[%d]\n", FORCE_CONTROL, request_id, subBd_try[request_id], subBdScanCnt);
            if(subBd_try[request_id]<50) subBd_try[request_id]++;
            data = mkReqCanData(0);
            if(request_id==CBD_CNT)subBdScanCnt++;
            JNI.CanCommand(request_id, data); //SystemClock.sleep(100);
            if(++request_id >CBD_CNT) request_id=0;
        }

    }

///===============================================================================
//  Control Sub Board
//================================================================================
public void MBOARD_StationReboot() {
    int id=0xAAAAAAAA;
    byte[] data = new byte[8];
    Arrays.fill(data, (byte) 0);
    JNI.CanCommand(id, data);
    SystemClock.sleep(20);
}

/*
* lock 0:unLock 1:Lock
* kind 0:Door 1:Battery 2:all
*/
public void CBOARD_Lock(int cid, int value, int mask, String str) {
    //IBUtil.Log(TAG, "#@#CBOARD_Lock value["+value+"] mask["+mask+"]");
    byte[] data = new byte[8];
    Arrays.fill(data, (byte)0);
    data[0] =2;
    data[1] = (byte)(value & 0xff);
    data[2] = (byte)(mask & 0xff);
    JNI.CanCommand(cid, data);
    SystemClock.sleep(20);
}

public void CBOARD_Locks(String str) {
    int data=0xff0000; //cid-mask-value
    int cid,value,mask;
    str=str.replace("#", "");
    data=valueOf(str,16);
    cid =  (data>>16)&0xff;
    mask = (data>>8)&0xff;
    value = data&0xff;
    //System.out.printf("#@#CBOARD_Locks [%d] mask[%x] value[%x] \n", cid, mask, value);
    CBOARD_Lock(cid, value, mask, str);
}

public void HVAC_ACRelays(String str) {
    int data=0x0000; //mask value
    int value,mask;
    str=str.replace("#", "");
    data=valueOf(str,16);
    mask = (data>>8)&0xff;
    value = data&0xff;
    HVAC_ACRelay(value, mask, str);
}

public void HVAC_Fans(String str) {
    int data=0x0000; //mask value
    int value,mask;
    str=str.replace("#", "");
    try {
        data = Integer.parseInt(str,16);
    } catch(NumberFormatException nfe) {
        System.out.println("Could not parse " + nfe);
    }
    mask = (data>>8)&0xff;
    value = data&0xff;
    HVAC_Fan(value, mask, str);
}

public void CBOARD_Chargings(String str) {
    int cid, set_voltage,set_Current,charging;
    byte[] data = new byte[8];
    Arrays.fill(data, (byte)0);

    str=str.replace("@", "");
    String[] parts = str.split("\\|");
    if(parts.length<3) return;
    System.out.printf("#@#CBOARD_Chargings parts[0]->[%s][%s][%s] \n", parts[0], parts[1], parts[2]);
   // cid = Integer.decode(parts[0]);

    cid=valueOf(parts[0]);
    set_voltage = valueOf(parts[1]);
    set_Current = valueOf(parts[2]);

    charging=(set_voltage>0)? 1:0;
    System.out.printf("#@#CBOARD_Chargings[%d][%d][%d] \n", cid, set_voltage, set_Current);
    data[0] =1;
    data[1] = (byte)charging;
    data[2] = (byte)((set_voltage>>8)&0xff);
    data[3] = (byte)(set_voltage & 0xff);
    data[4] = (byte)((set_Current>>8)&0xff);
    data[5] = (byte)(set_Current & 0xff);
    data[6] = (byte)((set_Current>>8)&0xff);
    data[7] = (byte)(set_Current & 0xff);
    JNI.CanCommand(cid, data);
    SystemClock.sleep(50);
}

public void CBOARD_SetRGB(int cid, int rgba) {
    int r, g, b, a;
    r=(rgba>>24)&0xff;
    g=(rgba>>16)&0xff;
    b=(rgba>>8)&0xff;
    a=(rgba)&0xff;
    byte[] data = new byte[8];
    Arrays.fill(data, (byte)0);
    data[0] =3;
    data[1] = (byte)r;
    data[2] = (byte)g;
    data[3] = (byte)b;
    data[4] = (byte)a;
    //ppppp
   // UpdateSetValue.RGB[cid]=rgba;
    String tmp = String.format("%02X|%08X", cid, rgba);
    JNI.CanCommand(cid, data); SystemClock.sleep(10);
}

public void CBOARD_SetRGBs(String str) {
    int cid, r, g, b, a,rgba;
    byte[] data = new byte[8];
    Arrays.fill(data, (byte)0);
    str=str.replace("@", "");
    String[] parts = str.split("\\|");
    if(parts.length<5) return;
    cid=valueOf(parts[0]);

    r=valueOf(parts[1]);
    g=valueOf(parts[2]);
    b=valueOf(parts[3]);
    a=valueOf(parts[4]);
    data[0] =3;
    data[1] = (byte)r;
    data[2] = (byte)g;
    data[3] = (byte)b;
    data[4] = (byte)a;
    rgba=(r<<24)|(g<<16)|(b<<8)|(a&0xff);
    CBOARD_SetRGB(cid, rgba);
}

public void CBOARD_SPSs(String str){
    int sdata;
    int cid,value,mask;
    byte[] data = new byte[8];
    str=str.replace("#", "");

    sdata=valueOf(str,16);
    System.out.printf("#@#CBOARD_SPSs [%s]==>[%x]\n",str, sdata);
// bit 0: relay
// bit 1: DCSD
// bit 2: SPS_FAN
    value=sdata&0xff;
    mask=(sdata>>8)&0xff;
    cid=(sdata>>16)&0xff;
    Arrays.fill(data, (byte)0);
    data[0] =6;
    data[1] =(byte)(value&0xff);
    data[2] =(byte)(mask&0xff);
    JNI.CanCommand(cid, data);
}


public void CBOARD_Relay(int id, int on) {
    byte[] data = new byte[8];
    Arrays.fill(data, (byte)0);
    System.out.printf("#@#CBOARD_Relay ==>[%d]\n", id);
    data[0] =4;
    if(on>0) data[1] = 1; else data[4] = 0;
    JNI.CanCommand(id, data);
    SystemClock.sleep(100);
}


public void HVAC_ACRelay(int value, int mask, String str) {
    byte[] data = new byte[8];
    Arrays.fill(data, (byte)0);
    System.out.printf("#@#HVAC_Relay ==>[%x][%x]\n", value,mask );
    data[0] =4;
    data[1] =(byte)(value&0xff);
    data[2] =(byte)(mask&0xff);
    JNI.CanCommand(0x10, data);
    SystemClock.sleep(100);
}

    public void HVAC_Fan(int value, int mask, String str) {
        byte[] data = new byte[8];
        Arrays.fill(data, (byte)0);
        System.out.printf("#@#HVAC_Relay ==>[%x][%x]\n", value,mask );
        data[0] =5;
        data[1] =(byte)(value&0xff);
        data[2] =(byte)(mask&0xff);
        JNI.CanCommand(0x10, data);
        SystemClock.sleep(100);
    }

    public void CBOARD_DCSD(int id, int on) {
        byte[] data = new byte[8];
        Arrays.fill(data, (byte)0);
        System.out.printf("#@#CBOARD_DCSD ==>[%d]\n", id);
        data[0] =5;
        if(on>0) data[1] = 1; else data[4] = 0;
        JNI.CanCommand(id, data);
        SystemClock.sleep(100);
    }

    public void CBOARD_Fan(int id, int on) {
        byte[] data = new byte[8];
        Arrays.fill(data, (byte)0);
        System.out.printf("#@#CBOARD_DCSD ==>[%d]\n", id);
        data[0] =6;
        if(on>0) data[1] = 1; else data[4] = 0;
        JNI.CanCommand(id, data);
        SystemClock.sleep(100);
    }

    public void HVAC_RelayLight(boolean on) {
        String relay= (on)? "#0101":"#0100";
        HVAC_ACRelays(relay);
    }
    public void HVAC_RelayHeater1(boolean on) {
        String relay= (on)? "#0202":"#0200";
        HVAC_ACRelays(relay);
    }
    public void HVAC_RelayHeater2(boolean on) {
        String relay= (on)? "#0404":"#0400";
        HVAC_ACRelays(relay);
    }
    public void HVAC_RelayHeater3(boolean on) {
        String relay= (on)? "#0808":"#0800";
        HVAC_ACRelays(relay);
    }

    public void Can_EventResponse(int cdata[], int cid) {
        int i;
        Cboards[cid].charger.batt_exist = (cdata[1]==1)? true:false;
        for(i=0;i<cdata.length;i++) System.out.printf("#@#ddd cdata[%d] 0x[%x]\n",i, cdata[i]);
    }

    //0 data 1:req
    public void CAN_Update(int id, int req) {
        int i;
        int cdata[];
        int cid = id&0x1f;
        if(cid >= CBD_CNT) return;
        if(req==0){ //data
            cdata = JNI.GetCboardData(cid);
            can_parser(cdata, cid);
            subBd_try[cid]=0;
            subBd_ready=true;
        }
        else if(req==1){ //reqest
            cdata = JNI.GetRequestData(cid);
            Can_EventResponse(cdata, cid);
        }
       // for(i=0;i<20;i++) System.out.printf("#@#cell voltage ==>[%d]\n", Constant.Cboards[1].cell_votage[i]);
    }

    public String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a)
            sb.append(String.format("%02x", b));
        return sb.toString();
    }

    public String getStringNfcID(){
     return UpdateSetValue.lastNfcId;
    }

    public void NFC_Event() {
        int i,id_size,len;
        int nfc_data[];
        byte nfc_id[] = new byte[8];
        Arrays.fill(nfc_id, (byte)0);

        nfc_data = JNI.GetNFCData();
        len = nfc_data.length;
        if(len<18 || TAG_SEQ !=0 ) return;
        id_size = nfc_data[9];
        for(i=0;i< id_size;i++) nfc_id[i] = (byte)nfc_data[10+i];
        String str_nfcid = byteArrayToHex(nfc_id);
        UpdateSetValue.lastNfcId=str_nfcid;
        take_battery_pos = BUCK_FindFull();
        ///test+
        take_battery_pos=1;
        ///test-
        System.out.printf("##@#Nfc len[%d] id_size[%d]_nfcid ==>[%s] TAG_SEQ[%d] take_battery_pos[%d]\n",len, id_size, str_nfcid, TAG_SEQ, take_battery_pos);
        TV_flag=1;
        IBUtil.tagSound(mcontext);
        if(take_battery_pos <0 ){
            System.out.printf("##@#Nfc Can not find full_bucket[%d]\n",take_battery_pos);
            return;
        }
        UpdateSetValue.lastNfcId=str_nfcid;
        pubMqtt_IsRemainMoney(str_nfcid);
       // String tmp="IsRemainMoney|"+str_nfcid;
//        if(FORCE_CONTROL){
//            TAG_SEQ=2;
//        }
//        else {
//
//        }
    }

    public  void can_parser(int cdata[], int cid) {
        boolean is_cbboard = (cid < CBD_CNT) ? true : false;
        if (is_cbboard) canFromCharger(cdata, cid);
        else canFromHvac(cdata);
    }

    public byte[] mkReqCanData( int cmd) {
        int i, sum, bucketNum;
        byte[] data = new byte[8];
        Arrays.fill(data, (byte) 0);
        data[0] = 0;
        if(request_id >=CBD_CNT) return data;
        bucketNum = request_id / 2;
        switch(cmd){
            case 0:
                break;
        }
        sum = 0;
        for (i = 0; i < 8; i++) sum = sum + data[i];
        data[7] = (byte) sum;
        return data;
    }

    public void canFromCharger(int cdata[], int cid) {
        int i, tmp, offset=0;
        int bucket_num =  cid/2;

        Cboards[bucket_num].exist=true;//charger exist

        for (i = 0; i < 20; i++) //0-40
        {
            Cboards[cid].bms.cell_votage[i] =( cdata[(i * 2) + 0] + (cdata[(i * 2) + 1] << 8));
            //System.out.printf("#@#cell_votage ==>[%d][%d]\n",i, Constant.Cboards[cid].cell_votage[i] );
        }

        for (i = 0; i < 4; i++)
        {
            Cboards[cid].bms.cell_temper[i] =(cdata[(i * 2) + 40] + (cdata[(i * 2) + 41] << 8));
            //System.out.printf("#@#cell_temper ==>[%d][%d]\n", i, Constant.Cboards[cid].cell_temper[i] );
        }

        Cboards[cid].bms.voltage = cdata[52] + (cdata[53] << 8);
        //System.out.printf("#@#pack_voltage ==>[%d]\n", Cboards[cid].bms.voltage);
        Cboards[cid].bms.current = (short)(cdata[54] + (cdata[55] << 8));
        Cboards[cid].bms.soc =        cdata[56] + (cdata[57] << 8);
        //System.out.printf("#@#pack_soc ==>[%d]\n", Cboards[cid].bms.soc);
        Cboards[cid].bms.temper =     cdata[58] + (cdata[59] << 8);
        Cboards[cid].bms.serial =     cdata[60] ;
        Cboards[cid].bms.parallel =   cdata[61];
        Cboards[cid].bms.capacity =   cdata[62];
        Cboards[cid].bms.year =   cdata[63];
        Cboards[cid].bms.month =  cdata[64];
        Cboards[cid].bms.day =    cdata[65];
        Cboards[cid].bms.nation = cdata[66];
        //System.out.printf("#@#pack_nation ==>[0x%x]\n", Cboards[cid].bms.nation);
//----------------------------------

        Cboards[cid].bms.serialNumber =(cdata[68] + (cdata[69] << 8));
        //70,71 사용안함
        Cboards[cid].bms.userData = (cdata[72] + (cdata[73] << 8)+ (cdata[74] <<16));
        Cboards[cid].bms.Counter1 = (cdata[76] + (cdata[75] << 8));
        Cboards[cid].bms.Counter2 = (cdata[78] + (cdata[77] << 8));
        Cboards[cid].bms.Counter3 = (cdata[80] + (cdata[79] << 8));

        //offset = 81;
        Cboards[cid].bms.cellBancing = (cdata[81] + (cdata[82] << 8)+ (cdata[83] <<16)+ (cdata[84] << 24));
       // System.out.printf("#@#pack_cellBancing ==>[0x%x]\n", Cboards[cid].bms.cellBancing);
//cell_alarm_status 88.89,90,91
        Cboards[cid].bms.cell_alarm_status = (cdata[88] + (cdata[89] << 8)+ (cdata[90] <<16)+ (cdata[91] << 24));

//======================CHARGE DATA ====================
        Cboards[cid].charger.setVoltage = cdata[92] + (cdata[93] << 8);
        Cboards[cid].charger.setCurrent = cdata[94] + (cdata[95] << 8);
        Cboards[cid].charger.spsVoltage = cdata[96] + (cdata[97] << 8);
        Cboards[cid].charger.spsInVoltage = cdata[98] + (cdata[99] << 8);
        Cboards[cid].charger.monCurrent = cdata[100] + (cdata[101] << 8);
        Cboards[cid].charger.inTemper = (cdata[102] + (cdata[103] << 8));
        //System.out.printf("#@#chg_inTemper ==>[%d]\n", Cboards[cid].charger.inTemper);
        //System.out.printf("#@#can_parser[%d]  setVoltage[%d] setCurrent[%d]\n",cid, Cboards[cid].charger.setVoltage ,Cboards[cid].charger.setCurrent );
        Cboards[cid].charger.rsev = (cdata[104] + (cdata[105] << 8));

        //System.out.printf("#@#offset ==>[%d]\n", offset);//106이면 됨
        Cboards[cid].charger.cid =       cdata[106];
        Cboards[cid].charger.seq =       cdata[107];
        Cboards[cid].charger.seq_sec =   cdata[108] + (cdata[109] << 8);
        Cboards[cid].charger.version =   cdata[110] + (cdata[111] << 8);
        Cboards[cid].charger.status =    cdata[112] + (cdata[113] << 8);

        Cboards[cid].charger.open_door = CheckBit(Cboards[cid].charger.status, 14);
        Cboards[cid].charger.batt_exist = CheckBit(Cboards[cid].charger.status, 3);
        Cboards[cid].charger.comm_err = CheckBit(Cboards[cid].charger.status, 4);

       // System.out.printf("#@#can_parser[%d-%d]  chg_seq[%d] chg_seqTime[%d] \n",cid, Cboards[cid].charger.cid, Cboards[cid].charger.seq, Cboards[cid].charger.seq_sec);
       // System.out.printf("#@#offset ==>[%d]\n", offset);//114이면 됨
        //tmp=cdata[114];//motor_ctrl
        //System.out.printf("#@#can_parser  motor_ctrl ==>[%x]\n", tmp);

//        Cboards[cid].Motor.D1_Ctrl= CheckBit(tmp,0);
//        Cboards[cid].Motor.D2_Ctrl= CheckBit(tmp,1);
//        Cboards[cid].Motor.B1_Ctrl= CheckBit(tmp,2);
//        Cboards[cid].Motor.B2_Ctrl= CheckBit(tmp,3);
//        Cboards[cid].Motor.D1_Err= CheckBit(tmp,4);
//        Cboards[cid].Motor.D2_Err= CheckBit(tmp,5);
//        Cboards[cid].Motor.B1_Err= CheckBit(tmp,6);
//        Cboards[cid].Motor.B2_Err= CheckBit(tmp,7);

        Cboards[cid].Motor.mControl=cdata[114]&0x0F;
        Cboards[cid].Motor.mError =(cdata[114]>>4)&0x0F;

         //115 MOTOR POS WWXXYYZZ ww:B2 XX:B1 YY:D2 ZZ:D1
//        tmp=cdata[115];//motor_pos
//        System.out.printf("#@#can_parser motor_pos==>[%x]\n", tmp);
//        Cboards[cid].Motor.D1_Pos= tmp&0x03;
//        Cboards[cid].Motor.D2_Pos= (tmp>>2)&0x03;
//        Cboards[cid].Motor.B1_Pos= (tmp>>4)&0x03;
//        Cboards[cid].Motor.B2_Pos= (tmp>>6)&0x03;

        Cboards[cid].Motor.mPosition=cdata[115];

        Cboards[cid].rgb=(cdata[119]&0xff)<<24 | (cdata[118]&0xff)<<16 | (cdata[117]&0xff)<<8 | (cdata[116]&0xff);
        //System.out.printf("#@#scan response _parser rgb==>[%x][%x][%x][%x] [%08x]\n", tmp=cdata[116], tmp=cdata[117], tmp=cdata[118],tmp=cdata[119], Cboards[cid].rgb);
        if(FORCE_CONTROL==false){
            STN_ChargingProcess(cid);
            STN_LockProcess(cid);
            STN_RgbProcess(cid);
        }
     }

    public void canFromHvac(int cdata[]) {
        int i;
        int tmp;
        for (i = 0; i < 3; i++)//12byte
        {
            hvacBoard.dth_th[i][1] =(short)( cdata[(i * 4) + 2] | cdata[(i * 4) + 3]<<8);//습도
            if(hvacBoard.dth_th[i][1]>0) {
                //습도가 0 이면 값 취하지 않음 19.10.26
                hvacBoard.dth_th[i][0] = (short) (cdata[(i * 4) + 0] | cdata[(i * 4) + 1] << 8);//온도
            }
           // hvacBoard.dth_th[i][1] =(short)( cdata[(i * 4) + 2] | cdata[(i * 4) + 3]<<8);//습도
           // System.out.printf("#@#dth_th[%d] ==>[%x][%x]\n",i, Constant.Dboards.dth_th[i][0],Constant.Dboards.dth_th[i][1] );
        }
        for (i = 0; i < 3; i++)//12byte
        {
            tmp=cdata[(i * 4) + 12]&0xff; hvacBoard.tmp36[i][0] = (tmp>128) ? tmp-256 : tmp;
            tmp=cdata[(i * 4) + 13]&0xff; hvacBoard.tmp36[i][1] = (tmp>128) ? tmp-256 : tmp;
            tmp=cdata[(i * 4) + 14]&0xff; hvacBoard.tmp36[i][2] = (tmp>128) ? tmp-256 : tmp;
            tmp=cdata[(i * 4) + 15]&0xff; hvacBoard.tmp36[i][3] = (tmp>128) ? tmp-256 : tmp;
        }
        hvacBoard.ac_detect.volt =    cdata[24] | (cdata[25]<<8);
        hvacBoard.ac_detect.ampere =  cdata[26] | (cdata[27]<<8);
        hvacBoard.ac_detect.watt =    cdata[28] | (cdata[29]<<8);
        //System.out.printf("#@#dpower==>v[%d] i[%d] p[%d]\n", Constant.Dboards.ac_detect.volt, Constant.Dboards.ac_detect.ampere, Constant.Dboards.ac_detect.watt );
        tmp=cdata[30];
        hvacBoard.relayControl=tmp;
        //hvacBoard.relay_ctrl.AC_light=CheckBit(tmp,0);
        //hvacBoard.relay_ctrl.AC_heater1=CheckBit(tmp,1);
        //hvacBoard.relay_ctrl.AC_heater2=CheckBit(tmp,2);
        //hvacBoard.relay_ctrl.AC_heater3=CheckBit(tmp,3);
        tmp=cdata[31];
        hvacBoard.fanControl=tmp;
        //hvacBoard.fan_ctrl.Fan1=CheckBit(tmp,0);
        //hvacBoard.fan_ctrl.Fan2=CheckBit(tmp,1);
        //hvacBoard.fan_ctrl.Fan3=CheckBit(tmp,2);
        //hvacBoard.fan_ctrl.Fan4=CheckBit(tmp,3);
       // hvacBoard.fan_ctrl.Fan5=CheckBit(tmp,4);
        //hvacBoard.fan_ctrl.Fan6=CheckBit(tmp,5);
        tmp=cdata[32];
        hvacBoard.fanAlarm=tmp;
        hvacBoard.fan_alarm.Fan1=CheckBit(tmp,0);
        hvacBoard.fan_alarm.Fan2=CheckBit(tmp,1);
        hvacBoard.fan_alarm.Fan3=CheckBit(tmp,2);
        hvacBoard.fan_alarm.Fan4=CheckBit(tmp,3);
        hvacBoard.fan_alarm.Fan5=CheckBit(tmp,4);
        hvacBoard.fan_alarm.Fan6=CheckBit(tmp,5);
        tmp=cdata[33];
        hvacBoard.inPort=tmp;
        hvacBoard.input.ac_fail=CheckBit(tmp,0);
        hvacBoard.input.dc_fail=CheckBit(tmp,1);
        hvacBoard.input.bat_fail=CheckBit(tmp,2);
        hvacBoard.input.ovc=CheckBit(tmp,3);
        hvacBoard.input.door=CheckBit(tmp,4);
        hvacBoard.input.slope1=CheckBit(tmp,5);
        hvacBoard.input.slope2=CheckBit(tmp,6);
        hvacBoard.input.shock=CheckBit(tmp,7);

       // System.out.printf("#@#dinput==>AC_light[%b] 1[%b] 7[%b]\n",
       //         hvacBoard.relay_ctrl.AC_light, hvacBoard.input.dc_fail, hvacBoard.input.shock );
       ///20.09.04 ppp
        //if(FORCE_CONTROL==false)
        {
            HVAC_Control();
        }
    }

    public JSONObject mkObjCbd( int cid) {

        JSONObject jsonCboard = new JSONObject();
        JSONArray jarryCellVoltage = new JSONArray();
        JSONArray jarryCellTemerature = new JSONArray();
        int i;
        String str;
        try
        {
            jsonCboard.put("iSB", cid);
            jsonCboard.put("iCSV", Cboards[cid].charger.setVoltage);
            jsonCboard.put("iCSC", Cboards[cid].charger.setCurrent);
            jsonCboard.put("iCVT", Cboards[cid].charger.spsVoltage);//relay 후단
            jsonCboard.put("iCCR", Cboards[cid].charger.monCurrent);
            jsonCboard.put("iCTP", Cboards[cid].charger.inTemper);
            jsonCboard.put("iCSQ", Cboards[cid].charger.seq);
            jsonCboard.put("iCSS", Cboards[cid].charger.seq_sec);
            jsonCboard.put("iCRV", Cboards[cid].charger.rsev);

            str= String.format("#%04X", (0xFFFF & Cboards[cid].charger.status));
            jsonCboard.put("sCST", str);

            str= String.format("#%04X", (0xFFFF & Cboards[cid].charger.version));
            jsonCboard.put("sCVR", str);
            //bms
            for (i = 0; i < 20; i++)  {
                jarryCellVoltage.put(i, Cboards[cid].bms.cell_votage[i]);
            }

            for (i = 0; i < 4; i++)  {
                jarryCellTemerature.put(i, Cboards[cid].bms.cell_temper[i]);
            }
            jsonCboard.put("iCV", jarryCellVoltage);
            jsonCboard.put("iCT", jarryCellTemerature);

            jsonCboard.put("iBV", Cboards[cid].bms.voltage);
            jsonCboard.put("iBA", Cboards[cid].bms.current);
            jsonCboard.put("iSOC", Cboards[cid].bms.soc);
            jsonCboard.put("iNS", Cboards[cid].bms.serial);//pack structure serial-parallel
            jsonCboard.put("iNP", Cboards[cid].bms.parallel);//pack structure serial-parallel
            jsonCboard.put("iCAP", Cboards[cid].bms.capacity);
            int nDate = ((Cboards[cid].bms.nation & 0xff) << 24) | ((Cboards[cid].bms.year&0xff)<<16 )|((Cboards[cid].bms.month&0xff)<<8) | (Cboards[cid].bms.day&0xff);
            str= String.format("#%08X", nDate);
            jsonCboard.put("sDAT", str);//HEX-sting nation-yy-mm-dd
            str= String.format("#%08X", Cboards[cid].bms.serialNumber);
            jsonCboard.put("sSN", str);//serial number

            jsonCboard.put("iCN1", Cboards[cid].bms.Counter1);//pack cycle
            jsonCboard.put("iCN2", Cboards[cid].bms.Counter2);//pack cycle
            jsonCboard.put("iCN3", Cboards[cid].bms.Counter3);//pack cycle

            str= String.format("#%08X", Cboards[cid].bms.userData);
            jsonCboard.put("sUSR", str);//pack user
            str= String.format("#%08X", Cboards[cid].bms.cellBancing);
            jsonCboard.put("sBAL", str);//pack balance

            str= String.format("#%04X", Cboards[cid].Motor.mPosition);
            jsonCboard.put("sLOP", str);
            str= String.format("#%04X", Cboards[cid].Motor.mControl);
            jsonCboard.put("sLOC", str);

            str= String.format("#%04X", Cboards[cid].Motor.mError);
            jsonCboard.put("sLOE", str);
            //ppp 20.09.03 sARM값이 00000000으로 올라가서 수정
           // str= String.format("#%08X", Cboards[cid].bms.cell_alarm_status);
            str= String.format("#%08X", (0xFFFFFFFF & Cboards[cid].bms.cell_alarm_status));
             //System.out.printf("#@#xxxx cell_alarm_status==>[%s][%d]\n",str);
            jsonCboard.put("sARM", str);

        }
        catch(JSONException e)
        {
            e.printStackTrace();
        }
        return jsonCboard;
    }

    public JSONObject mkObjStation() {
        String str;
        JSONObject jsonStation = new JSONObject();
        JSONArray ja_ddth11_tmp = new JSONArray();
        JSONArray ja_ddth11_humi = new JSONArray();
        JSONArray ja_ht0 = new JSONArray();
        JSONArray ja_ht1 = new JSONArray();
        JSONArray ja_ht2 = new JSONArray();
        int i;
        try
        {
            jsonStation.put("IDX", 1);
            jsonStation.put("sCPU", STATION_CPUID);
            jsonStation.put("sAPK", STATION_APK_VERSION );
            jsonStation.put("sIGN", IGNORE_BUCKET);//사용하지않는 Bucket정보
            jsonStation.put("bSVS", STATION_SERVICE);
            jsonStation.put("iTYP", STATION_TYPE);
            jsonStation.put("iPER", UPDATE_PERIOD_TIME);

            for (i = 0; i < 3; i++)  {
                ja_ddth11_tmp.put(i, hvacBoard.dth_th[i][0]);
                ja_ddth11_humi.put(i, hvacBoard.dth_th[i][1]);
            }
            for (i = 0; i < 4; i++) ja_ht0.put(i, hvacBoard.tmp36[0][i]);
            for (i = 0; i < 4; i++) ja_ht1.put(i, hvacBoard.tmp36[1][i]);
            for (i = 0; i < 4; i++) ja_ht2.put(i, hvacBoard.tmp36[2][i]);

            jsonStation.put("iDHT", ja_ddth11_tmp);
            jsonStation.put("iDHH", ja_ddth11_humi);
            jsonStation.put("iHT0", ja_ht0);
            jsonStation.put("iHT1", ja_ht1);
            jsonStation.put("iHT2", ja_ht2);

            jsonStation.put("iACV", hvacBoard.ac_detect.volt);
            jsonStation.put("iACA", hvacBoard.ac_detect.ampere);
            jsonStation.put("iACW", hvacBoard.ac_detect.watt);

            jsonStation.put("bLTC", hvacBoard.relay_ctrl.AC_light);
            jsonStation.put("bHC1", hvacBoard.relay_ctrl.AC_heater1);
            jsonStation.put("bHC2", hvacBoard.relay_ctrl.AC_heater2);
            jsonStation.put("bHC3", hvacBoard.relay_ctrl.AC_heater3);

            str= String.format("#%04X", hvacBoard.fanControl);
            jsonStation.put("sFC", str);
            str= String.format("#%04X", hvacBoard.fanAlarm);
            jsonStation.put("sFA", str);
            jsonStation.put("bACF", hvacBoard.input.ac_fail);
            jsonStation.put("bDCF", hvacBoard.input.dc_fail);
            jsonStation.put("bBAF", hvacBoard.input.bat_fail);
            jsonStation.put("bOVC", hvacBoard.input.ovc);
            jsonStation.put("bDOR", hvacBoard.input.door);
            jsonStation.put("bSL1", hvacBoard.input.slope1);
            jsonStation.put("bSL2", hvacBoard.input.slope2);
            jsonStation.put("bSHK", hvacBoard.input.shock);

            jsonStation.put("iFAS", StationSetTemp.FAS);
            jsonStation.put("iFAR", StationSetTemp.FAR);
            jsonStation.put("iRAS", StationSetTemp.RAS);
            jsonStation.put("iRAR", StationSetTemp.RAR);
            jsonStation.put("iVFO", StationSetTemp.VFO);
            jsonStation.put("iVFF", StationSetTemp.VFF);
            jsonStation.put("iVRO", StationSetTemp.VRO);
            jsonStation.put("iVRF", StationSetTemp.VRF);
            jsonStation.put("iCHO", StationSetTemp.CHO);
            jsonStation.put("iCHF", StationSetTemp.CHF);
            jsonStation.put("iCLO", StationSetTemp.CLO);
            jsonStation.put("iCLF", StationSetTemp.CLF);
            jsonStation.put("iHNO", StationSetTemp.HNO);
            jsonStation.put("iHNF", StationSetTemp.HNF);
            jsonStation.put("iHAS", StationSetTemp.HAS);
            jsonStation.put("iHAR", StationSetTemp.HAR);
            str= String.format("#%04X", StationSetTemp.HeaterAlarm);
            jsonStation.put("sHAL", str);

        }
        catch(JSONException e)
        {
            e.printStackTrace();
        }
        return jsonStation;
    }

    public void pubMqtt_StationInit() {
        JSONObject stn_init = new JSONObject();
        try
        {
            stn_init.put("sCPU", STATION_CPUID);
            stn_init.put("IDX", 0xA0);//init Station
            stn_init.put("sIP", STATION_IP);
            stn_init.put("sIP_City", STATION_CITY);
            stn_init.put("sLAT", STATION_LAT);
            stn_init.put("sLNG", STATION_LON);
    }
        catch(JSONException e) { e.printStackTrace();}
        IBUtil.Log("#@#", "mqtt_send pubMqtt_StationInit==>" + stn_init.toString());
        IBUtil.send_pubmqtt(stn_init.toString(), Constant.PUBLISH_TOPIC_SERVER);

    }

    public void pubMqttCboard() {
        int i;
        JSONObject jCboard =new JSONObject();
        JSONArray  jarrycData = new JSONArray();
        try
        {
            jCboard.put("IDX", 0);
            jCboard.put("sCPU", STATION_CPUID);
            //for(i=0;i<16;i++) jarrycData.put(i,mkObjCbd(i));
            for(i=0;i<CBD_CNT;i++) jarrycData.put(i,mkObjCbd(i));
            jCboard.put("C", jarrycData);
            //IBUtil.Log("#@#", "mqtt_send pubMqttCboard==>" + jCboard.toString() );
            IBUtil.send_pubmqtt(jCboard.toString(), Constant.PUBLISH_TOPIC_SERVER);
        }
        catch(JSONException e){e.printStackTrace();}
    }

    public void pubMqttStation(){
        JSONObject jStation = mkObjStation();
        //IBUtil.Log("#@#", "mqtt_send pubMqttStation==>" + jStation.toString() );
        IBUtil.send_pubmqtt(jStation.toString(), Constant.PUBLISH_TOPIC_SERVER);
    }

    public void pubMqttStationServiceEvent(String event, String nfc_id, int buck_id, int payed, int soc_av, int soc0, int soc1, String sn0, String sn1){
        System.out.printf("#@#xxxxx pubMqttStationServiceEvent==>[%s][%s]\n",event, nfc_id );
        JSONObject jmsg =new JSONObject();
        try
        {
            jmsg.put("IDX", 0xA1);
            jmsg.put("sCPU", STATION_CPUID);
            jmsg.put("sEVT", event);
            jmsg.put("sNFCID", nfc_id);
            jmsg.put("iBUCK", buck_id);
            jmsg.put("iPAYED", payed);
            jmsg.put("iSOCav", soc_av);
            jmsg.put("iSOC0", soc0);
            jmsg.put("iSOC1", soc1);
            jmsg.put("sSN0", sn0);
            jmsg.put("sSN1", sn1);
            IBUtil.send_pubmqtt(jmsg.toString(), Constant.PUBLISH_TOPIC_SERVER);
        }
        catch(JSONException e){e.printStackTrace();}
    }

    public void pubMqtt_IsRemainMoney( String id) {
        JSONObject jsonNfc = new JSONObject();
        try
        {
            jsonNfc.put("sCPU", STATION_CPUID);
            jsonNfc.put("IDX", 2);//Request Remain Money?
            jsonNfc.put("sID", id);
        }
        catch(JSONException e) { e.printStackTrace();}
        IBUtil.Log("#@#", "mqtt_send pubMqtt_IsRemainMoney jc==>" + jsonNfc.toString()+"TAG_SEQ[" +TAG_SEQ+"]");
        if(TAG_SEQ==0) {
            IBUtil.send_pubmqtt(jsonNfc.toString(), Constant.PUBLISH_TOPIC_SERVER);
           if(IGNORE_NFC) TAG_SEQ=2;
           else TAG_SEQ=1;
            TAG_WAIT=0;
        }
     }

    public void pubMqtt_IsCalcurateMoney( String id, int soc) {
        JSONObject jsonNfc = new JSONObject();
        try
        {
            jsonNfc.put("sCPU", STATION_CPUID);
            jsonNfc.put("IDX", 3);//REQ Type  1: soc에 해당하는 금액요청
            jsonNfc.put("sID", id);
            jsonNfc.put("iSOC",soc);
        }
        catch(JSONException e) { e.printStackTrace();}
        IBUtil.Log("#@#", "mqtt_send pubMqtt_IsCalcurateMoney jc==>" + jsonNfc.toString() );
        if(TAG_SEQ==3) {
            IBUtil.send_pubmqtt(jsonNfc.toString(), Constant.PUBLISH_TOPIC_SERVER);
            TAG_SEQ=4;
            TAG_WAIT=0;
        }
    }

    public void pubMqtt_GetApproval( String id, int amount) {
        JSONObject jsonNfc = new JSONObject();
        try
        {
            jsonNfc.put("sCPU", STATION_CPUID);
            jsonNfc.put("IDX", 4);//4: soc에 해당하는 금액요청 2:승인요청
            jsonNfc.put("sID", id);
            jsonNfc.put("iAMOUNT",amount);
        }
        catch(JSONException e) { e.printStackTrace();}
        IBUtil.Log("#@#", "mqtt_send pubMqtt_GetApproval==>" + jsonNfc.toString() );
        if(TAG_SEQ==6) {
            IBUtil.send_pubmqtt(jsonNfc.toString(), Constant.PUBLISH_TOPIC_SERVER);
            TAG_WAIT=0;
        }
    }

    public String Diago_Hvac(){
        int i;
        String str,str1,str2,str3,str4;
        StringBuffer  sb = new StringBuffer ();

        str= String.format("CPU[%s] APK[%s]\n",STATION_CPUID, STATION_APK_VERSION);  sb.append(str);
        str= String.format("[%s] City[%s] [%s]\n", STATION_IP, STATION_CITY, STATION_NATION);  sb.append(str);
        str= String.format("lat[%s] lon[%s] \n", STATION_LAT, STATION_LON);  sb.append(str);

        str1= (FORCE_CONTROL)? "MANU":"AUTO";
        str= String.format("TYPE[%d] IGN[%s] [%s] 주기[%d]Sec\n",STATION_TYPE, IGNORE_BUCKET, str1,UPDATE_PERIOD_TIME);  sb.append(str);

        str= String.format("AC [%3.1fV][%3.1fA][%3.3fKW]\n",
                (float)Constant.hvacBoard.ac_detect.volt/10,(float)hvacBoard.ac_detect.ampere/10,(float)hvacBoard.ac_detect.watt/1000);  sb.append(str);

        str= String.format("DTH11 온도 (%3.1f)(%3.1f)(%3.1f)\n", (float)hvacBoard.dth_th[0][0]/10, (float)hvacBoard.dth_th[1][0]/10,(float)hvacBoard.dth_th[2][0]/10);  sb.append(str);
        str= String.format("DTH11 습도 (%3.1f)(%3.1f)(%3.1f)\n", (float)hvacBoard.dth_th[0][1]/10, (float)hvacBoard.dth_th[1][1]/10,(float)hvacBoard.dth_th[2][1]/10);  sb.append(str);
        str= String.format("HI(%3.1f) LO(%3.1f)\n", (float)StationSetTemp.HI_Temp/10, (float)StationSetTemp.LO_Temp/10);  sb.append(str);

        str= String.format("FAS[%d] FAR[%d] RAS[%d] RAR[%d]\n", StationSetTemp.FAS, StationSetTemp.FAR, StationSetTemp.RAS, StationSetTemp.RAR); sb.append(str);
        str= String.format("VFO[%d] VFF[%d] VRO[%d] VRF[%d]\n", StationSetTemp.VFO, StationSetTemp.VFF, StationSetTemp.VRO, StationSetTemp.VRF); sb.append(str);
        str= String.format("CHO[%d] CHF[%d] CLO[%d] CLF[%d]\n", StationSetTemp.CHO, StationSetTemp.CHF, StationSetTemp.CLO, StationSetTemp.CLF); sb.append(str);
        str= String.format("HNO[%d] HNF[%d] HAS[%d] HAR[%d]\n", StationSetTemp.HNO, StationSetTemp.HNF, StationSetTemp.HAS, StationSetTemp.HAR); sb.append(str);

        for(i=0;i<3;i++){
            str= String.format("tmp36(%d)-(%d)(%d)(%d)(%d)\n",
                    i+1,hvacBoard.tmp36[i][0], hvacBoard.tmp36[i][1], hvacBoard.tmp36[i][2], hvacBoard.tmp36[i][3]);  sb.append(str);
        }
/// HVAC Control
/// 0000-0000-0000-0000
/// |__________________ VF Alarm
///  |_________________ VR Alarm
///   |________________
///      |____________| Heater Alarm
        int alalarm= StationSetTemp.HeaterAlarm;
        int alarmHeater=alalarm & 0x0fff;
        str1=(CheckBit(StationSetTemp.HeaterAlarm, 15)) ? "1":"0";//vf
        str2=(CheckBit(StationSetTemp.HeaterAlarm, 14)) ? "1":"0";//vr
        str= String.format("VF_Alarm(%s) VR_Alarm(%s)\n", str1,str2);  sb.append(str);

        str= Integer.toBinaryString( (1 << 12) | alarmHeater ); str= str.substring(1);
        str= String.format("HEATER Alarm (%s)\n", str);  sb.append(str);
//relayControl
        str= Integer.toBinaryString( (1 << 4) | hvacBoard.relayControl ); str= str.substring(1);
        str= String.format("RELAY_CTRL(R) (%s)\n",str); sb.append(str);

        str= Integer.toBinaryString( (1 << 6) | hvacBoard.fanControl ); str= str.substring(1);
        str= String.format("FAN CTRL(R) (%s)\n",str);  sb.append(str);

        str= Integer.toBinaryString( (1 << 6) | hvacBoard.fanAlarm ); str= str.substring(1);
        str= String.format("FAN ALARM(R) (%s)\n",str);  sb.append(str);

        str1=(hvacBoard.input.ac_fail) ? "NG":"OK";
        str2=(hvacBoard.input.dc_fail) ? "NG":"OK";
        str3=(hvacBoard.input.bat_fail) ? "NG":"OK";
        str4=(hvacBoard.input.ovc) ? "NG":"OK";
        str= String.format("PSU ac(%s) dc(%s) bat(%s) ovc(%s)\n",str1, str2, str3, str4);  sb.append(str);

        str=(hvacBoard.input.door) ? "OPEN":"CLOSE";
        str= String.format("STN door(R)-----[%s]\n",str);  sb.append(str);

        str1=(hvacBoard.input.slope1) ? "1":"0";
        str2=(hvacBoard.input.slope2) ? "1":"0";
        str3=(hvacBoard.input.shock) ? "1":"0";
        str= String.format("STN slope(%s)(%s) shock(%s)\n",str1,str2,str3);  sb.append(str);
        return sb.toString();
    }

    public String Diago_Cbd(int cid){
        int i;
        String str,str1,str2;
        StringBuffer  sb = new StringBuffer ();//subBd_exist[i]=false;
        str1= (subBd_exist[cid]) ? "O" : "X";
        str2= (FORCE_CONTROL)? "MANU":"AUTO";
        str= String.format("[%02d] C-Board exist--------------[%s][%s]\n",cid, str1,str2);  sb.append(str);
        str= String.format("BMS[%2.2fV][%2.2fA] SOC[%2.1f%%]\n",
                (float)Cboards[cid].bms.voltage/100, (float)Cboards[cid].bms.current/100,(float)Cboards[cid].bms.soc/10);  sb.append(str);
        str= String.format("[%2.1f℃] NS[%02d]  NP[%02d] CAP[%03d]\n",
                (float)Cboards[cid].bms.temper/10, Cboards[cid].bms.serial, Cboards[cid].bms.parallel, Cboards[cid].bms.capacity);  sb.append(str);
        int nDate = ((Cboards[cid].bms.nation & 0xff) << 24) | ((Cboards[cid].bms.year&0xff)<<16 )|((Cboards[cid].bms.month&0xff)<<8) | (Cboards[cid].bms.day&0xff);
        str= String.format("%08X", Cboards[cid].bms.serialNumber);
        str= String.format("[%08X][%s]\n", nDate, str);sb.append(str);
        str= String.format("Counter [%04d]-[%04d]-[%04d]\n", Cboards[cid].bms.Counter1, Cboards[cid].bms.Counter2,Cboards[cid].bms.Counter3);sb.append(str);
        str= String.format("userData :%08X\n", Cboards[cid].bms.userData);sb.append(str);
        str= String.format("cellBancing : %08X\n", Cboards[cid].bms.cellBancing);sb.append(str);
        str= String.format("cell_alarms : %08X\n", Cboards[cid].bms.cell_alarm_status);sb.append(str);
        for(i=0;i<4;i++) {
            str = String.format("CV [%1.3f][%1.3f][%1.3f][%1.3f][%1.3f]\n",
                    (float)Cboards[cid].bms.cell_votage[(i * 4) + 0]/1000, (float)Cboards[cid].bms.cell_votage[(i * 4) + 1]/1000,
                    (float)Cboards[cid].bms.cell_votage[(i * 4) + 2]/1000, (float)Cboards[cid].bms.cell_votage[(i * 4) + 3]/1000,
                    (float)Cboards[cid].bms.cell_votage[(i * 4) + 4]/1000);
            sb.append(str);
        }
        str = String.format("CT [%2.1f][%2.1f][%2.1f][%2.1f]℃\n",
            (float)Cboards[cid].bms.cell_temper[0]/10,  (float)Cboards[cid].bms.cell_temper[1]/10,
            (float)Cboards[cid].bms.cell_temper[2]/10,  (float)Cboards[cid].bms.cell_temper[3]/10);
        sb.append(str);

        str= String.format("\r\nsetVoltage[%2.1fV] setCurrent[%2.1fA]\n",
                (float)Cboards[cid].charger.setVoltage/10, (float)Cboards[cid].charger.setCurrent/10);sb.append(str);

        str= String.format("SPS [%2.1f-%2.1fV] [%2.1fA] [%2.1f℃]\n",
                (float)Cboards[cid].charger.spsInVoltage/10, (float)Cboards[cid].charger.spsVoltage/10, (float)Cboards[cid].charger.monCurrent/10, (float)Cboards[cid].charger.inTemper/10);sb.append(str);

        str1=(CheckBit(Cboards[cid].charger.status, 0)) ? "ON":"OFF";
        str2=(CheckBit(Cboards[cid].charger.status, 2)) ? "Finished":"ing";
        str= String.format("Charging [%s]---[%s]\n", str1, str2);sb.append(str);


        int time_target=Cboards[cid].charger.seq_sec;
        int hour=time_target/3600;
        int second=time_target % 3600;
        int minute=second/60;
        second %= 60;
        str1= String.format("%02d:%02d:%02d", hour,minute,second);
        str= String.format("Charging Seq[%d] Elapsed time[%s]\n", Cboards[cid].charger.seq, str1);sb.append(str);
        str= String.format("Charging status[%08X] Version[%04X]\n", Cboards[cid].charger.status, Cboards[cid].charger.version);sb.append(str);

        str1=(Cboards[cid].charger.open_door) ? "OPEN":"CLOSE";
        str= String.format("pBattIn Door[%s]\n", str1);sb.append(str);
        str1=(CheckBit(Cboards[cid].charger.status, 15)) ? "Enable":"Disable";//vf
        str= String.format("BMS Enable[%s]\n", str1);sb.append(str);
//        typedef struct tag_status_bit
//        {
//            uint8_t charging: 1;
//            uint8_t bms_err: 1;
//            uint8_t charging_finish: 1;
//            uint8_t batt_exist: 1;//x3
//            uint8_t comm_fail: 1;
//            uint8_t ac_fail: 1;
//            uint8_t tmp_fail: 1;
//            uint8_t fan_fail: 1;
//            uint8_t dc_fail: 1;
//            uint8_t load_balance: 1;
//            uint8_t relay: 1;
//            uint8_t dc_sd: 1;
//            uint8_t ctrl_fan: 1;//x12 //sps194 내부의 FAN
//            uint8_t pBattIn: 1;//x13
//            uint8_t pBattOut: 1;//x14
//            uint8_t bms_enable: 1;//x15
//        }SPS_STATUS_BIT;
        str= String.format("Lock_pos(PC):%02X ctrl :%02X error :%02X\n",
                Cboards[cid].Motor.mPosition, Cboards[cid].Motor.mControl,Cboards[cid].Motor.mError);sb.append(str);
        str= String.format("RGBA[%08X]\n", Cboards[cid].rgb);sb.append(str);
        return sb.toString();
    }

    public String Diago_SOC( ) {
        int i;
        String str;
        StringBuffer  sb = new StringBuffer ();//subBd_exist[i]=false;
        for(i=0;i<16;i++){
            str= String.format("[%02d] BMS[%2.2fV][%2.2fA] SOC[%2.1f%%]\n",
                    i,(float)Cboards[i].bms.voltage/100, (float)Cboards[i].bms.current/100,(float)Cboards[i].bms.soc/10);  sb.append(str);

        }
        return sb.toString();
    }

    public String Diago_LOCK(){
        int i;
        String str;
        StringBuffer  sb = new StringBuffer ();//subBd_exist[i]=false;
        for(i=0;i<16;i++){
            str= String.format("[%02d] Lock_pos(PC):%02X ctrl :%02X error :%02X\n",
                    i, Cboards[i].Motor.mPosition, Cboards[i].Motor.mControl,Cboards[i].Motor.mError);sb.append(str);
        }
        return sb.toString();
    }
        public void ctrl_asyncTask(int val1, int val2){
        new CTRL_Task().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,val1, val2);
    }

    private  class CTRL_Task extends AsyncTask<Integer ,Integer, Void> {
        //xx yy zz xx:0 lock yy: 0 all-open 1: all-close
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void  doInBackground(Integer... params)
        { //백그라운드 작업이 진행되는 곳.
            IBUtil.Log("#@#", "CTRL_Task params:" + params[0]+"  params.length:"+ params.length);
            switch(params[0]){
                case 0:
                    Constant.pUpdate.BUCKET_LockAll(params[1]);
                    break;
            }
            return null;
        }
        // @Override
        protected void onPostExecute(Void result) {
            //doInBackground 작업이 끝나고 난뒤의 작업
        }
    }

}

