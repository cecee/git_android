package com.otosone.otosstation.utils;

import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by shootdol on 2017-11-22.
 */

public class IBCheckNewSoftwere extends AsyncTask<Integer, Integer, Integer> {
    protected final String TAG = getClass().getSimpleName();

    Context context;
    String version = "", filename = "";
    URL url;
    HttpURLConnection conn;
    InputStreamReader isr;

    public IBCheckNewSoftwere(Context c){
        context = c;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Integer doInBackground(Integer... integers) {
        try {
            //url = new URL("http://221.165.27.76/station_apk/apkfile/lastversion");
            url = new URL("http://otosone.co.kr/station_apk/apkfile/lastversion");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
            version = in.readLine();
            while(true){
                String temp;
                temp = in.readLine();
                if(temp==null)
                    break;
                filename += temp;
            }
        } catch (Exception e) {
            IBUtil.Log(TAG, "Err BufferedReader_" + e.getMessage());
        }
        return 0;
    }

    @Override
    protected void onProgressUpdate(Integer... params) {

    }

    @Override
    protected void onPostExecute(Integer result) {
        super.onPostExecute(result);
        double lastver = 0, currver = 0;
        try {
            lastver = Double.parseDouble(version);
            currver = IBUtil.getVersion(context);
        } catch(NumberFormatException nfe) {
            IBUtil.Log(TAG, "Could not parse " + nfe);
        }

        IBUtil.Log(TAG, "LAST VERSION!!!lastver_" + lastver );
        IBUtil.Log(TAG, "LAST VERSION!!!currver_" + currver );
        if(lastver > currver) {
            IBUtil.Log(TAG, "NEED UPGRADE FILENAME!! " + filename );
            showFoundNewSoftwareDialog("http://221.165.27.76/station_apk/apkfile/" + filename, "/sdcard/" + filename);
        }
        //if(Common.m_total_songcount == 0) showCreateDBDialog();
    }

    public void showFoundNewSoftwareDialog(final String from, final String to) {
//        new InbarAlertDialog(context, InbarAlertDialog.WARNING_TYPE)
//                .setTitleText(context.getString(R.string.confirm_newsw))
//                .setContentText(context.getString(R.string.found_newsw))
//                .setCancelText(context.getString(R.string.confirm_no))
//                .setConfirmText(context.getString(R.string.confirm_yes))
//                .showCancelButton(true)
//                .setCancelClickListener(new InbarAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(InbarAlertDialog sDialog) {
//                        sDialog.dismiss();
//                    }
//                })
//                .setConfirmClickListener(new InbarAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(InbarAlertDialog sDialog) {
//                        sDialog.dismiss();
//                        IBNewSoftwareInstall mProcessTask = new IBNewSoftwareInstall(context, from, to);
//                        mProcessTask.execute();
//                    }
//                })
//                .show();

        IBNewSoftwareInstall mProcessTask = new IBNewSoftwareInstall(context, from, to);
        mProcessTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
