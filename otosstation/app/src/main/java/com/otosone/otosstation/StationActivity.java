package com.otosone.otosstation;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.ybq.android.spinkit.SpinKitView;
import com.otosone.otosstation.frag.IBFrag;
import com.otosone.otosstation.mqtt.MqttMessageService;
import com.otosone.otosstation.mqtt.PahoMqttClient;
import com.otosone.otosstation.utils.Constant;
import com.otosone.otosstation.utils.IBCheckNewSoftwere;
import com.otosone.otosstation.utils.IBUtil;
import com.otosone.otosstation.utils.IButton;
import com.otosone.otosstation.utils.Prefs;
import com.otosone.otosstation.utils.UpdateInfo;
import com.otosone.otosstation.utils.jni;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.TimeZone;


public class StationActivity extends AppCompatActivity implements View.OnClickListener {
    protected final String TAG = getClass().getSimpleName();
    Constant Const;
    private boolean period_flag=false;
    private boolean motorTestFlag=false;
    private boolean toggle=false;
    private boolean exMqttConnection = false;

    static final int LOG_MAXLINE_COUNT = 28;
    private int diag_tick=0;
    private int diag_id=0;

    private LinearLayout funcbtn_layout;
    private jni JNI;
    private TimeZone tz;

    private Activity act;
    private TextView debug_tv;
    private IButton android_setup_btn ;
    private LinearLayout init_layout;
    private SpinKitView spin_kit;

    private Handler otos_handler_100ms  = new Handler();
    private Handler otos_handler_10ms   = new Handler();
    private Handler otos_handler_bootcheck = new Handler();

    static Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station);

        IBUtil.Log(TAG, "onCreate()");
        act = this;
        context = this;
        bootcheck();
    }

    private final Runnable otosStationRunnable100ms = new Runnable() {
        //int res;
        //int i;
        //int[] c0_arg = new int[2];

        @Override
        public void run() {
            //100ms timer
            //IBUtil.Log(TAG, "timer Handler() 100ms");
            //long mtime_start, mtime_end;
            //mtime_start = System.currentTimeMillis();
            //IBUtil.Log(TAG, "--Handler100_AA--");

            Const.timer_cnt++;
            Const.updatePeriodCnt++;
            Constant.pUpdate.TAG_WAIT++;
            diag_tick++;
            //toast view display
            if(Constant.pUpdate.TV_flag > 0){
                System.out.printf("##@#toast view display TV_flag[%d]\n",Constant.pUpdate.TV_flag);
                if(Constant.pUpdate.TV_flag==1) {
                    String tx = Constant.pUpdate.getStringNfcID();
                    tx= tx.substring(0,2);
                    tx=tx+"xxxxxxxx";
                    //  stationInfor_textview.setText(tx);
                    //  stationInfor_textview.setTextColor(0xff0000ff);
                    //  Constant.pUpdate.TV_flag++;
                  

                    IBUtil.noti_msg(act, "Card - " + tx);
                    Constant.pUpdate.TV_flag=0;
                }
//                else if(Constant.pUpdate.TV_flag>50){
//                    stationInfor_textview.setText("Replace the scooter battery");
//                    stationInfor_textview.setTextColor(0xFFA6C4A3);
//                    Constant.pUpdate.TV_flag=0;
//                }
//                else  Constant.pUpdate.TV_flag++;
                // IBUtil.Log(TAG, "#@#Main Timer() Constant.pUpdate.TV_flag[" + Constant.pUpdate.TV_flag + "]");
            }

            //IBUtil.Log(TAG, "Handler100_BB");

            if (Const.updatePeriodCnt > (Const.pUpdate.UPDATE_PERIOD_TIME * 10)) {
                Const.updatePeriodCnt = 0;
                period_flag=true;
                //Constant.pUpdate.pubMqttPeriod();
                //IBUtil.Log(TAG, "#@#Main Timer() 주기데이타 UPDATE_PERIOD[" + Const.pUpdate.UPDATE_PERIOD_TIME + "]sec");
            }

            if (!Constant.pUpdate.STATION_READY) {
                //STATION READY
                //System.out.printf("#@#Wait STATION_READY subBdScanCnt[%d]\n",Constant.pUpdate.subBdScanCnt);
                if (Constant.pUpdate.subBdScanCnt > 0) {
                    Constant.pUpdate.STATION_READY = true;
                    Constant.pUpdate.ctrl_asyncTask(0,1);
                    init_layout.setVisibility(View.GONE);
                    spin_kit.setVisibility(View.GONE);
                }
            }

            //IBUtil.Log(TAG, "Handler100_CC");

            if (diag_tick > 20){
                //IBUtil.Log(TAG, "check every 2초----mqtt_TryConnection:"+Constant.mqtt_TryConnection +"");
                diag_tick=0;
                IBUtil.setBatteryStatusList();//2000ms마다 check

                if(motorTestFlag && Constant.pUpdate.FORCE_CONTROL) {
                    ///test+
                    if (toggle) Constant.pUpdate.ctrl_asyncTask(0,1);
                    else Constant.pUpdate.ctrl_asyncTask(0,0);
                    toggle = !toggle;
                    ///test-
                }

                if(Constant.pUpdate.STATION_IP.length()<5 && !Constant.pUpdate.STATION_READY) {
                    new GetLocation_Task().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }

                if(funcbtn_layout.getVisibility() == View.VISIBLE)
                {
                    printLogStation(diag_id);
                }
            }
            if(Constant.mqtt_TryConnection > 5) Constant.mqtt_connect_ok=false;
            else Constant.mqtt_connect_ok=true;
            if(Constant.mqtt_connect_ok != exMqttConnection){
                exMqttConnection = Const.mqtt_connect_ok;
                Intent sendIntent = new Intent("otos.station.MQTT_CONNECT");
                context.sendBroadcast(sendIntent);
                if(Const.mqtt_connect_ok){
                    IBUtil.Log(TAG, "mqtt_connection ---->ok");
                }
                else{
                    IBUtil.Log(TAG, "mqtt_connection ---->fail");
                }
            }
            //mtime_end = System.currentTimeMillis();
            //IBUtil.Log(TAG, "process time1: " + (mtime_end - mtime_start) + "ms");

            otos_handler_100ms.removeCallbacksAndMessages(null);
            otos_handler_100ms.postDelayed(otosStationRunnable100ms, 100);
            //IBUtil.Log(TAG, "Handler100_DD");
        }
    };

    private final Runnable otosStationRunnable10ms = new Runnable() {
        @Override
        public void run() {
            //IBUtil.Log(TAG, "timer Handler() 10ms");
            long mtime_start, mtime_end;
            //IBUtil.Log(TAG, "Handler10_AA");
            Constant.pUpdate.EventCheck();
            Constant.pUpdate.EventMQTTSubscribe();

            Constant.pUpdate.Scan_Request();
            Constant.pUpdate.BUCKET_Update();

            //IBUtil.Log(TAG, "Handler10_CC");
            Constant.pUpdate.Tag_process();
            if (period_flag) {
                period_flag = false;
                new MQTT_RERIOD_Task().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                IBUtil.Log(TAG, "#@#Main 주기데이타 UPDATE_PERIOD[" + Const.pUpdate.UPDATE_PERIOD_TIME + "]sec  mqtt_TryConnection:"+Constant.mqtt_TryConnection);
            }

            //mtime_end = System.currentTimeMillis();
            //IBUtil.Log(TAG, "process time2: " + (mtime_end - mtime_start) + "ms");
            otos_handler_10ms.removeCallbacksAndMessages(null);
            otos_handler_10ms.postDelayed(otosStationRunnable10ms, 10);
            //IBUtil.Log(TAG, "Handler10_DD");
        }
    };

    private final Runnable otosStationRunnableBootcheck = new Runnable() {
        @Override
        public void run() {
            if(Constant.m_boot_completed) {
                IBUtil.Log(TAG, "timer Handler()___Ready android system!");
                otos_handler_bootcheck.removeCallbacksAndMessages(null);
                init();
            } else {
                IBUtil.Log(TAG, "timer Handler()___Not ready android system!");
                new Thread(new Runnable() {
                    public void run() {
                        // for set m_boot_completed = true;
                        // 부팅완료 후 화면뜨기전까지는 키 안들어가는거 이용한 꼼수.
                        new Instrumentation().sendKeyDownUpSync(KeyEvent.KEYCODE_BACKSLASH);
                    }
                }).start();
                otos_handler_bootcheck.removeCallbacksAndMessages(null);
                otos_handler_bootcheck.postDelayed(otosStationRunnableBootcheck, 10);
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        IBUtil.Log(TAG, "onDestroy()");
        otos_handler_100ms.removeCallbacksAndMessages(null);
        otos_handler_10ms.removeCallbacksAndMessages(null);
        otos_handler_bootcheck.removeCallbacksAndMessages(null);

        //otos_welcome_video.stop();
        try {
            Constant.pahoMqttClient.disconnect(Constant.mqttclient);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IBUtil.Log(TAG, "onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        IBUtil.Log(TAG, "onPause()");
    }

    private void station_initalize() {
        Constant.tag_reset = false;
        Constant.pUpdate.TAG_SEQ = 0;
        Constant.pUpdate.TAG_WAIT = 0;
        Constant.pUpdate.STATION_READY = false;
       // Constant.pUpdate.ctrl_asyncTask(0,0);
        Constant.pUpdate.HVAC_Fans("#ff00");//
    }

    public void bootcheck() {
        // disable status bar & soft key & systemUI
        IBUtil.runProcess(Constant.m_disableUI, true);

        //Touch sound off - effect video play
        //frameworks/base/core/java/android/provider/Settings.java
        Settings.System.putInt(getContentResolver(), Settings.System.SOUND_EFFECTS_ENABLED, 0);
        Prefs.with(this).readAll();

        otos_handler_bootcheck.removeCallbacksAndMessages(null);
        otos_handler_bootcheck.postDelayed(otosStationRunnableBootcheck, 0);
    }

    public void init() {
        IBUtil.Log(TAG, "init()");
        IBCheckNewSoftwere nschk = new IBCheckNewSoftwere(this);
        nschk.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        JNI = new jni();
        Const = new Constant();
        Constant.pUpdate = new UpdateInfo(this);

        JNI.DboardOpen();
        tz = TimeZone.getTimeZone("Asia/Seoul");

        setLayout();
        ClearFragment();
        ChangeFragment(IBFrag.FRAG_A_HOMEMENU);

//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) { }

        new GetLocation_Task().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        Constant.pUpdate.STATION_CPUID = IBUtil.getCpuInfoMap().get("Serial"); //Serial : cbd09eda7e59fc33
        if(!Constant.CLIENT_ID.contains(Constant.pUpdate.STATION_CPUID)) {
            Constant.CLIENT_ID = Constant.CLIENT_ID + Constant.pUpdate.STATION_CPUID; //clientID 고유한 값이여야 함. 동일한 clientID 짤라버림.
        }

        Constant.TOPIC_STATION = Constant.TOPIC_STATION + Constant.pUpdate.STATION_CPUID;
        Constant.pUpdate.STATION_APK_VERSION = String.format("%.2f", IBUtil.getVersion(this));

        IBUtil.Log(TAG, "#@#version TOPIC_STATION[" + Constant.TOPIC_STATION + "] STATION_APK_VERSION[" + Constant.pUpdate.STATION_APK_VERSION + "]");

        Constant.pahoMqttClient = new PahoMqttClient();
        Constant.mqttclient = Constant.pahoMqttClient.getMqttClient(getApplicationContext(), Constant.MQTT_BROKER_URL, Constant.CLIENT_ID);
        startService(new Intent(this, MqttMessageService.class));
        IBUtil.Log(TAG, "MqttMessageService start !!!!!!!");

        if(Constant.main_thread==false) {
            Constant.main_thread=true;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    station_initalize();
                    IBUtil.recv_submqtt(Constant.TOPIC_STATION);
                    Constant.pUpdate.pubMqtt_StationInit();
                    //Constant.pUpdate.pubMqttStationEvent("eINIT", "Init-System");
                    //IBUtil.Log(TAG, "#@#version TOPIC_STATION[" + Constant.TOPIC_STATION + "] STATION_IP[" + Constant.pUpdate.STATION_IP + "]");
                }
            }, 5000);

            //mTimer.sendEmptyMessage(0);
            otos_handler_10ms.removeCallbacksAndMessages(null);
            otos_handler_10ms.postDelayed(otosStationRunnable10ms, 0);
            otos_handler_100ms.removeCallbacksAndMessages(null);
            otos_handler_100ms.postDelayed(otosStationRunnable100ms, 0);
        }
        //station_initalize();
    }

    private void setLayout() {
        funcbtn_layout = (LinearLayout) findViewById(R.id.funcbtn_layout);
        debug_tv = (TextView) findViewById(R.id.debug_tv);
        android_setup_btn = (IButton) findViewById(R.id.android_setup_btn);
        init_layout = (LinearLayout) findViewById(R.id.init_layout);
        spin_kit = (SpinKitView) findViewById(R.id.spin_kit);

        android_setup_btn.setOnClickListener(this);

        funcbtn_layout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
//            case R.id.tag_btn:
//                IBUtil.Log(TAG, "onClick() tag_btn");
//                startBatteryCheck();
//                break;
//            case R.id.send_btn:
//                IBUtil.Log(TAG, "onClick() send_btn");
//                IBUtil.send_pubmqtt("android_test", Constant.PUBLISH_TOPIC_SERVER);
//                break;
//            case R.id.setup_btn:
//                IBUtil.Log(TAG, "onClick() setup_btn");
//                intent = new Intent(this, SettingActivity.class);
//                startActivity(intent);
//                break;
//            case R.id.start_btn:
//                IBUtil.Log(TAG, "onClick() start_btn");
//                break;
            case R.id.android_setup_btn:
                IBUtil.Log(TAG, "onClick() android_setup_btn");
                goSetting();
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(IBUtil.frOnKeyDown(keyCode, getFragmentManager().findFragmentById(R.id.menu_frag))) return true;
        Intent sendIntent = new Intent("otos.station.FORCE_CONTROL");
        context.sendBroadcast(sendIntent);

        switch (keyCode) {
            case KeyEvent.KEYCODE_1:
                IBUtil.Log(TAG, "KeyEvent.KEYCODE_1");
                Constant.pUpdate.CBOARD_SetRGBs("@01|255|0|0|1");
                Constant.pUpdate.HVAC_ACRelays("#0101");
                Constant.pUpdate.CBOARD_SPSs("#000101");
                return true;
            case KeyEvent.KEYCODE_2:
                IBUtil.Log(TAG, "KeyEvent.KEYCODE_2");
                Constant.pUpdate.CBOARD_SetRGBs("@01|0|255|0|1");
                Constant.pUpdate.HVAC_ACRelays("#0202");
                Constant.pUpdate.CBOARD_SPSs("#000100");
                return true;
            case KeyEvent.KEYCODE_3:
                IBUtil.Log(TAG, "KeyEvent.KEYCODE_3");
                Constant.pUpdate.CBOARD_SetRGBs("@00|0|0|255|1");
                Constant.pUpdate.HVAC_ACRelays("#0404");
                return true;
            case KeyEvent.KEYCODE_4:
                IBUtil.Log(TAG, "KeyEvent.KEYCODE_4");
                Constant.pUpdate.HVAC_ACRelays("#0808");
                return true;
            case KeyEvent.KEYCODE_5:
                IBUtil.Log(TAG, "KeyEvent.KEYCODE_5");
                //Constant.pUpdate.CBOARD_Relay(0x00, 0);
                Constant.pUpdate.HVAC_Fans("#ffff");//
                return true;
            case KeyEvent.KEYCODE_6:
                IBUtil.Log(TAG, "KeyEvent.KEYCODE_6");
                Constant.pUpdate.HVAC_Fans("#ff00");
                if(Constant.pUpdate.FORCE_CONTROL) {
                    Constant.pUpdate.FC_flag=0xff80;
                }
                return true;
            case KeyEvent.KEYCODE_7:
                IBUtil.Log(TAG, "KeyEvent.KEYCODE_7");
                motorTestFlag = !motorTestFlag;
                return true;

            case KeyEvent.KEYCODE_8:
                Constant.pUpdate.FORCE_CONTROL=true;
                IBUtil.Log(TAG, "KeyEvent.KEYCODE_8");
                Constant.pUpdate.ctrl_asyncTask(0,0);
                Constant.pUpdate.CBOARD_LEDAll(0x80800000);
                return true;

            case KeyEvent.KEYCODE_0: //all lock close
                IBUtil.Log(TAG, "KeyEvent.KEYCODE_0");
                Constant.pUpdate.FORCE_CONTROL=false;
                Constant.pUpdate.HVAC_ACRelays("#0F0F");
                Constant.pUpdate.ctrl_asyncTask(0,1);
                return true;

            case KeyEvent.KEYCODE_MENU:
            case 605:    //inbar INFO-KEY
                diag_id=Constant.pUpdate.CBD_CNT;
                if(funcbtn_layout.getVisibility() != View.VISIBLE)
                    funcbtn_layout.setVisibility(View.VISIBLE);
                return true;

            case KeyEvent.KEYCODE_BACKSLASH:
                Constant.m_boot_completed = true;
                return true;
            case KeyEvent.KEYCODE_BACK:
                IBUtil.Log(TAG, "KeyEvent.KEYCODE_BACK");
                if(funcbtn_layout != null) {
                    if (funcbtn_layout.getVisibility() == View.VISIBLE)
                        funcbtn_layout.setVisibility(View.INVISIBLE);
                }
                return true;
            case KeyEvent.KEYCODE_DPAD_UP:
                IBUtil.Log(TAG, "KeyEvent.KEYCODE_DPAD_UP");
                if(funcbtn_layout.getVisibility() == View.VISIBLE) diag_id = Constant.pUpdate.CBD_CNT;
                return true;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                IBUtil.Log(TAG, "KeyEvent.KEYCODE_DPAD_DOWN");
                if(funcbtn_layout.getVisibility() == View.VISIBLE) diag_id=0;
                return true;

            case KeyEvent.KEYCODE_DPAD_LEFT:
                if(funcbtn_layout.getVisibility() == View.VISIBLE)
                {
                    diag_id--;
                    if(diag_id<0)diag_id=Constant.pUpdate.CBD_CNT;
                    diag_id = diag_id%(Constant.pUpdate.CBD_CNT+1);
                    printLogStation(diag_id);
                }
                IBUtil.Log(TAG, "#@# KeyEvent.KEYCODE_DPAD_LEFT"+diag_id);
                return true;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                if(funcbtn_layout.getVisibility() == View.VISIBLE)
                {
                    diag_id++;
                    diag_id = diag_id % (Constant.pUpdate.CBD_CNT+1);
                    printLogStation(diag_id);
                }
                IBUtil.Log(TAG, "#@# KeyEvent.KEYCODE_DPAD_RIGHT"+diag_id);
                return true;
        }

        IBUtil.Log(TAG, "KEYCODE onKeyDown: " + keyCode);
        //printLog("KEYCODE onKeyDown: " + keyCode);
        return super.onKeyDown(keyCode, event);
    }

    private  class MQTT_RERIOD_Task extends AsyncTask<Void,Void,Void> {
        private Elements element;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        { //백그라운드 작업이 진행되는 곳.
            Constant.pUpdate.pubMqttPeriod();
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            //doInBackground 작업이 끝나고 난뒤의 작업
            //Constant.pUpdate.STATION_IP = element.text();
        }
    }


    private  class GetLocation_Task extends AsyncTask<Void,Void,Void> {
        private String res;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        { //백그라운드 작업이 진행되는 곳.
            String url="http://ip-api.com/json";
            try {
                res = Jsoup.connect(url).ignoreContentType(true).execute().body();
            }
            catch (IOException e) {
                Log.d(TAG, "#@#Location IOException");
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            //doInBackground 작업이 끝나고 난뒤의 작업
            JSONObject obj=null;
            try {
                obj = new JSONObject(res);
                Constant.pUpdate.STATION_IP = obj.getString("query");
                Constant.pUpdate.STATION_NATION = obj.getString("countryCode");
                Constant.pUpdate.STATION_CITY = obj.getString("city");
                Constant.pUpdate.STATION_TIMEZONE = obj.getString("timezone");
                Constant.pUpdate.STATION_LAT = obj.getString("lat");
                Constant.pUpdate.STATION_LON = obj.getString("lon");

            } catch (Throwable t) {
                Log.e(TAG, "#@#Could not parse malformed JSON: \"" + res + "\"");
            }
        }
    }

    private void goSetting(){
        startActivity(new Intent(Settings.ACTION_SETTINGS));
    }

    private void printLogStation(int id) {
        //Log.d(TAG, "#@# ---printLogStation id: " + id + "---");
        String newText="";
       // if (debug_tv.getLineCount() > LOG_MAXLINE_COUNT) debug_tv.setText("");
        debug_tv.setText("");
        if(id< Constant.pUpdate.CBD_CNT){
            newText = Constant.pUpdate.Diago_Cbd(id);
            debug_tv.setText(newText);
        }
        else if(id == Constant.pUpdate.CBD_CNT){
            newText = Constant.pUpdate.Diago_Hvac();
            debug_tv.setText(newText);
        }
    }


    public void ClearFragment() {
        try {
            int count = getFragmentManager().getBackStackEntryCount();
            IBUtil.Log(TAG, "getBackStackEntryCount(): " + count);
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            Constant.m_frag_tag = IBFrag.FRAG_A_HOMEMENU;
            //showRequestBadge(Constant.pUpdate.m_frag_tag);
        } catch (Exception e) {
            IBUtil.Log(TAG, "ClearFragment err_" + e.getMessage());
        }
    }

    public void ChangeFragment(int menu) {
        FragmentTransaction fragTran = getFragmentManager().beginTransaction();
        Constant.m_current_frag = IBUtil.ChangeFragment(this, menu, R.id.menu_frag, fragTran);
    }

    public void display_batt_cnt(int pos) {
        String  str;
        if(pos==1)  str= String.format("Left Batt Incomming\n");
        else if(pos==2)  str= String.format("Right Batt Incomming\n");
        else if(pos==3)  str= String.format("Batt Incomming Success\n");
        else str="";
        IBUtil.noti_msg((Activity) context, str);
    }
}


