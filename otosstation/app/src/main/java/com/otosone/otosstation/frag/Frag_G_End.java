package com.otosone.otosstation.frag;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.otosone.otosstation.R;
import com.otosone.otosstation.StationActivity;
import com.otosone.otosstation.utils.Constant;
import com.otosone.otosstation.utils.IBUtil;

/**
 * Created by shootdol-tw on 2019-10-01.
 */

public class Frag_G_End extends Fragment {
    protected final String TAG = getClass().getSimpleName();
    private View view;
    static Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_g_end, viewGroup, false);
        context = view.getContext();
        init();
        return view;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible && isResumed()) {
            IBUtil.Log(TAG, "visible!!!!!!!!!!!!!! ");
            onResume();
        } else {
            IBUtil.Log(TAG, "invisible!!!!!!!!!!!!!!");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IBUtil.Log(TAG, "onResume()");
        Constant.m_frag_tag = IBFrag.FRAG_G_END;
    }

    @Override
    public void onPause() {
        super.onPause();
        IBUtil.Log(TAG, "onPause()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IBUtil.Log(TAG, "onDestroy()");
    }

    void init() {
        IBUtil.Log(TAG, "init()");

        setLayout();
        Handler mHandler = new Handler();
        mHandler.postDelayed(mEndRunnable, 2000);
    }

    public boolean frOnKeyDown(int key_code) {
        boolean ret = false;
        switch (key_code) {
        }
        return ret;
    }

    private void setLayout() {
    }

    private final Runnable mEndRunnable = new Runnable() {
        @Override
        public void run() {

            gotoHome();
        }
    };

    private void gotoHome() {
        Constant.tag_reset = true;//timeout
        ((StationActivity) context).ClearFragment();
        ((StationActivity) context).ChangeFragment(IBFrag.FRAG_A_HOMEMENU);

//        Intent intent;
//        intent = new Intent(this, StationActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(intent);
//        finish();
    }
}
