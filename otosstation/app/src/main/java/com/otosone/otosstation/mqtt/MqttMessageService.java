package com.otosone.otosstation.mqtt;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.NonNull;

import com.otosone.otosstation.utils.Constant;
import com.otosone.otosstation.utils.IBUtil;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;


public class MqttMessageService extends Service {

    private static final String TAG = "MqttMessageService";
    private PahoMqttClient pahoMqttClient;
    private MqttAndroidClient mqttAndroidClient;

    public MqttMessageService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        IBUtil.Log(TAG, "onCreate");
        try {
            if(!Constant.CLIENT_ID.contains(Constant.pUpdate.STATION_CPUID)) {
                Constant.pUpdate.STATION_CPUID = IBUtil.getCpuInfoMap().get("Serial"); //Serial : cbd09eda7e59fc33
                Constant.CLIENT_ID = Constant.CLIENT_ID + Constant.pUpdate.STATION_CPUID; //clientID 고유한 값이여야 함. 동일한 clientID 짤라버림.
            }

            pahoMqttClient = new PahoMqttClient();
            mqttAndroidClient = pahoMqttClient.getMqttClient(getApplicationContext(), Constant.MQTT_BROKER_URL, Constant.CLIENT_ID);
            mqttAndroidClient.setCallback(new MqttCallbackExtended() {
                @Override
                public void connectComplete(boolean b, String s) {
                    Constant.mqtt_TryConnection=0;
                    IBUtil.Log(TAG, "mqtt_connectComplete!");
                }

                @Override
                public void connectionLost(Throwable throwable) {
                    Constant.mqtt_TryConnection=100;
                    IBUtil.Log(TAG, "mqtt_connectionLost");
                }

                @Override
                public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
                    //setMessageNotification(s, new String(mqttMessage.getPayload(), "UTF-8"));
                    setMessageNotification(s, new String(mqttMessage.getPayload(), "UTF-8"));
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                    IBUtil.Log(TAG, "mqtt_deliveryComplete");
                }
            });
        } catch (Exception e) {
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        IBUtil.Log(TAG, "onStartCommand");
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IBUtil.Log(TAG, "onDestroy");
    }

    private void setMessageNotification(@NonNull String topic, @NonNull String msg)
    {
        JSONObject jobj;
        IBUtil.Log(TAG, "#@#Mqtt ==========setMessageNotification==topic: "+topic+" msg"+msg);
        try {
            jobj = new JSONObject(msg);
        }
        catch (JSONException e) {
            jobj =null;
        }
        IBUtil.Log(TAG, "mqtt===> Message arrived jarry: " + jobj);
        Constant.list_mqtt_obj.add(jobj);
    }
}
