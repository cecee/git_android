package com.otosone.otosstation.frag;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.otosone.otosstation.BatteryList;
import com.otosone.otosstation.BatteryListAdapter;
import com.otosone.otosstation.R;
import com.otosone.otosstation.utils.Constant;
import com.otosone.otosstation.utils.IBUtil;

import java.util.ArrayList;

/**
 * Created by shootdol-tw on 2019-10-01.
 */

public class Frag_F_Take extends Fragment {
    protected final String TAG = getClass().getSimpleName();
    private View view;
    static Context context;
    private TextView card_name_txt, info_txt;

    private ArrayList<BatteryList> battery_status_list = new ArrayList<BatteryList>();
    private GridView battery_status_grid = null;
    public BatteryListAdapter battery_status_adapter = null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_f_take, viewGroup, false);
        context = view.getContext();
        init();
        return view;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible && isResumed()) {
            IBUtil.Log(TAG, "visible!!!!!!!!!!!!!! ");
            onResume();
        } else {
            IBUtil.Log(TAG, "invisible!!!!!!!!!!!!!!");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IBUtil.Log(TAG, "onResume()");
        Constant.m_frag_tag = IBFrag.FRAG_F_TAKE;
    }

    @Override
    public void onPause() {
        super.onPause();
        IBUtil.Log(TAG, "onPause()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IBUtil.Log(TAG, "onDestroy()");
    }

    void init() {
        IBUtil.Log(TAG, "init()");

        setLayout();
        battery_take_info();
    }

    public boolean frOnKeyDown(int key_code) {
        boolean ret = false;
        switch (key_code) {
            case KeyEvent.KEYCODE_1:
                IBUtil.Log(TAG, "KeyEvent.KEYCODE_1");
                Constant.test_take = true;
                Constant.pUpdate.TAG_SEQ = 9;
                return true;
        }
        return ret;
    }

    private void setLayout() {
        card_name_txt = (TextView) view.findViewById(R.id.card_name_txt);
        info_txt = (TextView) view.findViewById(R.id.info_txt);
        battery_status_grid = (GridView) view.findViewById(R.id.battery_status_grid);
        card_name_txt.setText(Constant.NFC_CARD_NAME);
    }

    private void battery_take_info() {
        battery_status_adapter = new BatteryListAdapter(context, R.layout.item_batterylist, battery_status_list);
        battery_status_grid.setAdapter(battery_status_adapter);
        battery_status_adapter.setType(1);
        info_txt.setText(getString(R.string.replace_info1));
        setBatteryStatusList(true);
    }

    private void setBatteryStatusList(boolean update) {
        battery_status_list.clear();
        for(int ii=0; ii< Constant.pUpdate.BUCK_CNT; ii++) {
            BatteryList s1 = new BatteryList();
            s1.setBattery_id(ii);
            if(ii == Constant.pUpdate.take_battery_pos)
            {
                s1.setGateEmpty(true);
            } else {
                s1.setGateEmpty(false);
            }

            battery_status_list.add(s1);
            //3x3 배열중 첫라인 두번째는 버킷아님. 모니터
            //if(ii==0) {
               // battery_status_list.add(null);
            //}
            IBUtil.Log(TAG, "ADD DUMMY  !!!!!");
        }

        if(update) {
            battery_status_adapter.notifyDataSetChanged();
        }
    }
}
