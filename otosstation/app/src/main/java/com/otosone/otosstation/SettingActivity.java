package com.otosone.otosstation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.otosone.otosstation.utils.Constant;
import com.otosone.otosstation.utils.IBUtil;
import com.otosone.otosstation.utils.IButton;
import com.otosone.otosstation.utils.Prefs;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {
    protected final String TAG = getClass().getSimpleName();
    private IButton close_btn;
    private EditText server_addr_txt, server_port_txt, server_topic_server_txt, server_topic_station_txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        init();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IBUtil.Log(TAG, "onDestroy()");
    }

    @Override
    public void onResume() {
        super.onResume();
        IBUtil.Log(TAG, "onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        IBUtil.Log(TAG, "onPause()");
    }

    public void init() {
        setLayout();
        setSetting();
    }

    private void setLayout() {
        close_btn   = (IButton) findViewById(R.id.close_btn);
        close_btn.setOnClickListener(this);

        server_addr_txt = (EditText) findViewById(R.id.server_addr_txt);
        server_port_txt = (EditText) findViewById(R.id.server_port_txt);
        server_topic_server_txt = (EditText) findViewById(R.id.server_topic_server_txt);
        server_topic_station_txt = (EditText) findViewById(R.id.server_topic_station_txt);
    }

    public void setSetting() {
        server_addr_txt.setText(Constant.MQTT_BROKER_ADDR);
        server_port_txt.setText(Constant.MQTT_BROKER_PORT);
        server_topic_server_txt.setText(Constant.PUBLISH_TOPIC_SERVER);
        //server_topic_station_txt.setText(Constant.PUBLISH_TOPIC_STATION);
    }

    public void close_setting() {
        Prefs.with(this).write("SERVER_ADDR", server_addr_txt.getText().toString().trim());
        Prefs.with(this).write("SERVER_PORT", server_port_txt.getText().toString().trim());
        Prefs.with(this).write("PUBLISH_TOPIC_SERVER", server_topic_server_txt.getText().toString().trim());
        Prefs.with(this).write("PUBLISH_TOPIC_STATION", server_topic_station_txt.getText().toString().trim());
        Prefs.with(this).write("CLIENT_ID", "otos_station_999999");
        Prefs.with(this).readAll();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.close_btn:
                IBUtil.Log(TAG, "onClick() close_btn");
                close_setting();
                break;
        }
    }
}
