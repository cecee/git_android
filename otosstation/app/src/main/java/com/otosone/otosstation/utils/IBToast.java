package com.otosone.otosstation.utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.otosone.otosstation.R;

/**
 * Created by shootdol-tw on 2017-03-06.
 */

public class IBToast {
    public static final int LENGTH_SHORT = 3000;
    public static final int LENGTH_LONG = 7000;

    public static final int LENGTH_STICKY = -1;

    public static final int PRIORITY_LOW = Integer.MIN_VALUE;
    public static final int PRIORITY_NORMAL = 0;
    public static final int PRIORITY_HIGH = Integer.MAX_VALUE;
    public static final Style STYLE_ALERT = new Style(LENGTH_LONG, R.color.ibtoast_alert_bg);
    public static final Style STYLE_CONFIRM = new Style(LENGTH_SHORT, R.color.confirm);
    //public static final Style STYLE_INFO = new Style(LENGTH_SHORT, R.color.ibtoast_info_bg);
    //public static final Style STYLE_INFO = new Style(LENGTH_STICKY, R.color.ibtoast_info_bg);
    public static final Style STYLE_INFO = new Style(LENGTH_STICKY, R.drawable.bg_songinfobar);

    private final Activity mActivity;
    private int mDuration = LENGTH_SHORT;
    private View mView;
    private ViewGroup mParent;
    private ViewGroup.LayoutParams mLayoutParams;
    private boolean mFloating;
    Animation mInAnimation, mOutAnimation;
    int mPriority = PRIORITY_NORMAL;

    public IBToast(Activity activity) {
        mActivity = activity;
    }

    public void show() {
        IBToastManager manager = IBToastManager.obtain(mActivity);
        manager.add(this);
    }

    public boolean isShowing() {
        if (mFloating) {
            return mView != null && mView.getParent() != null;
        } else {
            return mView.getVisibility() == View.VISIBLE;
        }
    }

    public void cancel() {
        IBToastManager.obtain(mActivity).clearMsg(this);
    }

    public static void cancelAll() {
        IBToastManager.clearAll();
    }

    public static void cancelAll(Activity activity) {
        IBToastManager.release(activity);
    }

    public Activity getActivity() {
        return mActivity;
    }

    public void setView(View view) {
        mView = view;
    }

    public View getView() {
        return mView;
    }

    public void setDuration(int duration) {
        mDuration = duration;
    }

    public int getDuration() {
        return mDuration;
    }

//    public void setText(int resId) {
//        setText(mActivity.getText(resId));
//    }
//
//    public void setText(CharSequence s) {
//        if (mView == null) {
//            throw new RuntimeException("This IBToast was not created with IBToast.makeText()");
//        }
//        TextView tv = (TextView) mView.findViewById(R.id.ibtoast_songno);
//        if (tv == null) {
//            throw new RuntimeException("This IBToast was not created with IBToast.makeText()");
//        }
//        tv.setText(s);
//    }

    public ViewGroup.LayoutParams getLayoutParams() {
        if (mLayoutParams == null) {
            mLayoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        return mLayoutParams;
    }

    public IBToast setLayoutParams(ViewGroup.LayoutParams layoutParams) {
        mLayoutParams = layoutParams;
        return this;
    }

    public IBToast setLayoutGravity(int gravity) {
        mLayoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, gravity);
        return this;
    }

    public boolean isFloating() {
        return mFloating;
    }

    public void setFloating(boolean mFloating) {
        this.mFloating = mFloating;
    }

    public IBToast setAnimation(int inAnimation, int outAnimation) {
        return setAnimation(AnimationUtils.loadAnimation(mActivity, inAnimation),
                AnimationUtils.loadAnimation(mActivity, outAnimation));
    }

    public IBToast setAnimation(Animation inAnimation, Animation outAnimation) {
        mInAnimation = inAnimation;
        mOutAnimation = outAnimation;
        return this;
    }

    public int getPriority() {
        return mPriority;
    }

    public void setPriority(int priority) {
        mPriority = priority;
    }

    public ViewGroup getParent() {
        return mParent;
    }

    public void setParent(ViewGroup parent) {
        mParent = parent;
    }

    public void setParent(int parentId) {
        setParent((ViewGroup) mActivity.findViewById(parentId));
    }

    public static class Style {

        private final int duration;
        private final int background;

        public Style(int duration, int resId) {
            this.duration = duration;
            this.background = resId;
        }

        public int getDuration() {
            return duration;
        }

        public int getBackground() {
            return background;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof IBToast.Style)) {
                return false;
            }
            Style style = (Style) o;
            return style.duration == duration
                    && style.background == background;
        }
    }

    //for info msg
    public static IBToast makeText_InfoMsg(Activity context, int msgtype, int infomsg, Style style) {
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflate.inflate(R.layout.toast_infomsg, null);

        IBToast result = new IBToast(context);

        //view.setBackgroundResource(style.background);
        LinearLayout lv = (LinearLayout) view.findViewById(R.id.ibtoast_layout);
        lv.setBackgroundResource(style.background);

        ImageView ibtoast_msgtype_img = (ImageView) view.findViewById(R.id.ibtoast_msgtype_img);
        TextView ibtoast_infomsg_txt = (TextView) view.findViewById(R.id.ibtoast_infomsg_txt);

        switch (msgtype) {
            default:
            case 0:
                ibtoast_msgtype_img.setBackgroundResource(R.drawable.ic_error_circle);
                break;
        }
        ibtoast_infomsg_txt.setText(infomsg);

        result.mView = view;
        result.mDuration = style.duration;
        result.mFloating = true;
        IBUtil.Log("TAG", "#@#@@@@@@@@@@@#### makeText_InfoMsg " + infomsg);
        return result;
    }

    public static IBToast makeText_InfoMsg(Activity context, int msgtype, String infomsg, Style style) {
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflate.inflate(R.layout.toast_infomsg, null);

        IBToast result = new IBToast(context);

        //view.setBackgroundResource(style.background);
        LinearLayout lv = (LinearLayout) view.findViewById(R.id.ibtoast_layout);
        lv.setBackgroundResource(style.background);

        ImageView ibtoast_msgtype_img = (ImageView) view.findViewById(R.id.ibtoast_msgtype_img);
        TextView ibtoast_infomsg_txt = (TextView) view.findViewById(R.id.ibtoast_infomsg_txt);

        switch (msgtype) {
            default:
            case 0:
                ibtoast_msgtype_img.setBackgroundResource(R.drawable.ic_error_circle);
                break;
        }
        ibtoast_infomsg_txt.setText(infomsg);

        result.mView = view;
        result.mDuration = style.duration;
        result.mFloating = true;
        IBUtil.Log("TAG", "#@#@@@@@@@@@@@#### makeText_InfoMsg " + infomsg);

        return result;
    }
}
