package com.otosone.otosstation.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

/**
 * Created by shootdol-tw on 2017-02-22.
 */

public class IBMonitor extends BroadcastReceiver {
    protected final String TAG = getClass().getSimpleName();

    public final static int CONNECTIVITY_CHANGE = 0x00;
    public final static int FORCE_CONTROL = CONNECTIVITY_CHANGE + 1;
    public final static int MQTT_CONNECT = 2;

    public interface OnChangeIBStatusListener {
        public void OnChanged(int status);
    }

    //private WifiManager m_WifiManager = null;
    //private ConnectivityManager m_ConnManager = null;
    private OnChangeIBStatusListener m_OnChangeIBStatusListener = null;

    public IBMonitor(Context context) {
        //m_WifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        //m_ConnManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public void setOnChangeIBStatusListener(OnChangeIBStatusListener listener) {
        m_OnChangeIBStatusListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (m_OnChangeIBStatusListener == null) {
            return;
        }

        String strAction = intent.getAction();
        //IBUtil.Log(TAG, "strAction: " + strAction);
        if(strAction.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
            IBUtil.Log(TAG, "android.net.conn.CONNECTIVITY_CHANGE");
            m_OnChangeIBStatusListener.OnChanged(CONNECTIVITY_CHANGE);
        }
        if(strAction.equals("otos.station.FORCE_CONTROL")) {
            IBUtil.Log(TAG, "otos.station.FORCE_CONTROL");
            m_OnChangeIBStatusListener.OnChanged(FORCE_CONTROL);
        }
        if(strAction.equals("otos.station.MQTT_CONNECT")) {
            IBUtil.Log(TAG, "otos.station.MQTT_CONNECT");
            m_OnChangeIBStatusListener.OnChanged(MQTT_CONNECT);
        }
    }
}
